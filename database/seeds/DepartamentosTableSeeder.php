<?php

use Illuminate\Database\Seeder;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departamentos')->insert(
            [
				['id_pais' => 114, 'nombre' => 'Antioquia'],
				['id_pais' => 114, 'nombre' => 'Atlántico'],
				['id_pais' => 114, 'nombre' => 'Bolívar'],
				['id_pais' => 114, 'nombre' => 'Boyacá'],
				['id_pais' => 114, 'nombre' => 'Caldas'],
				['id_pais' => 114, 'nombre' => 'Cauca'],
				['id_pais' => 114, 'nombre' => 'Cesar'],
				['id_pais' => 114, 'nombre' => 'Córdoba'],
				['id_pais' => 114, 'nombre' => 'Cundinamarca'],
				['id_pais' => 114, 'nombre' => 'Guajira'],
				['id_pais' => 114, 'nombre' => 'Huila'],
				['id_pais' => 114, 'nombre' => 'Magdalena'],
				['id_pais' => 114, 'nombre' => 'Meta'],
				['id_pais' => 114, 'nombre' => 'Nariño'],
				['id_pais' => 114, 'nombre' => 'Norte de Santander'],
				['id_pais' => 114, 'nombre' => 'Quindío'],
				['id_pais' => 114, 'nombre' => 'Risaralda'],
				['id_pais' => 114, 'nombre' => 'Santander'],
				['id_pais' => 114, 'nombre' => 'Sucre'],
				['id_pais' => 114, 'nombre' => 'Tolima'],
				['id_pais' => 114, 'nombre' => 'Valle del Cauca'],
            ]
        );
    }
}
