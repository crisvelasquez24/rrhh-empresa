<?php

use Illuminate\Database\Seeder;

class DepartamentosRhTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departamentos_rhs')->insert(
            [
                [
                    'nombre'  => 'Comercial/Ventas',
                    'funcion' => 'funcion1',
                ],
                [
                    'nombre'  => 'Producción',
                    'funcion' => 'funcion2',
                ],
                ['nombre' => 'Compras',
                    'funcion' => 'funcion3',
                ],
                ['nombre' => 'Administración ',
                    'funcion' => 'funcion4',
                ],
                ['nombre' => 'Financiero',
                    'funcion' => 'funcion5',
                ],
                ['nombre' => 'Control de Gestión',
                    'funcion' => 'funcion5'],
                ['nombre' => 'Marketing',
                    'funcion' => 'funcion6',
                ],
                ['nombre' => 'Recursos Humanos',
                    'funcion' => 'funcion7'],
                ['nombre' => 'Dirección/Gerencia',
                    'funcion' => 'funcion8',
                ],
            ]
        );
    }
}
