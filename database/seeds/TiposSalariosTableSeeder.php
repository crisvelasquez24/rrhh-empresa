<?php

use Illuminate\Database\Seeder;

class TiposSalariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_salarios')->insert(
            [
                [
                    'descripcion' => 'Salario por unidad de obra:',
                    'caracteristicas'=>
                        'es el que se recibe en función de la cantidad de trabajo realizado. No importa el tiempo empleado. Lo importante es que la obra haya finalizado para poder recibir el pago.'
                ],
                [
                    'descripcion' => 'Salario por unidad de tiempo',
                    'caracteristicas'=>
                    'en este caso, en cambio, lo que determina el salario es el tiempo empleado durante la realización de una tarea: un día, una semana, un mes, un trimestre, un semestre o incluso un año.'
                ],
                [
                    'descripcion' => 'Salario mixto',
                    'caracteristicas'=>
                        'aquel que encuentra una vía intermedia entre el salario por unidad de obra y el salario por unidad de tiempo.'
                ],
                [
                    'descripcion' => 'Salario nominal',
                    'caracteristicas'=>
                        'es uno de los salarios más habituales. Se establece en el contrato refrendado por el trabajador y la empresa. Retribuye un cargo o plaza específica. Es el que comúnmente denominamos ‘nómina’.'
                ],
                [
                    'descripcion' => 'Salario en metálico',
                    'caracteristicas'=>
                        'es el que se paga con la moneda legal y en vigor del lugar en el que se realiza el trabajo. La persona que lo recibe tiene la facultad de realizar pagos en efectivo o ‘en metálico’. Esta modalidad de salario se ha visto reducida considerablemente en las últimas décadas con el auge de los recursos digitales y el servicio de banca online de las entidades bancarias.'
                ],
                [
                    'descripcion' => 'Salario en especie',
                    'caracteristicas'=>
                        'es aquel que se paga con medios o bienes diferentes al dinero. Por ejemplo con ropa, artículos para el hogar, comida, bonos o incluso con bienes inmateriales y servicios como el alojamiento o las clases a domicilio.'
                ]
            ]
        );
    }
}
