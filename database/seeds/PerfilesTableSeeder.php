<?php

use Illuminate\Database\Seeder;

class PerfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('perfiles')->insert(
			[
				['nombre' => 'Aspirante'],
				['nombre' => 'Administrador'],
				['nombre' => 'Jefe de Área'],
				['nombre' => 'Empleado'],
			]
		);
    }
}
