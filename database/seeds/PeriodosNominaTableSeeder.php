<?php

use Illuminate\Database\Seeder;

class PeriodosNominaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('periodos_nominas')->insert(
            [
                [
                    'periodo' => '01',
                    'mes' => 'Enero',
                ],
                [
                    'periodo' => '02',
                    'mes' => 'Febrero',
                ],
                            [
                    'periodo' => '03',
                    'mes' => 'Marzo',
                ],
                            [
                    'periodo' => '04',
                    'mes' => 'Abril',
                ],
                            [
                    'periodo' => '05',
                    'mes' => 'Mayo',
                ],
                            [
                    'periodo' => '06',
                    'mes' => 'Junio',
                ],
                            [
                    'periodo' => '07',
                    'mes' => 'Julio',
                ],
                            [
                    'periodo' => '08',
                    'mes' => 'Agosto',
                ],
                            [
                    'periodo' => '09',
                    'mes' => 'Septiembre',
                ],
                            [
                    'periodo' => '10',
                    'mes' => 'Octubre',
                ],
                            [
                    'periodo' => '11',
                    'mes' => 'Noviembre',
                ],
                            [
                    'periodo' => '12',
                    'mes' => 'Diciembre',
                ]
            ]
		);
    }
}
