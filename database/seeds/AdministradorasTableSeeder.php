<?php

use Illuminate\Database\Seeder;

class AdministradorasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('administradoras')->insert(
            [
                [
                    'nombre' => "EPS: Coomeva",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Comparta",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Sanitas",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS:Asmet salu",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Coosalud",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Saludcoop",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Medimas",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Famisanar",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Saludtotal",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: AlianSalud",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Cafesalud",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Comfenalco",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Susalud",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Fosyga",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: Saludvida",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "EPS: SURA",
                    'tipo' => 1,               
                ],
                [
                    'nombre' => "AFP:Fondo de pension ISS",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFP:Fondo de pension ING",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFP:Fondo de pension Colfondos",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFP:Fondo de Pension Horizonte",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFP:Fondo de pension Cajanal",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFP:Fondo de Pension Porvenir",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFP:Fondo de pension Proteccion",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFP:Fondo de pension Skandia",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFP:Fondo de pension Colpensiones",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFP:Fondo de pension Citycolfondos",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFS:Fondo solid pensi ISS",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFS Fondo solid pensi Colfondos",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFS:Fondo solid pensi horizone",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFS:Fondo solid pensi Porvenir",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFS:Fondo solid pensi Proteccion",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFS:Fondo solid pensi ING",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFS:Fondo solid pensi Colpensiones",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFS:Fondo solid pensi Skandia",
                    'tipo' => 2,               
                ],
                [
                    'nombre' => "AFS:Citycolfondos",
                    'tipo' => 2,               
                ],
            ]
        );        
    }
}
