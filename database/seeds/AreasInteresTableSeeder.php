<?php

use Illuminate\Database\Seeder;

class AreasInteresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('area_interes')->insert(
			[
				['nombre' => 'Ingeniero Sistema'],
				['nombre' => 'Contaduria'],
				['nombre' => 'Recurso Jumano'],
				['nombre' => 'Servicios Generales'],
			]
		);
    }
}
