<?php

use Illuminate\Database\Seeder;

class CargosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('cargos')->insert(
			[
				['nombre' => 'Gerente Área Informática','id_departamento_rhs' => 1],
				['nombre' => 'Encargado de Informática','id_departamento_rhs' => 2],
				['nombre' => 'Asistente Informático','id_departamento_rhs' => 3],
				['nombre' => 'Gerente de Análisis de Sistemas','id_departamento_rhs' => 4],
				['nombre' => 'Arquitecto de Sistemas Informáticos','id_departamento_rhs' => 5],
				['nombre' => 'Programador Senior','id_departamento_rhs' => 6],
				['nombre' => 'Analista Programador','id_departamento_rhs' => 7],
				['nombre' => 'Programador','id_departamento_rhs' => 8],
				['nombre' => 'Tester de Control de Calidad de Software','id_departamento_rhs' => 9],
			]
		);
    }
}
