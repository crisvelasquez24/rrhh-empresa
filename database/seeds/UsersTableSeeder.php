<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert(
			[
                [
                    'first_name' => 'william',
                    'last_name' => 'Martinez',
                    'name' => 'Administrador',
                    'email' => 'wirimago19@gmail.com',
                    'estado' => 1,
                    'password' => Hash::make("123456"),
                    'id_perfil' => 2,                    
                ],
                [
                    'first_name' => 'Juan',
                    'last_name' => 'Menenez',
                    'name' => 'aspirante',
                    'email' => 'aspirante@gmail.com',
                    'estado' => 1,
                    'password' => Hash::make("123456"),
                    'id_perfil' => 1,                    
                ],
                [
                    'first_name' => 'Carlos',
                    'last_name' => 'Perez',
                    'name' => 'jefearea',
                    'email' => 'jefearea@gmail.com',
                    'estado' => 1,
                    'password' => Hash::make("123456"),
                    'id_perfil' => 3,                    
                ],
                [
                    'first_name' => 'Pedro',
                    'last_name' => 'Lopez',
                    'name' => 'empleado',
                    'email' => 'empleado@gmail.com',
                    'estado' => 1,
                    'password' => Hash::make("123456"),
                    'id_perfil' => 4,                    
                ],                
			]
		);
    }
}
