<?php

use Illuminate\Database\Seeder;

class TiposAltasBajasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_altas_bajas')->insert(
            [
                [
                    'nombre' => "Hora Extra",
                    'tipo' => 1,
                    
                ],
            ]
        );
    }
}
