<?php

use Illuminate\Database\Seeder;

class TiposAltasBajasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_altas_bajas')->insert(
            [
                [
                    'nombre' => "Alta1",
                    'tipo' => "1",
                    'valor' => '10000.00',
                    
                ]
            ]
        );
    }
}
