<?php

use Illuminate\Database\Seeder;

class TiposContratosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_contratos')->insert(
            [
                [
                    'descripcion' => 'Contrato indefinido',
                    'caracteristicas'=>
                        'Se trata de un tipo de contrato que se establece sin una limitación temporal
                        en lo que respecta al período de realización del servicio. Dicho de otro modo,
                        en este tipo de contrato no se estipula una fecha de finalización.
                        Supone la existencia de estabilidad por parte del empleado, y en caso de que
                        el empleador decida dar por finalizada la relación laboral deberá
                        indemnizar al susodicho. '
                ],
                [
                    'descripcion' => 'Contrato temporal',
                    'caracteristicas'=>''
                ],
                [
                    'descripcion' => 'Contrato para la formación y el aprendizaje',
                    'caracteristicas'=>
                        'Se emplea este tipo de contrato en aquellas vinculaciones laborales que se
                        sabe que van a tener un inicio y final determinados, si bien la fecha de
                        finalización es incierta y se circunscribe a la finalización de un
                        determinado servicio.'
                ],
                [
                    'descripcion' => 'Eventual',
                    'caracteristicas'=>
                        'Este tipo de contrato, que como máximo ha de durar seis meses, es uno de los
                        más habituales en la actualidad. En principio este contrato se emplea en
                        aquellos momentos en que una empresa o empleador precisa de una ayuda
                        temporal debido a circunstancias imprevistas en las que se precisa de
                        mayor cantidad de trabajadores de lo habitual.'
                ],
                [
                    'descripcion' => 'De interinidad',
                    'caracteristicas'=>
                        'El contrato de interinidad es aquel cuyo principal objetivo es la
                        cobertura o sustitución de un puesto vacante temporalmente. La duración
                        del contrato cubre el tiempo de ausencia del trabajador o vacante a sustituir.
                        Suele realizarse ante la solicitud y concesión de bajas laborales a empleados,
                        vacaciones de éstos o mientras se está realizando un proceso de selección para
                        cubrir la vacante.'
                ],
                [
                    'descripcion' => 'De relevo',
                    'caracteristicas'=>
                        'Este tipo de contrato se emplea en situaciones en que es necesario sustituir
                        por un período determinado a una persona dentro de una empresa, la cual posee
                        una reducción de jornada debido a la jubilación parcial. De este modo, el
                        contrato se realiza para cubrir la parte de la jornada correspondiente a la
                        que deja de ejercer el trabajador sustituido.'
                ],
                [
                    'descripcion' => 'De formación y aprendizaje',
                    'caracteristicas'=>
                        'Este tipo de contrato sólo debería utilizarse con individuos entre dieciséis y
                        treinta años de edad (hasta los veinticinco si la tasa de paro baja del 15%).

                        Su principal función es la de permitir una alternancia entre actividad laboral
                        y formación, con lo que se pretende aumentar la inserción laboral a la vez que
                        se otorga la debida formación que permita ejercer adecuadamente. Como máximo
                        pueden llegar a durar hasta tres años, tras lo cual es posible
                        (si bien no obligatorio) pasar a entrar en plantilla de manera indefinida.
                        La retribución no ha de ser menor que el salario mínimo interprofesional,
                        siendo pactada por convenio.'
                ],
                [
                    'descripcion' => 'Contrato de prácticas',
                    'caracteristicas'=>
                        'De manera semejante al contrato de formación y aprendizaje, el contrato
                        de prácticas se realiza bajo la pretensión de mejorar la cualificación y
                        competencia profesional del empleado de cara a ejercer de manera eficiente.
                        Se vincula a una formación específica, ofreciendo experiencia en el sector
                        a la vez que permite una mejor comprensión de los contenidos formativos.
                        La retribución viene fijada por convenio, sin que sea posible que sea
                        inferior al 75% de lo que recibiría un trabajador en el mismo puesto. '
                ],
            ]
        );
    }
}
