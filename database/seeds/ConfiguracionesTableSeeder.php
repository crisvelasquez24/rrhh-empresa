<?php

use Illuminate\Database\Seeder;

class ConfiguracionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('configuraciones')->insert(
			[
                [
                    'tipo_dato' => 'Salud',
                    'siglas' => 'SAL',
                    'valores' => '4',                                     
                ], 
                [
                    'tipo_dato' => 'Pensión',
                    'siglas' => 'PEN',
                    'valores' => '4',                                     
                ], 
                [
                    'tipo_dato' => 'Auxilio de Transporte',
                    'siglas' => 'AUX',
                    'valores' => '88211',                                     
                ], 
                [
                    'tipo_dato' => 'Salario Minimo',
                    'siglas' => 'SALMIN',
                    'valores' => '782282',                                     
                ],                
			]
		);
    }
}
