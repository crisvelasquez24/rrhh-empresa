<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('PerfilesTableSeeder');
        $this->call('AreasInteresTableSeeder');
        $this->call('DepartamentosRhTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('PaisesTableSeeder');
        $this->call('DepartamentosTableSeeder');
        $this->call('CiudadesTableSeeder');
        $this->call('CargosTableSeeder');
        $this->call('TiposContratosTableSeeder');
        $this->call('TiposSalariosTableSeeder');
        $this->call('AdministradorasTableSeeder');
        $this->call('ConfiguracionesTableSeeder');
        $this->call('PeriodosNominaTableSeeder');

    }
}
