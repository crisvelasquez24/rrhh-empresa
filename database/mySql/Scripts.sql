-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.perfiles
CREATE TABLE IF NOT EXISTS `perfiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `perfiles_nombre_unique` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_perfil` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_name_unique` (`name`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_id_perfil_foreign` (`id_perfil`),
  CONSTRAINT `users_id_perfil_foreign` FOREIGN KEY (`id_perfil`) REFERENCES `perfiles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.vacantes
CREATE TABLE IF NOT EXISTS `vacantes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `localizacion` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requerimientos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salario` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vigencia` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.paises
CREATE TABLE IF NOT EXISTS `paises` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `abreviatura` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.periodos_nominas
CREATE TABLE IF NOT EXISTS `periodos_nominas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `periodo` int(11) NOT NULL,
  `mes` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.departamentos
CREATE TABLE IF NOT EXISTS `departamentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_pais` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `departamentos_id_pais_foreign` (`id_pais`),
  CONSTRAINT `departamentos_id_pais_foreign` FOREIGN KEY (`id_pais`) REFERENCES `paises` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.convocatorias
CREATE TABLE IF NOT EXISTS `convocatorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vacante` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `a_interes` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lugar` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `citacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.departamentos_rhs
CREATE TABLE IF NOT EXISTS `departamentos_rhs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `funcion` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.festivos
CREATE TABLE IF NOT EXISTS `festivos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dia_festivo` date NOT NULL,
  `nombre_festivo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ano` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.configuraciones
CREATE TABLE IF NOT EXISTS `configuraciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_dato` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `siglas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valores` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Volcando estructura para tabla rrhh.administradoras
CREATE TABLE IF NOT EXISTS `administradoras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.archivos
CREATE TABLE IF NOT EXISTS `archivos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contenido` longblob NOT NULL,
  `extension` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_archivo` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.area_interes
CREATE TABLE IF NOT EXISTS `area_interes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.ciudades
CREATE TABLE IF NOT EXISTS `ciudades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_departamento` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ciudades_id_departamento_foreign` (`id_departamento`),
  CONSTRAINT `ciudades_id_departamento_foreign` FOREIGN KEY (`id_departamento`) REFERENCES `departamentos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.cargos
CREATE TABLE IF NOT EXISTS `cargos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_departamento_rhs` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cargos_id_departamento_rhs_foreign` (`id_departamento_rhs`),
  CONSTRAINT `cargos_id_departamento_rhs_foreign` FOREIGN KEY (`id_departamento_rhs`) REFERENCES `departamentos_rhs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.control_empleados
CREATE TABLE IF NOT EXISTS `control_empleados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `control_epleados_id_users_foreign` (`id_user`),
  CONSTRAINT `control_epleados_id_users_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.capacitaciones
CREATE TABLE IF NOT EXISTS `capacitaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `lugar` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hora` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tema` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_cargo` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `capacitaciones_id_cargo_foreign` (`id_cargo`),
  CONSTRAINT `capacitaciones_id_cargo_foreign` FOREIGN KEY (`id_cargo`) REFERENCES `cargos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.hojas_vidas
CREATE TABLE IF NOT EXISTS `hojas_vidas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_nombre` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_nombre` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_apellido` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_apellido` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_doc` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_civil` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genero` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nac` date NOT NULL,
  `dir` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `movil` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_pais_nacimiento` int(10) unsigned NOT NULL,
  `id_departamento_nacimiento` int(10) unsigned NOT NULL,
  `id_ciudad_nacimiento` int(10) unsigned NOT NULL,
  `id_pais_actual` int(10) unsigned NOT NULL,
  `id_departamento_actual` int(10) unsigned NOT NULL,
  `id_ciudad_actual` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `id_area_interes` int(10) unsigned NOT NULL,
  `profesion` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perfil_laboral` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anos_experiencia` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aspiracion_salarial` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trabaja_actualmente` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posibilidad_translado` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posibilidad_viajar` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hojas_vidas_id_pais_nacimiento_foreign` (`id_pais_nacimiento`),
  KEY `hojas_vidas_id_departamento_nacimiento_foreign` (`id_departamento_nacimiento`),
  KEY `hojas_vidas_id_ciudad_nacimiento_foreign` (`id_ciudad_nacimiento`),
  KEY `hojas_vidas_id_pais_actual_foreign` (`id_pais_actual`),
  KEY `hojas_vidas_id_departamento_actual_foreign` (`id_departamento_actual`),
  KEY `hojas_vidas_id_ciudad_actual_foreign` (`id_ciudad_actual`),
  KEY `hojas_vidas_id_user_foreign` (`id_user`),
  KEY `hojas_vidas_id_area_interes_foreign` (`id_area_interes`),
  CONSTRAINT `hojas_vidas_id_area_interes_foreign` FOREIGN KEY (`id_area_interes`) REFERENCES `area_interes` (`id`),
  CONSTRAINT `hojas_vidas_id_ciudad_actual_foreign` FOREIGN KEY (`id_ciudad_actual`) REFERENCES `ciudades` (`id`),
  CONSTRAINT `hojas_vidas_id_ciudad_nacimiento_foreign` FOREIGN KEY (`id_ciudad_nacimiento`) REFERENCES `ciudades` (`id`),
  CONSTRAINT `hojas_vidas_id_departamento_actual_foreign` FOREIGN KEY (`id_departamento_actual`) REFERENCES `departamentos` (`id`),
  CONSTRAINT `hojas_vidas_id_departamento_nacimiento_foreign` FOREIGN KEY (`id_departamento_nacimiento`) REFERENCES `departamentos` (`id`),
  CONSTRAINT `hojas_vidas_id_pais_actual_foreign` FOREIGN KEY (`id_pais_actual`) REFERENCES `paises` (`id`),
  CONSTRAINT `hojas_vidas_id_pais_nacimiento_foreign` FOREIGN KEY (`id_pais_nacimiento`) REFERENCES `paises` (`id`),
  CONSTRAINT `hojas_vidas_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.liquidaciones
CREATE TABLE IF NOT EXISTS `liquidaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cesantias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prima` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intereses` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacaciones` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor_liquidacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_empleado` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `liquidaciones_id_empleado_foreign` (`id_empleado`),
  CONSTRAINT `liquidaciones_id_empleado_foreign` FOREIGN KEY (`id_empleado`) REFERENCES `hojas_vidas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;





-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.nomina_generadas
CREATE TABLE IF NOT EXISTS `nomina_generadas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `total` decimal(17,2) NOT NULL DEFAULT '0.00',
  `salud` decimal(17,2) NOT NULL DEFAULT '0.00',
  `pension` decimal(17,2) NOT NULL DEFAULT '0.00',
  `auxilio` decimal(17,2) NOT NULL DEFAULT '0.00',
  `periodo` int(11) NOT NULL,
  `ano` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_empleado` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nomina_generadas_id_empleado_foreign` (`id_empleado`),
  CONSTRAINT `nomina_generadas_id_empleado_foreign` FOREIGN KEY (`id_empleado`) REFERENCES `hojas_vidas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.vacaciones
CREATE TABLE IF NOT EXISTS `vacaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_hojas_vidas` int(10) unsigned NOT NULL,
  `numero_dias` int(11) NOT NULL,
  `fecha_salida` date NOT NULL,
  `fecha_entrada` date NOT NULL,
  `remuneracion` int(11) NOT NULL,
  `observaciones` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vacaciones_id_hojas_vidas_foreign` (`id_hojas_vidas`),
  CONSTRAINT `vacaciones_id_hojas_vidas_foreign` FOREIGN KEY (`id_hojas_vidas`) REFERENCES `hojas_vidas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.tipos_salarios
CREATE TABLE IF NOT EXISTS `tipos_salarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caracteristicas` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.tipos_contratos
CREATE TABLE IF NOT EXISTS `tipos_contratos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caracteristicas` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.tipos_altas_bajas
CREATE TABLE IF NOT EXISTS `tipos_altas_bajas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  `valor` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.preguntares
CREATE TABLE IF NOT EXISTS `preguntares` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.postulados
CREATE TABLE IF NOT EXISTS `postulados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_hoja_vida` int(10) unsigned NOT NULL,
  `id_vacante` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `postulados_id_hoja_vida_foreign` (`id_hoja_vida`),
  KEY `postulados_id_vacante_foreign` (`id_vacante`),
  CONSTRAINT `postulados_id_hoja_vida_foreign` FOREIGN KEY (`id_hoja_vida`) REFERENCES `hojas_vidas` (`id`),
  CONSTRAINT `postulados_id_vacante_foreign` FOREIGN KEY (`id_vacante`) REFERENCES `vacantes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.formaciones
CREATE TABLE IF NOT EXISTS `formaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nivel_estudios` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_estudios` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo_estudios` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institucion_estudios` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais_estudios` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departamento_estudios` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad_estudios` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `funciones_estudios` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_hoja` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `formaciones_id_hoja_foreign` (`id_hoja`),
  CONSTRAINT `formaciones_id_hoja_foreign` FOREIGN KEY (`id_hoja`) REFERENCES `hojas_vidas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.experiencias_laborales
CREATE TABLE IF NOT EXISTS `experiencias_laborales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `empresa` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_empresa` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sector_empresa` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo_empresa` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jefe_empresa` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_ingreso_empresa` date NOT NULL,
  `fecha_finalizacion_empresa` date NOT NULL,
  `tiempo_experiencia` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais_empresa` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departamento_empresa` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad_empresa` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `funciones_empresa` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_hoja` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `experiencias_laborales_id_hoja_foreign` (`id_hoja`),
  CONSTRAINT `experiencias_laborales_id_hoja_foreign` FOREIGN KEY (`id_hoja`) REFERENCES `hojas_vidas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.evaluars
CREATE TABLE IF NOT EXISTS `evaluars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_psicologica` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_conocimiento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entrevista` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_hojadevida` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `evaluars_id_hojadevida_foreign` (`id_hojadevida`),
  CONSTRAINT `evaluars_id_hojadevida_foreign` FOREIGN KEY (`id_hojadevida`) REFERENCES `hojas_vidas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.evaluacion_rendimiento
CREATE TABLE IF NOT EXISTS `evaluacion_rendimiento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rta` int(11) NOT NULL,
  `id_hojadevida` int(10) unsigned NOT NULL,
  `id_preguntare` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `evaluacion_rendimiento_id_hojadevida_foreign` (`id_hojadevida`),
  KEY `evaluacion_rendimiento_id_preguntare_foreign` (`id_preguntare`),
  CONSTRAINT `evaluacion_rendimiento_id_hojadevida_foreign` FOREIGN KEY (`id_hojadevida`) REFERENCES `hojas_vidas` (`id`),
  CONSTRAINT `evaluacion_rendimiento_id_preguntare_foreign` FOREIGN KEY (`id_preguntare`) REFERENCES `preguntares` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.evaluaciones
CREATE TABLE IF NOT EXISTS `evaluaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op1_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op1_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op1_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op2_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op2_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op2_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op3_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op3_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op3_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op4_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op4_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op4_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op5_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op5_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op5_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p6` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op6_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op6_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op6_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p7` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op7_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op7_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op7_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p8` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op8_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op8_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op8_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p9` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op9_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op9_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op9_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p10` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op10_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op10_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `op10_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp1` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp2` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp3` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp4` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp5` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp6` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp7` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp8` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp9` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp10` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_cargo` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `evaluaciones_id_cargo_foreign` (`id_cargo`),
  CONSTRAINT `evaluaciones_id_cargo_foreign` FOREIGN KEY (`id_cargo`) REFERENCES `cargos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.contratos
CREATE TABLE IF NOT EXISTS `contratos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `documentos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `salario_basico` decimal(17,2) NOT NULL,
  `aporte_pension` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aporte_salud` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aporte_riesgos` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aporte_parafiscales` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_tipo_contrato` int(10) unsigned NOT NULL,
  `id_tipo_salario` int(10) unsigned NOT NULL,
  `id_cargo` int(10) unsigned NOT NULL,
  `id_hojavida` int(10) unsigned NOT NULL,
  `id_administradora_salud` int(10) unsigned NOT NULL,
  `id_administradora_pension` int(10) unsigned NOT NULL,
  `id_pais` int(10) unsigned NOT NULL,
  `id_departamento` int(10) unsigned NOT NULL,
  `id_ciudad` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contratos_id_departamento_foreign` (`id_departamento`),
  KEY `contratos_id_ciudad_foreign` (`id_ciudad`),
  KEY `contratos_id_pais_foreign` (`id_pais`),
  KEY `contratos_id_cargo_foreign` (`id_cargo`),
  KEY `contratos_id_hojavida_foreign` (`id_hojavida`),
  KEY `contratos_id_tipo_salario_foreign` (`id_tipo_salario`),
  KEY `contratos_id_tipo_contrato_foreign` (`id_tipo_contrato`),
  KEY `contratos_id_administradora_salud_foreign` (`id_administradora_salud`),
  KEY `contratos_id_administradora_pension_foreign` (`id_administradora_pension`),
  CONSTRAINT `contratos_id_administradora_pension_foreign` FOREIGN KEY (`id_administradora_pension`) REFERENCES `administradoras` (`id`),
  CONSTRAINT `contratos_id_administradora_salud_foreign` FOREIGN KEY (`id_administradora_salud`) REFERENCES `administradoras` (`id`),
  CONSTRAINT `contratos_id_cargo_foreign` FOREIGN KEY (`id_cargo`) REFERENCES `cargos` (`id`),
  CONSTRAINT `contratos_id_ciudad_foreign` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudades` (`id`),
  CONSTRAINT `contratos_id_departamento_foreign` FOREIGN KEY (`id_departamento`) REFERENCES `departamentos` (`id`),
  CONSTRAINT `contratos_id_hojavida_foreign` FOREIGN KEY (`id_hojavida`) REFERENCES `hojas_vidas` (`id`),
  CONSTRAINT `contratos_id_pais_foreign` FOREIGN KEY (`id_pais`) REFERENCES `paises` (`id`),
  CONSTRAINT `contratos_id_tipo_contrato_foreign` FOREIGN KEY (`id_tipo_contrato`) REFERENCES `tipos_contratos` (`id`),
  CONSTRAINT `contratos_id_tipo_salario_foreign` FOREIGN KEY (`id_tipo_salario`) REFERENCES `tipos_salarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.bajas
CREATE TABLE IF NOT EXISTS `bajas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comentario` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` date NOT NULL,
  `id_contrato` int(10) unsigned NOT NULL,
  `id_tipos_altas_bajas` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bajas_id_contrato_foreign` (`id_contrato`),
  KEY `bajas_id_tipos_altas_bajas_foreign` (`id_tipos_altas_bajas`),
  CONSTRAINT `bajas_id_contrato_foreign` FOREIGN KEY (`id_contrato`) REFERENCES `contratos` (`id`),
  CONSTRAINT `bajas_id_tipos_altas_bajas_foreign` FOREIGN KEY (`id_tipos_altas_bajas`) REFERENCES `contratos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.asignar_evaluaciones
CREATE TABLE IF NOT EXISTS `asignar_evaluaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_users` int(10) unsigned NOT NULL,
  `id_evaluaciones` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asignar_evaluaciones_id_users_foreign` (`id_users`),
  KEY `asignar_evaluaciones_id_evaluaciones_foreign` (`id_evaluaciones`),
  CONSTRAINT `asignar_evaluaciones_id_evaluaciones_foreign` FOREIGN KEY (`id_evaluaciones`) REFERENCES `evaluaciones` (`id`),
  CONSTRAINT `asignar_evaluaciones_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;





-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.hojasvidasconvocatorias
CREATE TABLE IF NOT EXISTS `hojasvidasconvocatorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_hojavida` int(10) unsigned NOT NULL,
  `id_convocatoria` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hojasvidasconvocatorias_id_hojavida_foreign` (`id_hojavida`),
  KEY `hojasvidasconvocatorias_id_convocatoria_foreign` (`id_convocatoria`),
  CONSTRAINT `hojasvidasconvocatorias_id_convocatoria_foreign` FOREIGN KEY (`id_convocatoria`) REFERENCES `convocatorias` (`id`),
  CONSTRAINT `hojasvidasconvocatorias_id_hojavida_foreign` FOREIGN KEY (`id_hojavida`) REFERENCES `hojas_vidas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.registro_altas_bajas
CREATE TABLE IF NOT EXISTS `registro_altas_bajas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_contrato` int(10) unsigned NOT NULL,
  `id_tipos_altas_bajas` int(10) unsigned NOT NULL,
  `fecha` date NOT NULL,
  `comentario` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_altas_bajas_id_contrato_foreign` (`id_contrato`),
  KEY `registro_altas_bajas_id_tipos_altas_bajas_foreign` (`id_tipos_altas_bajas`),
  CONSTRAINT `registro_altas_bajas_id_contrato_foreign` FOREIGN KEY (`id_contrato`) REFERENCES `contratos` (`id`),
  CONSTRAINT `registro_altas_bajas_id_tipos_altas_bajas_foreign` FOREIGN KEY (`id_tipos_altas_bajas`) REFERENCES `tipos_altas_bajas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rrhh.respuestas
CREATE TABLE IF NOT EXISTS `respuestas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rp1` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp2` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp3` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp4` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp5` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp6` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp7` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp8` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp9` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rp10,10` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calificacion` int(11) NOT NULL,
  `tiempo` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_users` int(10) unsigned NOT NULL,
  `id_evaluaciones` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `respuestas_id_users_foreign` (`id_users`),
  KEY `respuestas_id_evaluaciones_foreign` (`id_evaluaciones`),
  CONSTRAINT `respuestas_id_evaluaciones_foreign` FOREIGN KEY (`id_evaluaciones`) REFERENCES `evaluaciones` (`id`),
  CONSTRAINT `respuestas_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

