<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostuladosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_hoja_vida')->unsigned();            
            $table->integer('id_vacante')->unsigned();
            $table->foreign('id_hoja_vida')->references('id')->on('hojas_vidas');
            $table->foreign('id_vacante')->references('id')->on('vacantes');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulados');
    }
}
