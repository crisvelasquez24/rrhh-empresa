<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_hojas_vidas')->unsigned();
            $table->foreign('id_hojas_vidas')->references('id')->on('hojas_vidas');
            $table->integer('numero_dias');
            $table->date('fecha_salida');
            $table->date('fecha_entrada');
            $table->integer('remuneracion');
            $table->string('observaciones',300)->nullable();
            $table->integer('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacaciones');
    }
}
