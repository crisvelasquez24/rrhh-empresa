<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiquidacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquidaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cesantias');
            $table->string('prima');
            $table->string('intereses');
            $table->string('vacaciones');
            $table->string('valor_liquidacion');
            $table->integer('id_empleado')->unsigned();
            $table->foreign('id_empleado')->references('id')->on('hojas_vidas');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liquidaciones');
    }
}
