<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluacionRendimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluacion_rendimiento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rta');
            $table->integer('id_hojadevida')->unsigned();
            $table->integer('id_preguntare')->unsigned();

            $table->foreign('id_hojadevida')->references('id')->on('hojas_vidas');
            $table->foreign('id_preguntare')->references('id')->on('preguntares');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluacion_rendimiento');
    }
}
