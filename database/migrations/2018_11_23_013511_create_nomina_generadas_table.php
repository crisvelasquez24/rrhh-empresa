<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNominaGeneradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nomina_generadas', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('total', 17, 2)->default(0);
            $table->decimal('salud',17, 2)->default(0);
            $table->decimal('pension', 17, 2)->default(0);
            $table->decimal('auxilio', 17, 2)->default(0);
            $table->integer('periodo');
            $table->string('ano', 4);
            $table->integer('id_empleado')->unsigned();
            $table->foreign('id_empleado')->references('id')->on('hojas_vidas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nomina_generadas');
    }
}
