<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->string('documentos');
            $table->integer('estado');//1 vigente 2Retirado 3vacaiones 4suspendido
            $table->decimal('salario_basico', 17, 2);
            $table->string('aporte_pension',30);
            $table->string('aporte_salud',30);
            $table->string('aporte_riesgos',30);
            $table->string('aporte_parafiscales',30);
            $table->integer('id_tipo_contrato')->unsigned();
            $table->integer('id_tipo_salario')->unsigned();
            $table->integer('id_cargo')->unsigned();
            $table->integer('id_hojavida')->unsigned();
            $table->integer('id_administradora_salud')->unsigned();
            $table->integer('id_administradora_pension')->unsigned();
            $table->integer('id_pais')->unsigned();
            $table->integer('id_departamento')->unsigned();
            $table->integer('id_ciudad')->unsigned();
            $table->foreign('id_departamento')->references('id')->on('departamentos');
            $table->foreign('id_ciudad')->references('id')->on('ciudades');
            $table->foreign('id_pais')->references('id')->on('paises');
            $table->foreign('id_cargo')->references('id')->on('cargos');
            $table->foreign('id_hojavida')->references('id')->on('hojas_vidas');
            $table->foreign('id_tipo_salario')->references('id')->on('tipos_salarios');
            $table->foreign('id_tipo_contrato')->references('id')->on('tipos_contratos');
            $table->foreign('id_administradora_salud')->references('id')->on('administradoras');
            $table->foreign('id_administradora_pension')->references('id')->on('administradoras');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
