<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bajas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comentario',300)->nullable();
            $table->date('fecha');
            $table->integer('id_contrato')->unsigned(); //nombre que va a tomar en la tabla hija
            $table->foreign('id_contrato')->references('id')->on('contratos'); // relacion de la tabla padre
            $table->integer('id_tipos_altas_bajas')->unsigned();
            $table->foreign('id_tipos_altas_bajas')->references('id')->on('contratos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bajas');
    }
}
