<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienciasLaboralesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiencias_laborales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa',25);
            $table->string('telefono_empresa',25);
            $table->string('sector_empresa',25);
            $table->string('cargo_empresa',25);
            $table->string('jefe_empresa',25);
            $table->date('fecha_ingreso_empresa');
            $table->date('fecha_finalizacion_empresa');
            $table->string('tiempo_experiencia',20);
            $table->string('pais_empresa',30);
            $table->string('departamento_empresa',30);
            $table->string('ciudad_empresa',30);
            $table->string('funciones_empresa',2000);
            $table->integer('id_hoja')->unsigned();
            $table->foreign('id_hoja')->references('id')->on('hojas_vidas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiencias_laborales');
    }
}
