<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nivel_estudios',45);
            $table->string('area_estudios',45);
            $table->string('titulo_estudios',45);
            $table->string('institucion_estudios',50);
            $table->string('pais_estudios',45);
            $table->string('departamento_estudios',45);
            $table->string('ciudad_estudios',45);
            $table->string('funciones_estudios',2000);    
            $table->integer('id_hoja')->unsigned();
            $table->foreign('id_hoja')->references('id')->on('hojas_vidas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formaciones');
    }
}
