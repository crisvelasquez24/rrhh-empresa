<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHojasVidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hojas_vidas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foto');
            $table->string('p_nombre',20)->nullable();
            $table->string('s_nombre',20);
            $table->string('p_apellido',20)->nullable();
            $table->string('s_apellido',20);
            $table->string('tipo_doc',20);
            $table->string('doc',10);
            $table->string('estado_civil',20);
            $table->string('genero',20);
            $table->string('email',30);
            $table->date('fecha_nac');
            $table->string('dir',30);
            $table->string('tel',10)->nullable();
            $table->string('movil',15)->nullable();
            $table->integer('id_pais_nacimiento')->unsigned();
            $table->integer('id_departamento_nacimiento')->unsigned();
            $table->integer('id_ciudad_nacimiento')->unsigned();
            $table->integer('id_pais_actual')->unsigned();;
            $table->integer('id_departamento_actual')->unsigned();
            $table->integer('id_ciudad_actual')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->integer('id_area_interes')->unsigned();

            $table->foreign('id_pais_nacimiento')->references('id')->on('paises');
            $table->foreign('id_departamento_nacimiento')->references('id')->on('departamentos');
            $table->foreign('id_ciudad_nacimiento')->references('id')->on('ciudades');
            $table->foreign('id_pais_actual')->references('id')->on('paises');
            $table->foreign('id_departamento_actual')->references('id')->on('departamentos');
            $table->foreign('id_ciudad_actual')->references('id')->on('ciudades');
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_area_interes')->references('id')->on('area_interes');


            $table->string('profesion',50);
            $table->string('perfil_laboral',2000);
            $table->string('anos_experiencia',25);
            $table->string('aspiracion_salarial',25);
            $table->string('trabaja_actualmente',2);
            $table->string('posibilidad_translado',2);
            $table->string('posibilidad_viajar',2);

            // $table->string('empresa',25);
            // $table->string('telefono_empresa',25);
            // $table->string('sector_empresa',25);
            // $table->string('cargo_empresa',25);
            // $table->string('jefe_empresa',25);
            // $table->date('fecha_ingreso_empresa');
            // $table->date('fecha_finalizacion_empresa');
            // $table->string('tiempo_experiencia',20);
            // $table->string('pais_empresa',30);
            // $table->string('departamento_empresa',30);
            // $table->string('ciudad_empresa',30);
            // $table->string('funciones_empresa',2000);

            // $table->string('nivel_estudios',45);
            // $table->string('area_estudios',45);
            // $table->string('titulo_estudios',45);
            // $table->string('institucion_estudios',50);
            // $table->string('pais_estudios',45);
            // $table->string('departamento_estudios',45);
            // $table->string('ciudad_estudios',45);
            // $table->string('funciones_estudios',2000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hojas_vidas');
    }
}
