<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Hojadevida extends Model
{

    protected $table = 'hojas_vidas';

    protected $fillable = [
        'id',
        'foto',
        'p_nombre',
        's_nombre',
        'p_apellido',
        's_apellido',
        'tipo_doc',
        'doc',
        'estado_civil',
        'genero',
        'email',
        'fecha_nac',
        'dir',
        'tel',
        'movil',
        'id_pais_nacimiento',
        'id_departamento_nacimiento',
        'id_ciudad_nacimiento',
        'id_pais_actual',
        'id_departamento_actual',
        'id_ciudad_actual',
        'id_user',
        'id_area_interes',
        'profesion',
        'perfil_laboral',
        'anos_experiencia',
        'aspiracion_salarial',
        'trabaja_actualmente',
        'posibilidad_translado',
        'posibilidad_viajar',
    ]
    ;

    public function setFechaNacAttribute($value)
    {
        $this->attributes['fecha_nac'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }
}
