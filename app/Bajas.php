<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bajas extends Model
{
     protected $table = 'bajas';
    protected $fillable = ['id','id_contrato','id_tipos_altas_bajas','fecha','comentario'];
}
