<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area_interes extends Model {
    
    protected $table = 'area_interes';
    protected $fillable = ['id','nombre'];
    
}
