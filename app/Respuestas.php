<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuestas extends Model {

    protected $table = 'respuestas';
    protected $fillable = [
        'id', 'rp1', 'rp2', 'rp3', 'rp4',
        'rp5', 'rp6', 'rp7', 'rp8', 'rp9', 'rp10', 'id_evaluaciones', 'id_users', 'calificacion', 'tiempo'
    ];

}
