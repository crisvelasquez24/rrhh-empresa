<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Vacantes extends Model {

    protected $table = 'vacantes';
    protected $fillable = [
        'id',
         'nombre',
         'descripcion',
         'localizacion',
         'requerimientos',
         'salario',
         'vigencia'
     ];

    public function setVigenciaAttribute($value)
    {
        $this->attributes['vigencia'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }

    public function estado_postulado($id_vacante, $id_user) {
        $id_hojadevida_user = Hojadevida::select('id')->where('id_user', $id_user)->first();

        if (!$id_hojadevida_user) {
            return "NOHOJA";
        }
        $id_hojadevida = $id_hojadevida_user->id;
        $postulados = Postulados::select()
                ->where('id_hoja_vida', $id_hojadevida)
                ->where('id_vacante', $id_vacante)
                ->count();
        if ($postulados > 0) {
            return "POSTULADO";
        }
    }

}
