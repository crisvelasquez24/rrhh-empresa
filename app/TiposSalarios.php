<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposSalarios extends Model
{
    protected $table = 'tipos_salarios';
    protected $fillable = ['id','descripcion','caracteristicas'];
}
