<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignar_evaluacion extends Model {
    
    protected $table = 'asignar_evaluaciones';
    protected $fillable = ['id', 'id_users','id_evaluaciones'];
    
}
