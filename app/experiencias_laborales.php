<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class experiencias_laborales extends Model
{
    protected $table = 'experiencias_laborales';
    protected $fillable = [
        'id',
        'empresa',
        'telefono_empresa',
        'sector_empresa',
        'cargo_empresa',
        'jefe_empresa',
        'fecha_ingreso_empresa',
        'fecha_finalizacion_empresa',
        'tiempo_experiencia',
        'pais_empresa',
        'departamento_empresa',
        'ciudad_empresa',
        'funciones_empresa',
        'id_hoja',
    ];


    public function setFechaIngresoEmpresaAttribute($value)
    {
        $this->attributes['fecha_ingreso_empresa'] 
            = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }

    public function setFechaFinalizacionEmpresaAttribute($value)
    {
        $this->attributes['fecha_finalizacion_empresa'] 
            = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }
}
