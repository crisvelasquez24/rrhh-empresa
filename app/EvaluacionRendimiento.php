<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluacionRendimiento extends Model
{
    protected $table = 'evaluacion_rendimiento';
    protected $fillable = [
    	'id',
    	'id_hojadevida',
    	'id_preguntare',
    	'rta',
    ];
}
