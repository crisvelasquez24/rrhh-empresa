<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Administradoras extends Model
{
    protected $table = 'administradoras';
    protected $fillable = ['id','nombre','tipo'];
    
}
