<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preguntares extends Model
{
    protected $table    = 'preguntares';
    protected $fillable = [
        'id','categoria','p1','p2'
    ];//
}
