<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodosNomina extends Model
{
    protected $table = 'periodos_nominas';
    protected $fillable = ['id', 'periodo', 'mes'];
}
