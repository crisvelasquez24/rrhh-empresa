<?php

namespace App\Custom;

class Festivos
{
    private $hoy;
    public $festivos;
    private $ano;
    private $pascua_mes;
    private $pascua_dia;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ano)
    {
        $this->ano = $ano;
        $this->festivos($ano);
    }

    public function getFestivos($ano = '')
    {
        $this->festivos($ano);
        return $this->festivos;
    }

    public function festivos($ano = '')
    {
        $this->hoy = date('d/m/Y');

        if ($ano == '') {
            $ano = date('Y');
        }

        $this->ano = $ano;

        $this->pascua_mes = date("m", easter_date($this->ano));
        $this->pascua_dia = date("d", easter_date($this->ano));

        $this->festivos[] = [$this->ano . '-1' . '-1', 'Año Nuevo']; //'AÃ±o Nuevo'
        $this->festivos[] = [$this->ano . '-5' . '-1', 'Día del Trabajo']; // Dia del Trabajo
        $this->festivos[] = [$this->ano . '-7' . '-20', 'Independencia']; // Independencia
        $this->festivos[] = [$this->ano . '-8' . '-7', 'Batalla de Boyacá']; // Batalla de Boyacá
        $this->festivos[] = [$this->ano . '-12' . '-8', 'Maria Inmaculada']; // Maria Inmaculada
        $this->festivos[] = [$this->ano . '-12' . '-25', 'Navidad']; // Navidad
        $this->festivos[] = [$this->calcula_emiliani(1, 6), 'Reyes Magos']; // Reyes Magos
        $this->festivos[] = [$this->calcula_emiliani(3, 19), 'San José']; // San Jose
        $this->festivos[] = [$this->calcula_emiliani(6, 29), 'San Pedro y San Pablo']; // San Pedro y San Pablo
        $this->festivos[] = [$this->calcula_emiliani(8, 15), 'Asunción']; // AsunciÃ³n
        $this->festivos[] = [$this->calcula_emiliani(10, 12), 'Descubrimiento de América']; // Descubrimiento de América
        $this->festivos[] = [$this->calcula_emiliani(11, 1), 'Todos los santos']; // Todos los santos
        $this->festivos[] = [$this->calcula_emiliani(11, 11), 'Independencia de Cartagena'];

        /**
         * Independencia de Cartagena
         */

        /**
         * otras fechas calculadas a partir de la pascua.
         */

        $this->festivos[] = [$this->otrasFechasCalculadas(-3), 'jueves santo']; //jueves santo
        $this->festivos[] = [$this->otrasFechasCalculadas(-2), 'viernes santo']; //viernes santo

        $this->festivos[] = [$this->otrasFechasCalculadas(43, true), 'Ascensión de Señor (pascua)']; //Ascensión de Señor (pascua)
        $this->festivos[] = [$this->otrasFechasCalculadas(64, true), 'Corpus Christi']; //Corpus Cristi
        $this->festivos[] = [$this->otrasFechasCalculadas(71, true), 'Sagrado Corazón']; //Sagrado Corazón
    }

    protected function calcula_emiliani($mes_festivo, $dia_festivo)
    {

        /**
         * funcion que mueve una fecha diferente a lunes al siguiente lunes en el
         */

        /**
         * calendario y se aplica a fechas que estan bajo la ley emiliani
         */

        /**
         * global  $y,$dia_festivo,$mes_festivo,$festivo;
         */

        /**
         * Extrae el dia de la semana
         */

        /**
         * 0 Domingo Â… 6 SÃ¡bado
         */
        $dd = date("w", mktime(0, 0, 0, $mes_festivo, $dia_festivo, $this->ano));

        switch ($dd) {
        case 0: // Domingo
            $dia_festivo = $dia_festivo + 1;
            break;
        case 2: // Martes.
            $dia_festivo = $dia_festivo + 6;
            break;
        case 3: // MiÃ©rcoles
            $dia_festivo = $dia_festivo + 5;
            break;
        case 4: // Jueves
            $dia_festivo = $dia_festivo + 4;
            break;
        case 5: // Viernes
            $dia_festivo = $dia_festivo + 3;
            break;
        case 6: // SÃ¡bado
            $dia_festivo = $dia_festivo + 2;
            break;
        }

        $mes = date("n", mktime(0, 0, 0, $mes_festivo, $dia_festivo, $this->ano)) + 0;
        $dia = date("d", mktime(0, 0, 0, $mes_festivo, $dia_festivo, $this->ano)) + 0;
        return $this->ano . '-' . $mes . '-' . $dia;
    }

    protected function otrasFechasCalculadas($cantidadDias = 0, $siguienteLunes = false)
    {
        $mes_festivo = date("n", mktime(0, 0, 0, $this->pascua_mes, $this->pascua_dia + $cantidadDias, $this->ano));
        $dia_festivo = date("d", mktime(0, 0, 0, $this->pascua_mes, $this->pascua_dia + $cantidadDias, $this->ano));

        if ($siguienteLunes) {
            return $this->calcula_emiliani($mes_festivo, $dia_festivo);
        } else {
            return $this->ano . '-' . ($mes_festivo + 0) . '-' . ($dia_festivo + 0);
        }

    }

}
