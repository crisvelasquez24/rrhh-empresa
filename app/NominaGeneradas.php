<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NominaGeneradas extends Model
{

    protected $table = 'nomina_generadas';
    protected $fillable = [
        'id',
        'total',
        'salud',
        'pension',
        'auxilio',
        'periodo',
        'ano',
        'id_empleado',
    ];

}
