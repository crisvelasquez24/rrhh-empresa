<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Liquidaciones extends Model
{
    protected $table = 'liquidaciones';
    protected $fillable = [
        'id',
        'cesantias',
        'prima',
        'intereses',
        'vacaciones',
        'valor_liquidacion',
        'id_empleado',
    ];    
}
