<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamentos_rh extends Model {

    protected $table = 'departamentos_rhs';
    protected $fillable = ['id', 'nombre', 'funcion'];

}
