<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluaciones extends Model {
    
    protected $table    = 'evaluaciones';
    protected $fillable = [
        'id', 'id_cargo', 'p1', 'op1_a', 'op1_b', 'op1_c',
        'p2', 'op2_a', 'op2_b', 'op2_c',
        'p3', 'op3_a', 'op3_b', 'op3_c',
        'p4', 'op4_a', 'op4_b', 'op4_c',
        'p5', 'op5_a', 'op5_b', 'op5_c',
        'p6', 'op6_a', 'op6_b', 'op6_c',
        'p7', 'op7_a', 'op7_b', 'op7_c',
        'p8', 'op8_a', 'op8_b', 'op8_c',
        'p9', 'op9_a', 'op9_b', 'op9_c',
        'p10', 'op10_a', 'op10_b', 'op10_c',
        'rp1', 'rp2', 'rp3', 'rp4',
        'rp5', 'rp6', 'rp7', 'rp8', 'rp9', 'rp10',
        'nombre'
    ];
    
}
