<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class controlEmpleados extends Model
{
    protected $table = 'control_empleados';
    protected $fillable =
        [
        'id',
        'id_user'
    ]
    ;    
}
