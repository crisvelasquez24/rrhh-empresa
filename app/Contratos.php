<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Contratos extends Model
{

    protected $table = 'contratos';
    protected $fillable =
        [
        'id',
        'fecha_inicio',
        'fecha_fin',
        'documentos',
        'estado',
        'salario_basico',
        'aporte_pension',
        'aporte_salud',
        'aporte_riesgos',
        'aporte_parafiscales',
        'id_tipo_contrato',
        'id_tipo_salario',
        'id_cargo',
        'id_hojavida',
        'id_administradora_salud',
        'id_administradora_pension',
        'id_pais',
        'id_departamento',
        'id_ciudad',
    ]
    ;

    // public function setDocumentosAttribute($foto)
    // {
    //     if (!empty($foto)) {
    //         $this->attributes['documentos']
    //             = Carbon::now()->second . Carbon::now()->format('Ymd')  . $foto->getClientOriginalName();
    //         // $name = Carbon::now()->second . $foto->getClientOriginalName();
    //         // \Storage::disk('imgContrato')->put($name, \File::get($foto));
    //     }

    // }

    public function setFechaInicioAttribute($value)
    {
        $this->attributes['fecha_inicio'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }

    public function setFechaFinAttribute($value)
    {
        $this->attributes['fecha_fin'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }

    public function verificar_estado($id)
    {
        $fecha_actual = date('Y-m-d');
        $contratos = Contratos::select()
            ->where('id', $id)
            ->whereBetween('fecha_fin', ['fecha_inicio', $fecha_actual])
            ->count();

        $contrato_update = Contratos::find($id);

        if ($contratos == 0 && $contrato_update->estado != 3) {
            $contrato_update->fill([
                'estado' => 1,
            ]);
            $contrato_update->save();
            return $contrato_update->estado;
        } else
        if ($contratos != 0 && $contrato_update->estado != 3) {
            $contrato_update->fill([
                'estado' => 2,
            ]);
            $contrato_update->save();
            return $contrato_update->estado;
        }

    }

}
