<?php

namespace App\Http\Controllers;
use App\Administradoras;
use Session;

use Illuminate\Http\Request;

class AdministradorasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $administradoras = Administradoras::all();
        return view('administradoras.administrar', compact('administradoras'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administradoras.crear');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $administradoras = Administradoras::create($request->all());
        if($administradoras){
            Session::flash('message-success',$request['nombre'].'fue creado correctamente');
        }else{
            Session::flash('message-error','No pudo ser creado');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $administradora = Administradoras::find($id);
        return view('administradoras.actualizar', compact('administradora'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $administradora = Administradoras::find($id);
        $administradora->fill($request->all());
        if ($administradora->save()) {
            Session::flash('message-success', 'El Administradora '.$request['nombre'].' fue actualizado correctamente');
        } else {
            Session::flash('message-error', 'Error al editar el Administradora '.$request['nombre']);
        }
        return $this->retorno();        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre,$id)
    {
        if (Administradoras::destroy($id)) {
            Session::flash('message-success', 'Administradora '.$nombre.' fue eliminadada correctamente');
        } else {
            Session::flash('message-error', 'Error al eliminar el Administradora '.$nombre);
        }
        return redirect('/administradora');
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/administradora'; \n";
        $script.= "</script>\n";
        echo $script;
    }
}
