<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Festivos;
use DB;
use Session;

class FestivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $festivos = Festivos::all();
        return view('festivos.administrar', compact('festivos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $update = 0;
        return view('festivos.crear', compact('update'));

// $festivos = new \App\Custom\Festivos('2018');

// for ($i=0; $i < count($festivos->festivos); $i++) {

// $datos[] = array(

// 'dia_festivo' => $festivos->festivos[$i][0],

// 'nombre_festivo' => $festivos->festivos[$i][1],

// );

// }

// DB::table('festivos')->insert($datos);

// // Festivos::create($datos);
        // dd("hola");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exist = DB::table('festivos')->where('ano', $request['ano'])->exists();

        if ($exist) {
            $update = 0;
            Session::flash('message-error', 'Año ' . $request['ano'] . ' ya existe Registrado');
            return view('festivos.crear' ,compact("update"));
        }

        $festivos = new \App\Custom\Festivos($request['ano']);

        for ($i = 0; $i < count($festivos->festivos); $i++) {
            $datos[] = array(
                'dia_festivo'    => $festivos->festivos[$i][0],
                'nombre_festivo' => $festivos->festivos[$i][1],
                'ano'            => $request['ano'],
            );
        }

        $festivos = DB::table('festivos')->insert($datos);

        if ($festivos) {
            Session::flash('message-success', $request['nombre'] . 'fue creado correctamente');
        } else {
            Session::flash('message-error', 'No pudo ser creado');
        }

        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $festivos = Festivos::find($id);
        $update = 1;
        return view('festivos.actualizar', compact('festivos', 'update'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $festivos = Festivos::find($id);
        $festivos->fill($request->all());

        if ($festivos->save()) {
            Session::flash('message-success', 'El Festivo ' . $request['nombre_festivo'] . ' fue actualizado correctamente');
        } else {
            Session::flash('message-error', 'Error al editar el Festivo ' . $request['nombre_festivo']);
        }

        return $this->retorno();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre, $id)
    {

        if (Festivos::destroy($id)) {
            Session::flash('message-success', 'festivo ' . $nombre . ' fue eliminadado correctamente');
        } else {
            Session::flash('message-error', 'Error al eliminar el festivo ' . $nombre);
        }

        return redirect('/festivos');
    }

    public function retorno()
    {
        $script = "<script>\n";
        $script .= "window.parent.location.href = '/festivos'; \n";
        $script .= "</script>\n";
        echo $script;
    }

}
