<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Postulados;
use App\Hojadevida;
use Session;
use Redirect;
use App\Http\Controllers\Controller;

class PostuladosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $postulados 
            = Postulados::select()
            ->join('hojas_vidas AS h', 'h.id', '=', 'postulados.id_hoja_vida')
            ->join('vacantes AS v', 'v.id', '=', 'postulados.id_vacante')
            ->get()
            ;
        return view('postulados.administrar', compact('postulados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
    }

    public function postular($id_user, $id_vacante) {
        if ($id_user == 0) {
            Session::flash('message-error', 'No se puede postular sin Ingresar al sistema. Para Ingresar al sistema registra tu Hoja de Vida');
            return Redirect::to('/ver_vacantes');
        }

        $id_hojadevida = Hojadevida::select('id')->where('id_user', $id_user)->get();
        
        $postulados = Postulados::create([
                    'id_hoja_vida' => $id_hojadevida[0]->id,
                    'id_vacante' => $id_vacante
        ]);
        
        return Redirect::to('/ver_vacantes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
