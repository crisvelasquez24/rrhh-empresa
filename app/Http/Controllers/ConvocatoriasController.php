<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Convocatorias;
use App\Http\Controllers\Controller;
use App\Vacantes;
use App\Hojadevida;
use App\Postulados;
use App\Hojadevida_conv;
use App\Area_interes;
use Session;
use Mail;

class ConvocatoriasController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $convocatorias = Convocatorias::all();
        return view('convocatorias.administrar', compact('convocatorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $vacantes = Vacantes::lists('nombre', 'id')->toArray();
        $area_interes = Area_interes::lists('nombre', 'id')->toArray();
        return view('convocatorias.crear', compact('vacantes', 'area_interes'));
    }

    public function getPostulados(Request $request, $id) {
        if ($request->ajax()) {
            $postulados = Hojadevida::select('hojadevidas.id as id_hdv', 'p_nombre', 'p_apellido', 's_apellido', 'tipo_doc', 'doc')
                    ->join('postulados', 'hojadevidas.id', '=', 'postulados.id_hojadevidas')
                    ->where('id_vacantes', $id)
                    ->get();
            return response()->json($postulados);
        }
    }
    
    public function getPostulados_area_int(Request $request, $id) {
        if ($request->ajax()) {
            $postulados = Hojadevida::select('hojadevidas.id as id_hdv', 'p_nombre', 'p_apellido', 's_apellido', 'tipo_doc', 'doc')
                    ->where('id_area_interes', $id)
                    ->get();
            return response()->json($postulados);
        }
    }
    
    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/convocatorias'; \n";
        $script.= "</script>\n";
        echo $script;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $dato_vacante = Vacantes::select('nombre')->where('id', $request['vacante'])->get();
        $dato_area_interes = Area_interes::select('nombre')->where('id', $request['a_interes'])->get();
        $nombre_vacante='';
        $nombre_area_interes='';
        foreach ($dato_vacante as $data){$nombre_vacante=$data->nombre;}
        foreach ($dato_area_interes as $data){$nombre_area_interes=$data->nombre;}
        $convocatorias = Convocatorias::create([
                    'vacante' => $nombre_vacante,
                    'a_interes' => $nombre_area_interes,
                    'fecha' => $request['fecha'],
                    'hora' => $request['hora'],
                    'lugar' => $request['lugar'],
                    'citacion' => $request['citacion'],
        ]);

        $id_convocatorias = Convocatorias::max('id');
        $postulados = $request['postulados'];
        for ($i = 0; sizeof($postulados) > $i; $i++) {
            Hojadevida_conv::create([
                'id_hojadevidas' => $postulados[$i],
                'id_convocatorias' => $id_convocatorias
            ]);
            $datos_hdv = Hojadevida::select()->where('id', $postulados[$i])->get();
            $email = $datos_hdv[0]->email;
            Mail::send('emails.convocatoria',$request->all(), function($msj) use ($email) {
                $msj->subject('Convocatoria');
                $msj->to($email);
            });
        }

        if ($convocatorias) {
            Session::flash('message-success', 'La convocatoria fue creada correctamente');
        } else {
            Session::flash('message-error', 'La convocatoria no pudo ser creada');
        }
        
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
