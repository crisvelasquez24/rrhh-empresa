<?php

namespace App\Http\Controllers;
use App\TiposContratos;
use Session;

use Illuminate\Http\Request;

class TiposContratosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiposContratos = TiposContratos::all();
        return view('tipos_contratos.administrar', compact('tiposContratos'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipos_contratos.crear');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tiposContratos = TiposContratos::create($request->all());
        if($tiposContratos){
            Session::flash('message-success',$request['descripcion'].' fue creado correctamente');
        }else{
            Session::flash('message-error','No pudo ser creado');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tiposContratos = TiposContratos::find($id);
        return view('Tipos_contratos.actualizar', compact('tiposContratos'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tiposContratos = TiposContratos::find($id);
        $tiposContratos->fill($request->all());
        if ($tiposContratos->save()) {
            Session::flash('message-success', 'El Tipo Contrato '.$request['descripcion'].' fue actualizado correctamente');
        } else {
            Session::flash('message-error', 'Error al editar el tipo Contrato '.$request['descripcion']);
        }
        return $this->retorno();        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre,$id)
    {
        if (TiposContratos::destroy($id)) {
            Session::flash('message-success', 'tipo Contrato '.$nombre.' fue eliminadada correctamente');
        } else {
            Session::flash('message-error', 'Error al eliminar el tipo Contrato '.$nombre);
        }
        return redirect('/tipoContrato');
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/tipoContrato'; \n";
        $script.= "</script>\n";
        echo $script;
    }
}
