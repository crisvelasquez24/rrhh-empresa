<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Paises;
use App\Departamentos;
use App\Ciudades;
use Mail;
use Session;
use Redirect;
use App\User;
use App\Hojadevida;
use App\Area_interes;
use App\Postulados;
use App\experiencias_laborales;
use App\formaciones;
use App\Archivos;
use App\Hojadevida_conv;
use Auth;
use DB;
use Carbon\Carbon;

class HojadeVidaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleado =
            DB::table('hojas_vidas')
                ->select(DB::raw('COUNT(*) AS total'))
                ->where('id_user', '=', auth()->user()->id)->get();
        $id = Hojadevida::where('id_user', '=', auth()->user()->id)->get();
        if ($empleado[0]->total > 0) {
            $nombreArchivo = "hojas_vidas/" . $this->download($id[0]->foto . "," . $id[0]->p_nombre);

            if ("hojas_vidas/" == $nombreArchivo) {
                $nombreArchivo = "user.png";
            }
            $modificando = 1;
            $paises = Paises::pluck('nombre', 'id')->all();
            $departamentos = Departamentos::pluck('nombre', 'id')->all();
            $ciudades = Ciudades::pluck('nombre', 'id')->all();
            $area_interes = Area_interes::pluck('nombre', 'id')->all();
            $hojaVida = Hojadevida::find($id[0]->id);

            $experienciasLaborales = experiencias_laborales::where('id_hoja', $id[0]->id)->get();

            $formaciones = formaciones::where('id_hoja', $id[0]->id)->get();

            return view(
                'hojadevida.actualizar',
                compact(
                    'hojaVida',
                    'paises',
                    'departamentos',
                    'ciudades',
                    'area_interes',
                    'modificando',
                    'experienciasLaborales',
                    'nombreArchivo',
                    'formaciones'
                )
            );
        } else {
            $modificando = 0;
            $paises = Paises::pluck('nombre', 'id')->all();
            $departamentos = Departamentos::pluck('nombre', 'id')->all();
            $ciudades = Ciudades::pluck('nombre', 'id')->all();
            $area_interes = Area_interes::pluck('nombre', 'id')->all();
            return view(
                'hojadevida.crear',
                compact('paises', 'departamentos', 'ciudades', 'area_interes', 'modificando')
            );
        }
    }

public function listarHojas()
{
    $empleado = Hojadevida::select(
            'hojas_vidas.*',
            'ciudades.nombre as nom_ciudad',
            'area_interes.nombre as nom_area_interes'
        )
        ->join('ciudades', 'ciudades.id', '=', 'hojas_vidas.id_ciudad_actual')
        ->join('area_interes', 'area_interes.id', '=', 'hojas_vidas.id_area_interes')
        ->join('users AS u', 'u.id', '=', 'hojas_vidas.id_user')
        ->join('contratos AS c', 'c.id_hojavida', '=', 'hojas_vidas.id')
        ->where('c.estado', 1)
        ->get();

    $aspirantes = User::select(
            'users.*',
            'ciudades.nombre as nom_ciudad',
            'area_interes.nombre as nom_area_interes'
        )
        ->leftjoin('hojas_vidas AS h', 'users.id', '=', 'h.id_user')
        ->leftjoin('ciudades', 'ciudades.id', '=', 'h.id_ciudad_actual')
        ->leftjoin('area_interes', 'area_interes.id', '=', 'h.id_area_interes')
        ->where('id_perfil', 1)
        ->where('users.estado', 1)
        ->get();

    $inactivos = Hojadevida::select(
            'hojas_vidas.*',
            'ciudades.nombre as nom_ciudad',
            'area_interes.nombre as nom_area_interes'
        )
        ->join('ciudades', 'ciudades.id', '=', 'hojas_vidas.id_ciudad_actual')
        ->join('area_interes', 'area_interes.id', '=', 'hojas_vidas.id_area_interes')
        ->join('users AS u', 'u.id', '=', 'hojas_vidas.id_user')
        ->where('u.estado', 0)
        ->get();
    return view('hojadevida.listarHojasVidas', compact('empleado', 'aspirantes', 'inactivos'));
}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paises = Paises::pluck('nombre', 'id')->all();
        $departamentos = Departamentos::pluck('nombre', 'id')->all();
        $ciudades = Ciudades::pluck('nombre', 'id')->all();
        $area_interes = Area_interes::pluck('nombre', 'id')->all();
        return view(
            'hojadevida.crear',
            compact('paises', 'departamentos', 'ciudades', 'area_interes')
        );
    }

    public function getDepartamentos(Request $request, $id)
    {

        if ($request->ajax()) {
            $departamentos = Departamentos::where('id_pais', $id)->get();
            return response()->json($departamentos);
        }

    }

    public function getCiudades(Request $request, $id)
    {

        if ($request->ajax()) {
            $ciudades = Ciudades::where('id_departamento', $id)->get();
            return response()->json($ciudades);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $archivos = "";
        if ($request['foto_old']) {
            $archivos = Archivos::create([
                'contenido' => \File::get($request['foto_old']),
                'extension' => '',
                'nombre_archivo' => $request['foto_old']->getClientOriginalName(),
            ])->id;
        }
        $hojadevida
        = Hojadevida::create([
            'foto'                       => $archivos,
            'p_nombre'                   => $request['p_nombre'],
            's_nombre'                   => $request['s_nombre'],
            'p_apellido'                 => $request['p_apellido'],
            's_apellido'                 => $request['s_apellido'],
            'tipo_doc'                   => $request['tipo_doc'],
            'doc'                        => $request['doc'],
            'estado_civil'               => $request['estado_civil'],
            'genero'                     => $request['genero'],
            'email'                      => $request['email'],
            'fecha_nac'                  => $request['fecha_nac'],
            'dir'                        => $request['dir'],
            'tel'                        => $request['tel'],
            'movil'                      => $request['movil'],
            'id_pais_nacimiento'         => $request['id_pais_nacimiento'],
            'id_departamento_nacimiento' => $request['id_departamento_nacimiento'],
            'id_ciudad_nacimiento'       => $request['id_ciudad_nacimiento'],
            'id_pais_actual'             => $request['id_pais_actual'],
            'id_departamento_actual'     => $request['id_departamento_actual'],
            'id_ciudad_actual'           => $request['id_ciudad_actual'],
            'id_user'                    => Auth::user()->id,
            'id_area_interes'            => $request['id_area_interes'],
            'profesion'                  => $request['profesion'],
            'perfil_laboral'             => $request['perfil_laboral'],
            'anos_experiencia'           => $request['anos_experiencia'],
            'aspiracion_salarial'        => $request['aspiracion_salarial'],
            'trabaja_actualmente'        => $request['trabaja_actualmente'],
            'posibilidad_translado'      => $request['posibilidad_translado'],
            'posibilidad_viajar'         => $request['posibilidad_viajar'],

        ]);

        $id_hoja = Hojadevida::max('id');

        for ($i = 0; $i < count($request['nivel_estudios']); $i++) {
            $expeLaborales = formaciones::create([
                'id_hoja'               => $id_hoja,
                'nivel_estudios'        => $request['nivel_estudios'][$i],
                'area_estudios'         => $request['area_estudios'][$i],
                'titulo_estudios'       => $request['titulo_estudios'][$i],
                'institucion_estudios'  => $request['institucion_estudios'][$i],
                'pais_estudios'         => $request['pais_estudios'][$i],
                'departamento_estudios' => $request['departamento_estudios'][$i],
                'ciudad_estudios'       => $request['ciudad_estudios'][$i],
                'funciones_estudios'    => $request['funciones_estudios'][$i],

            ]
            );

        }

        for ($i = 0; $i < count($request['empresa']); $i++) {
            $formaciones =
            experiencias_laborales::create(
                [
                    'id_hoja'                    => $id_hoja,
                    'empresa'                    => $request['empresa'][$i],
                    'telefono_empresa'           => $request['telefono_empresa'][$i],
                    'sector_empresa'             => $request['sector_empresa'][$i],
                    'cargo_empresa'              => $request['cargo_empresa'][$i],
                    'jefe_empresa'               => $request['jefe_empresa'][$i],
                    'fecha_ingreso_empresa'      => $request['fecha_ingreso_empresa'][$i],
                    'fecha_finalizacion_empresa' => $request['fecha_finalizacion_empresa'][$i],
                    'tiempo_experiencia'         => '1000',
                    'pais_empresa'               => $request['pais_empresa'][$i],
                    'departamento_empresa'       => $request['departamento_empresa'][$i],
                    'ciudad_empresa'             => $request['ciudad_empresa'][$i],
                    'funciones_empresa'          => $request['funciones_empresa'][$i],

                ]
            );

        }

        if ($hojadevida) {
            Session::flash('message-success', 'Registraste la hoja de vida satisfactoriamente. Revisa tu Correo Electronico.');
            $email = $request['email'];
            // Mail::to($request['email'])->send(new \App\Mail\MailHojasVidas($request->all()));

            return Redirect::to('/dashboard');
        } else {
            Session::flash('message-error', 'Algo salio mal');
            return $this->create();
        }

    }

    public function depar($id)
    {
        $departamentos = Departamentos::where('id', $id)->get();
        return $departamentos[0]->nombre;

    }

    public function ciuda($id)
    {
        $departamentos = Ciudades::where('id', $id)->get();
        return $departamentos[0]->nombre;

    }

    /**
     * Display the specified resource.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datos_hojadevida
            = Hojadevida::select(
                'hojas_vidas.*',
                'pn.nombre as pais_naci',
                'dn.nombre as departamento_naci',
                'cn.nombre as ciudad_naci',
                'pa.nombre as pais_actual',
                'da.nombre as departamento_actual',
                'ca.nombre as ciudad_actual',
                'area_interes.nombre as nom_area_interes'
            )
            ->join('paises AS pn', 'pn.id', '=', 'hojas_vidas.id_pais_nacimiento')
            ->join('departamentos AS dn', 'dn.id', '=', 'hojas_vidas.id_departamento_nacimiento')
            ->join('ciudades AS cn', 'cn.id', '=', 'hojas_vidas.id_ciudad_nacimiento')
            ->join('paises AS pa', 'pa.id', '=', 'hojas_vidas.id_pais_actual')
            ->join('departamentos AS da', 'da.id', '=', 'hojas_vidas.id_departamento_actual')
            ->join('ciudades AS ca', 'ca.id', '=', 'hojas_vidas.id_ciudad_actual')
            ->join('area_interes', 'area_interes.id', '=', 'hojas_vidas.id_area_interes')
            ->where('hojas_vidas.id', $id)
            ->get();
        $hojadevida = $datos_hojadevida[0];
        // $nombreArchivo = $this->download($hojadevida->foto . "," . $hojadevida->p_nombre);
        $nombreArchivo = "hojas_vidas/" . $this->download($hojadevida->foto . "," . $hojadevida->p_nombre);

        if ("hojas_vidas/" == $nombreArchivo) {
            $nombreArchivo = "user.png";
        }
        $formaciones =
            formaciones::select(
                "*",
                "p.nombre AS pais",
                "d.nombre AS depar",
                "c.nombre AS ciudad"
            )
                ->join('paises AS p', 'p.id', '=', 'formaciones.pais_estudios')
                ->join('departamentos AS d', 'd.id', '=', 'formaciones.departamento_estudios')
                ->join('ciudades AS c', 'c.id', '=', 'formaciones.ciudad_estudios')
                ->where('id_hoja', $id)
                ->get();


        $experiencias_laborales =
            experiencias_laborales::select(
                "*",
                "p.nombre AS pais",
                "d.nombre AS depar",
                "c.nombre AS ciudad"
            )
                ->join('paises AS p', 'p.id', '=', 'experiencias_laborales.pais_empresa')
                ->join('departamentos AS d', 'd.id', '=', 'experiencias_laborales.departamento_empresa')
                ->join('ciudades AS c', 'c.id', '=', 'experiencias_laborales.ciudad_empresa')
                ->where('id_hoja', $id)
                ->get();
        return view(
            'suscritos.ver_hojadevida',
            compact(
                'hojadevida',
                'formaciones',
                'nombreArchivo',
                'experiencias_laborales'
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empleado =
            DB::table('hojas_vidas')
                ->select(DB::raw('COUNT(*) AS total'))
                ->where('id_user', '=', auth()->user()->id)->get();
        $id = Hojadevida::where('id_user', auth()->user()->id)->get()[0]->id;

        if ($empleado[0]->total > 0) {
            $paises = Paises::pluck('nombre', 'id')->all();
            $departamentos = Departamentos::pluck('nombre', 'id')->all();
            $ciudades = Ciudades::pluck('nombre', 'id')->all();
            $area_interes = Area_interes::pluck('nombre', 'id')->all();
            $hojaVida = Hojadevida::find($id);
            return view(
                'hojadevida.actualizar',
                compact('hojaVida', 'paises', 'departamentos', 'ciudades', 'area_interes')
            );
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $foto = '';
        if ($request['foto_old']) {
            if ($request['foto']) {
                $foto = Archivos::where('id', $request['foto'])
                ->update(
                    [
                        'contenido' => \File::get($request['foto_old']),
                        'extension' => '',
                        'nombre_archivo' => $request['foto_old']->getClientOriginalName(),
                    ]
                );
            } else {
                $foto = Archivos::create(
                    [
                        'contenido' => \File::get($request['foto_old']),
                        'extension' => '',
                        'nombre_archivo' => $request['foto_old']->getClientOriginalName(),
                    ]
                )->id;
            }
        }
        $request['foto'] = $foto;
        $hojadevida = Hojadevida::find($id);
        $hojadevida->fill($request->all());
        if ($hojadevida->save()) {

        $id_hoja = Hojadevida::max('id');
        for ($i = 0; $i < count($request['nivel_estudios']); $i++) {
            $expeLaborales = formaciones::where('id', $request['id_formaciones'][$i])
            ->update(
                [
                    'nivel_estudios'        => $request['nivel_estudios'][$i],
                    'area_estudios'         => $request['area_estudios'][$i],
                    'titulo_estudios'       => $request['titulo_estudios'][$i],
                    'institucion_estudios'  => $request['institucion_estudios'][$i],
                    'pais_estudios'         => $request['pais_estudios'][$i],
                    'departamento_estudios' => $request['departamento_estudios'][$i],
                    'ciudad_estudios'       => $request['ciudad_estudios'][$i],
                    'funciones_estudios'    => $request['funciones_estudios'][$i],
                ]
            );
        }

        for ($i = 0; $i < count($request['empresa']); $i++) {
            $fecha_ingreso_empresa =
                Carbon::createFromFormat(
                    'd-m-Y',
                    $request['fecha_ingreso_empresa'][$i])->format('Y-m-d'
                );
            $fecha_ingreso_empresa =
                Carbon::createFromFormat(
                    'd-m-Y',
                    $request['fecha_ingreso_empresa'][$i])->format('Y-m-d'
                );

            $formaciones = experiencias_laborales::where('id', $request['id_experiencias'][$i])
            ->update(
                [
                    'empresa'                    => $request['empresa'][$i],
                    'telefono_empresa'           => $request['telefono_empresa'][$i],
                    'sector_empresa'             => $request['sector_empresa'][$i],
                    'cargo_empresa'              => $request['cargo_empresa'][$i],
                    'jefe_empresa'               => $request['jefe_empresa'][$i],
                    'fecha_ingreso_empresa'      => $fecha_ingreso_empresa,
                    'fecha_finalizacion_empresa' => $fecha_ingreso_empresa,
                    'tiempo_experiencia'         => '1000',
                    'pais_empresa'               => $request['pais_empresa'][$i],
                    'departamento_empresa'       => $request['departamento_empresa'][$i],
                    'ciudad_empresa'             => $request['ciudad_empresa'][$i],
                    'funciones_empresa'          => $request['funciones_empresa'][$i],
                ]
            );
        }
            Session::flash('message-success', 'Hoja de vida Actualizada correctamente.');
        } else {
            Session::flash('message-error', 'No se pudo Actualizar la Hoja de vida.');
        }

        return Redirect::to('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function confirmar(Request $request, $email)
    {
        $valid = [];

        if ($request->ajax()) {
            $correo = Hojadevida::where('email', $request['email'])->get();

            if ($correo) {
                $valid = ["valid", "true"];
                echo 'existe';
            } else {
                $valid = ["valid", "false"];
                echo 'no existe';
            }

            return response()->json($valid);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function susPostulaciones($id)
    {
        $postulados = Postulados::select('nombre', 'postulados.created_at as fecha_postulacion')
            ->join('vacantes', 'vacantes.id', '=', 'postulados.id_vacantes')
            ->where('postulados.id_hojas_vidas', $id)
            ->get();
        return view('suscritos.sus_postulados', compact('postulados'));
    }

    public function susConvocatorias($id)
    {
        $convocatorias = Hojadevida_conv::select('convocatorias.*')
            ->join('convocatorias', 'convocatorias.id', '=', 'hojadevida_convs.id_convocatorias')
            ->where('hojadevida_convs.id_hojas_vidas', $id)
            ->get();
        return view('suscritos.sus_convocatorias', compact('convocatorias'));
    }

    public function download($file) {
        // \Storage::disk('imgContrato')->delete('*');
        $datos = explode(',', $file);
        $contenido
            = DB::table('archivos')
                ->select('contenido','nombre_archivo')
                ->where('id', '=', $datos[0])->get()->toArray()
            ;

        if (!$contenido) {
            return false;
        }
        $nombreArchivo = $datos[1] . $contenido[0]->nombre_archivo;
        \Storage::disk('imgHojas')->put($nombreArchivo, $contenido[0]->contenido);
        $file_path = public_path('fotos/hojas_vidas/'.$datos[1].'-'.$contenido[0]->nombre_archivo);
        if (!$file_path) {
            return false;
        }
        return $nombreArchivo;
    }

}
