<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Respuestas;
use Session;
use App\Asignar_evaluacion;
use App\Evaluaciones;
use App\User;
use App\Http\Requests;
use Auth;
use App\Http\Controllers\Controller;
use DB;


class RespuestasController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {        
        return view('respuestas.evaluaciones_asignadas', compact('evaluaciones_asignadas'));        
    }

    public function retorno() {
        $script = "<script>\n";
        $script .= "window.parent.location.href = '/evaluacion'; \n";
        $script .= "</script>\n";
        echo $script;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }
    
    public function presentar_evaluacion(Request $request) {
        $evaluacion_asig = Evaluaciones::select()
                ->where('id', $request['id_evaluacion'])
                ->get();
        $evaluacion_asignada = $evaluacion_asig[0];
        return view('respuestas.responder_preguntas', compact('evaluacion_asignada'));
    }
    
    public function cargar_evaluaciones() {
        $evaluaciones_asignadas = Asignar_evaluacion::select('evaluaciones.nombre', 'evaluaciones.id')
                ->join('evaluaciones','evaluaciones.id','=','asignar_evaluacions.id_evaluaciones')
                ->where('id_users', Auth::user()->id)
                ->get();
        return response()->json($evaluaciones_asignadas->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $respu = Respuestas::create($request->all());
        $ultRespuesta = Respuestas::max('id');
        $data = Respuestas::find($ultRespuesta);
        $data2 = Evaluaciones::find($data->id_evaluaciones);
        $respu = $data->toArray();
        $eval = $data2->toArray();
        $calificacion = 0;
        $aciertos = 0;
        for ($i = 1; $i < 11; $i++) {
            if ($respu['rp' . $i] == $eval['rp' . $i]) {
                $aciertos++;
                $calificacion +=10;
            }
        }
        $tiempo = $request->get('tiempo');
        $data->fill([
            'calificacion' => $calificacion,
            'tiempo' => $request->get('tiempo'),
        ]);
        $data->save();
        
        Asignar_evaluacion::where('id_users',$data->id_users)
                ->where('id_evaluaciones',$data->id_evaluaciones)
                ->delete();
        return view('respuestas.resultados', compact('calificacion', 'aciertos', 'tiempo'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return view('respuestas.administrar_resultados');
    }   


public function usuarios($id){
    $users = User::select()->where('id',$id)->get();
    return $users;
}

    public function listing($id) {       
        $genres = Evaluaciones::select()->where('id',$id)->get();    
        return response()->json(
        $genres->toArray()
        );
    }
     public function administrar_respuestas() {   
        if(Auth::user()->id_perfiles == 3){
             $resultados = Respuestas::select(
                       'respuestas.rp1','respuestas.rp2','respuestas.rp3','respuestas.rp4','respuestas.rp5','respuestas.rp6','respuestas.rp7','respuestas.rp8','respuestas.rp9','respuestas.rp10','respuestas.id_evaluaciones','tiempo','calificacion','evaluaciones.p1','evaluaciones.op1_a','evaluaciones.op1_b','evaluaciones.op1_c','evaluaciones.p2','evaluaciones.op2_a','evaluaciones.op2_b','evaluaciones.op2_c'
                                      ,'evaluaciones.p3','evaluaciones.op3_a','evaluaciones.op3_b','evaluaciones.op3_c','evaluaciones.p4','evaluaciones.op4_a','evaluaciones.op4_b','evaluaciones.op4_c','evaluaciones.p5','evaluaciones.op5_a','evaluaciones.op5_b','evaluaciones.op5_c','evaluaciones.p6','evaluaciones.op6_a','evaluaciones.op6_b','evaluaciones.op6_c','evaluaciones.p7','evaluaciones.op7_a','evaluaciones.op7_b','evaluaciones.op7_c','evaluaciones.p8','evaluaciones.op8_a','evaluaciones.op8_b','evaluaciones.op8_c','evaluaciones.p9','evaluaciones.op9_a','evaluaciones.op9_b','evaluaciones.op9_c','evaluaciones.p10','evaluaciones.op10_a','evaluaciones.op10_b','evaluaciones.op10_c','evaluaciones.nombre','name')                                   
                                    ->join('evaluaciones','evaluaciones.id','=','respuestas.id_evaluaciones')
                                    ->join('users','users.id','=','respuestas.id_users')->get();
                                             $evalua = array();
                                     $array_evalu = array();
                                     foreach ($resultados as $key => $value) {                                    
                                           $evalua[] = Evaluaciones::select('rp1','rp2','rp3','rp4','rp5','rp6','rp7','rp8','rp9','rp10')
                                                     ->where('id',$value->id_evaluaciones)->get();
                                                      $array_evalu[] =$evalua[$key]->toArray();
                                     }    
               return view('respuestas.administrar_resultados',compact('resultados','array_evalu'));
        }
        if(Auth::user()->id_perfiles == 5){
             $resultados = Respuestas::select(
                            'respuestas.rp1','respuestas.rp2','respuestas.rp3','respuestas.rp4','respuestas.rp5','respuestas.rp6','respuestas.rp7','respuestas.rp8','respuestas.rp9','respuestas.rp10','respuestas.id_evaluaciones','tiempo','calificacion','evaluaciones.p1','evaluaciones.op1_a','evaluaciones.op1_b','evaluaciones.op1_c','evaluaciones.p2','evaluaciones.op2_a','evaluaciones.op2_b','evaluaciones.op2_c'
                                      ,'evaluaciones.p3','evaluaciones.op3_a','evaluaciones.op3_b','evaluaciones.op3_c','evaluaciones.p4','evaluaciones.op4_a','evaluaciones.op4_b','evaluaciones.op4_c','evaluaciones.p5','evaluaciones.op5_a','evaluaciones.op5_b','evaluaciones.op5_c','evaluaciones.p6','evaluaciones.op6_a','evaluaciones.op6_b','evaluaciones.op6_c','evaluaciones.p7','evaluaciones.op7_a','evaluaciones.op7_b','evaluaciones.op7_c','evaluaciones.p8','evaluaciones.op8_a','evaluaciones.op8_b','evaluaciones.op8_c','evaluaciones.p9','evaluaciones.op9_a','evaluaciones.op9_b','evaluaciones.op9_c','evaluaciones.p10','evaluaciones.op10_a','evaluaciones.op10_b','evaluaciones.op10_c','evaluaciones.nombre','name')                                   
                                    ->join('evaluaciones','evaluaciones.id','=','respuestas.id_evaluaciones')
                                    ->join('users','users.id','=','respuestas.id_users')
                                     ->where('respuestas.id_users',Auth::user()->id)->get();
                                     $evalua = array();
                                     $array_evalu = array();
                                     foreach ($resultados as $key => $value) {                                    
                                           $evalua[] = Evaluaciones::select('rp1','rp2','rp3','rp4','rp5','rp6','rp7','rp8','rp9','rp10')
                                                     ->where('id',$value->id_evaluaciones)->get();
                                                      $array_evalu[] =$evalua[$key]->toArray();
                                     }                         
                           
              return view('respuestas.administrar_resultados',compact('resultados','array_evalu'));

        }
      
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
    
    function conversorSegundosHoras($tiempo_en_segundos) {
	$horas = floor($tiempo_en_segundos / 3600);
	$minutos = floor(($tiempo_en_segundos - ($horas * 3600)) / 60);
	$segundos = $tiempo_en_segundos - ($horas * 3600) - ($minutos * 60);
 
	return $horas . ':' . $minutos . ":" . $segundos;
}

}
