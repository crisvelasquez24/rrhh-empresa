<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hojadevida;
use App\Contratos;
use App\Configuraciones;
use App\Liquidaciones;
use Carbon\Carbon;
use DB;

class LiquidacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = auth()->user()->id_perfil == 4  ? auth()->user()->id : false;
        $empleados
            = DB::table('hojas_vidas')
            ->select(
                'p_nombre',
                's_nombre',
                'hojas_vidas.id AS id_hoja',
                's_apellido',
                'p_apellido'
            )
            ->join('contratos', 'hojas_vidas.id', '=', 'contratos.id_hojavida')
            ->where('contratos.estado', 1)
            ->when($id, function ($query, $id) {
                return $query->where('id_user', $id);
            })
            ->get();
        // $empleados = Hojadevida::select('p_nombre','p_apellido','hojas_vidas.id AS id_hoja')
        // ->join('contratos AS c', 'c.id_hojavida', '=', 'hojas_vidas.id')
        // ->get();
        return view('liquidaciones.administrar', compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $salud
            = Configuraciones::select('valores')
                ->where('siglas', 'SAL')
                ->first()->valores;

        $pension
            = Configuraciones::select('valores')
                ->where('siglas', 'PEN')
                ->first()->valores;

        $salario = Configuraciones::select('valores')
            ->where('siglas', 'SALMIN')
            ->first()->valores;

        $transporte = Configuraciones::select('valores')
            ->where('siglas', 'AUX')
            ->first()->valores;
        for ($i = 0; $i < count($request->get('empleado')); $i++) {
            $contratos
                = Contratos::where(
                    'id_hojavida',
                    $request->get('empleado')[$i])->first();

            $salarioBasico = $contratos->salario_basico;
            $fechaInicio = $contratos->fecha_inicio;
            $fechaFin = $contratos->fecha_fin;
            $dateSystem = date('Y-m-d');
            if ($dateSystem < $fechaFin) {
                $fechaFin = $dateSystem;
            }
            //convertimos la fecha 1 a objeto Carbon
            $carbon1 = new \Carbon\Carbon("$fechaInicio 00:00:00");
            //convertimos la fecha 2 a objeto Carbon
            $carbon2 = new \Carbon\Carbon("$fechaFin 00:00:00");
            //de esta manera sacamos la diferencia en minutos
            $daysDiff = $carbon1->diffInDays($carbon2);
            // return response()->json($daysDiff);
            if ($salarioBasico <= (2 * $salario)) {
                $transporte = $transporte;
            } else {
                $transporte = 0;
            }

            $cesantias = ($salarioBasico + $transporte) * $daysDiff/360;
            $prima = $cesantias;
            $intereces = ($cesantias * $daysDiff * 0.12)/360;

            $vacaciones = ($salarioBasico * $daysDiff)/360;

            $totalLiquidacion = $cesantias + $prima + $intereces + $vacaciones;
            $liquidaciones
                = Liquidaciones::create(
                    [
                        'cesantias'         => $cesantias,
                        'prima'             => $prima,
                        'intereses'         => $intereces,
                        'vacaciones'        => $vacaciones,
                        'valor_liquidacion' => $totalLiquidacion,
                        'id_empleado'       => $request->get('empleado')[$i],
                    ]
                );
            if ($liquidaciones) {
                $estado
                    = Contratos::where('id', $contratos->id)
                    ->update(
                        [
                            'estado' => 0,
                        ]
                    );
            }
        }
        return response()->json($liquidaciones);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $LiquidacionesGeneradas
            = Liquidaciones::select(
                'liquidaciones.*',
                'h.p_nombre',
                'h.s_nombre',
                'h.s_apellido',
                'h.tipo_doc',
                'h.doc',
                'h.p_apellido'
            )
            ->join('hojas_vidas AS h', 'h.id', '=', 'liquidaciones.id_empleado')
            ->where('id_empleado', $id)
            ->get();

            return view('liquidaciones.imprimir_liquidacion', compact('LiquidacionesGeneradas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
