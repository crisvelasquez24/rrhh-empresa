<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\controlEmpleados;
use App\User;
use App\Hojadevida;
use DB;
use Carbon\Carbon;

class ControlEmpleadosContorller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('controlempleado.control');
    }


    public function controlListar()
    {
        $zona =date_default_timezone_get();
         $controlesEmpleados
            = DB::table('control_empleados')
            ->select(DB::raw('*'))
            ->join('users AS u', 'u.id', '=', 'control_empleados.id_user')
            ->join('hojas_vidas AS h', 'u.id', '=', 'h.id_user')
            // ->whereRaw('Date(control_empleados.created_at) = CURDATE()')
            ->get();
            // dd($controlesEmpleados);
        return view('controlempleado.administrar', compact('controlesEmpleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $data = explode("0000000000", $request->get("id_user"));
        // $id = $data[0];
        $users = Hojadevida::where('doc', '=', $request->get("id_user"))->first();
        if (!$users) {
            return response()->json("Empleado No existe");
        }
        // $control = controlEmpleados::where('id_user', '=', $users->id_user)

        // ->first();
        // return response()->json(substr($control->fecha_ingreso, 0, 10));
        $request["id_user"] = $users->id_user;
        $control = controlEmpleados::create($request->all());
        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
