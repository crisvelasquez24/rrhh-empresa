<?php

namespace App\Http\Controllers;

use App\Departamentos_rh;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\cargos;

class DepartamentosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $depar = Departamentos_rh::all();
        return view('departamentos.administrar', compact('depar'));
    }

    public function retorno() {
        $script = "<script>\n";
        $script .= "window.parent.location.href = '/depar'; \n";
        $script .= "</script>\n";
        echo $script;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('departamentos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $depar = Departamentos_rh::create($request->all());
        if ($depar) {
            Session::flash('message-success', 'El Departamento ' . $request['nombre'] . ' fue creado correctamente');
        } else {
            Session::flash('message-error', 'El Departamento ' . $request['nombre'] . ' no fue creado');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $depar = Departamentos_rh::find($id);
        return view('departamentos.actualizar', compact('depar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $depar = Departamentos_rh::find($id);
        $depar->fill($request->all());
        if ($depar->save()) {
            Session::flash('message-success', 'El Departamento ' . $request['nombre'] . ' fue actualizado correctamente');
        } else {
            Session::flash('message-error', 'El Departamento ' . $request['nombre'] . ' no fue actualizado');
        }
        return $this->retorno();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre, $id) {
        if (Departamentos_rh::destroy($id)) {
            Session::flash('message-success', 'El Departamento ' . $nombre . ' fue eliminado correctamente');
        } else {
            Session::flash('message-error', 'El Departamento ' . $nombre . ' no fue eliminado');
        }
        return redirect('/depar');
    }

    public function gethijodepar(Request $request, $id) {
        if ($request->ajax()) {
            $users = cargos::where('id_departamento_rhs', $id)->get();
            return response()->json($users);
        }
    }

}
