<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cargos;
use App\Evaluaciones;
use App\Respuestas;
use App\User;
use Session;
use App\Http\Controllers\Controller;

class EvaluacionesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $evaluaciones = Evaluaciones::select('evaluaciones.*', 'cargos.nombre as nom_cargo')
                ->join('cargos', 'cargos.id', '=', 'evaluaciones.id_cargo')
                ->get();
        return view('evaluaciones.administrar', compact('evaluaciones'));
    }

    public function retorno() {
        $script = "<script>\n";
        $script .= "window.parent.location.href = '/evaluacion'; \n";
        $script .= "</script>\n";
        echo $script;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $cargos = Cargos::pluck('nombre','id')->all();
        $respues = " ";
        $parame = '0';
        return view('evaluaciones.crear', compact('cargos', 'respues', 'parame'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $eval = Evaluaciones::create($request->all());
        if ($eval) {
            Session::flash('message-success', 'Evaluacion ' . $request['nombre'] . ' fue creada correctamente');
        } else {
            Session::flash('message-error', 'Evaluacion ' . $request['nombre'] . ' no fue creada');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $parame = "1";
        $cargos = Cargos::pluck('nombre','id')->all();
        $evaluaciones = Evaluaciones::find($id);
        return view('evaluaciones.actualizar', compact('evaluaciones', 'cargos', 'parame'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $parame = "0";
        $cargos = Cargos::pluck('nombre','id')->all();
        $evaluaciones = Evaluaciones::find($id);
        return view('evaluaciones.actualizar', compact('evaluaciones', 'cargos', 'parame'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $eval = Evaluaciones::find($id);
        $eval->fill($request->all());
        if ($eval->save()) {
            Session::flash('message-success', 'Evaluacion ' . $request['nombre'] . ' fue actualizada correctamente');
        } else {
            Session::flash('message-error', 'Evaluacion ' . $request['nombre'] . ' no fue actualizado');
        }
        return $this->retorno();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

    }

    public function asignarEval($id) {
        Session::put('id_evaluacion', $id);
        return view('evaluaciones.asignar');
    }

    public function listing() {
        $id_evaluacion = Session::get('id_evaluacion');
        $user = User::select('hojas_vidas.p_nombre','hojas_vidas.p_apellido','s_apellido','users.id')
                ->join('hojas_vidas', 'users.id', '=', 'hojas_vidas.id_user')
                ->join('contratos', 'contratos.id_hojavida', '=', 'hojas_vidas.id')
                ->join('cargos', 'cargos.id', '=', 'contratos.id_cargo')
                ->join('evaluaciones', 'evaluaciones.id_cargo', '=', 'cargos.id')
                ->where('evaluaciones.id', $id_evaluacion)
                ->where('contratos.estado', 1)
                ->get();
        return response()->json($user->toArray());
    }

}
