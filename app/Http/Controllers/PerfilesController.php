<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Perfiles;
use Session;

class PerfilesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $perfiles = Perfiles::all();
        return view('perfiles.administrar', compact('perfiles'));
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/perfiles'; \n";
        $script.= "</script>\n";
        echo $script;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('perfiles.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $perfiles = Perfiles::create([
            'nombre' => $request['nombre']
        ]);
        if($perfiles){
            Session::flash('message-success',$request['nombre'].'fue creado correctamente');
        }else{
            Session::flash('message-error','No pudo ser creado');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $perfiles = Perfiles::find($id);
        return view('perfiles.actualizar', compact('perfiles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $perfiles = Perfiles::find($id);
        $perfiles->fill($request->all());
        if ($perfiles->save()) {
            Session::flash('message-success', 'El perfil '.$request['nombre'].' fue actualizado correctamente');
        } else {
            Session::flash('message-error', 'Error al editar el perfil '.$request['nombre']);
        }
        return $this->retorno();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre,$id) {
        if (Perfiles::destroy($id)) {
            Session::flash('message-success', 'Perfil '.$nombre.' fue eliminadado correctamente');
        } else {
            Session::flash('message-error', 'Error al eliminar el perfil '.$nombre);
        }
        return redirect('/perfiles');
    }

}
