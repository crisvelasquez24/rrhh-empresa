<?php

namespace App\Http\Controllers;
use App\TiposSalarios;
use Session;

use Illuminate\Http\Request;

class TiposSalariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiposSalarios = TiposSalarios::all();
        return view('tipos_salarios.administrar', compact('tiposSalarios'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipos_salarios.crear');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tiposSalarios = TiposSalarios::create($request->all());
        if($tiposSalarios){
            Session::flash('message-success',$request['descripcion'].' fue creado correctamente');
        }else{
            Session::flash('message-error','No pudo ser creado');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tiposSalarios = TiposSalarios::find($id);
        return view('tipos_salarios.actualizar', compact('tiposSalarios'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tiposSalarios = TiposSalarios::find($id);
        $tiposSalarios->fill($request->all());
        if ($tiposSalarios->save()) {
            Session::flash('message-success', 'El Tipo Salario '.$request['descripcion'].' fue actualizado correctamente');
        } else {
            Session::flash('message-error', 'Error al editar el tipo Salario '.$request['descripcion']);
        }
        return $this->retorno();        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre,$id)
    {
        if (TiposSalarios::destroy($id)) {
            Session::flash('message-success', 'tipo Salario '.$nombre.' fue eliminadada correctamente');
        } else {
            Session::flash('message-error', 'Error al eliminar el tipo Salario '.$nombre);
        }
        return redirect('/tipoSalario');
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/tipoSalario'; \n";
        $script.= "</script>\n";
        echo $script;
    }
}
