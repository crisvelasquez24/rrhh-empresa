<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TiposAltasBajas;
use Session;

class TiposAltasBajasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos_altas = TiposAltasBajas::where('tipo',1)->get();
        $tipos_bajas = TiposAltasBajas::where('tipo',2)->get();
        return view('tipos_altas_bajas.administrar', compact('tipos_altas','tipos_bajas'));
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/tipos_altas_bajas'; \n";
        $script.= "</script>\n";
        echo $script;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                return view('tipos_altas_bajas.crear');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $altasbajas = TiposAltasBajas::create($request->all());
        if($altasbajas){
            Session::flash('message-success',$request['nombre'].'fue creado correctamente');
        }else{
            Session::flash('message-error','No pudo ser creado');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
    public function edit($id)
    {
         $tipos = TiposAltasBajas::find($id);
        return view('tipos_altas_bajas.actualizar', compact('tipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipos = TiposAltasBajas::find($id);
        $tipos->fill($request->all());
        if ($tipos->save()) {
            Session::flash('message-success', 'Alta o Baja '.$request['nombre'].' fue actualizado correctamente');
        } else {
            Session::flash('message-error', 'Error al editar alta o baja '.$request['nombre']);
        }
        return $this->retorno();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre,$id)
    {
        if (TiposAltasBajas::destroy( $id)) {
            Session::flash('message-success', 'El tipo de alta '.$nombre.' fue eliminadado correctamente');
        } else {
            Session::flash('message-error', 'Error al eliminar el tipo '.$nombre);
        }
        return redirect('/tipos_altas_bajas');
    }
}
