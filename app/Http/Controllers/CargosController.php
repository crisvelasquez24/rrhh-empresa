<?php

namespace App\Http\Controllers;

use App\cargos;
use App\Departamentos_rh;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class CargosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $cargos = cargos::select('cargos.*', 'departamentos_rhs.nombre as nom_depar')
                ->join('departamentos_rhs', 'departamentos_rhs.id', '=', 'cargos.id_departamentos_rhs')
                ->get();
        return view('cargos.administrar', compact('cargos'));
    }

    public function retorno() {
        $script = "<script>\n";
        $script .= "window.parent.location.href = '/cargos'; \n";
        $script .= "</script>\n";
        echo $script;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $departamentos = Departamentos_rh::lists('nombre', 'id')->toArray();
        return view('cargos.crear', compact('departamentos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $cargos = cargos::create($request->all());
        if ($cargos) {
            Session::flash('message-success', 'El Cargo ' . $request['nombre'] . ' fue creado correctamente');
        } else {
            Session::flash('message-error', 'El Cargo ' . $request['nombre'] . ' no fue cread');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $departamentos = Departamentos_rh::lists('nombre', 'id')->toArray();
        $cargos = cargos::find($id);
        return view('cargos.actualizar', compact('cargos', 'departamentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $cargos = cargos::find($id);
        $cargos->fill($request->all());
        if ($cargos->save()) {
            Session::flash('message-success', 'El Cargo ' . $request['nombre'] . ' fue actualizado correctamente');
        } else {
            Session::flash('message-error', 'El Cargo ' . $request['nombre'] . ' no fue actualizado');
        }
        return $this->retorno();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre, $id) {
        if (cargos::destroy($id)) {
            Session::flash('message-success', 'El Cargo ' . $nombre . ' fue eliminado correctamente');
        } else {
            Session::flash('message-error', 'El Cargo ' . $nombre . ' no fue eliminado');
        }
        return redirect('/cargos');
    }

}
