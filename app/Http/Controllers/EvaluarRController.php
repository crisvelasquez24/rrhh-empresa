<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Evaluar;
use App\Hojadevida;
use App\Contratos;
use App\Preguntares;
use App\evaluacionRendimiento;
use Session;
use Redirect;

class EvaluarRController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function listing()
    {
        $evaluacion
            = evaluacionRendimiento::select(
                'hojas_vidas.p_nombre',
                'hojas_vidas.s_nombre',
                'hojas_vidas.p_apellido',
                'hojas_vidas.s_apellido',
                'evaluacionRendimiento.*'
            )
            ->join('hojas_vidas','id_hojadevida','=','hojas_vidas.id')
            ->get();

        return response()->json(
            $evaluacion->toArray()
        );
    }

    public function index()
    {
        $id = auth()->user()->id_perfil == 4  ? auth()->user()->id : false;
        $eval = evaluacionRendimiento::select('hojas_vidas.*','evaluacion_Rendimiento.*')
        ->join('hojas_vidas','id_hojadevida','=','hojas_vidas.id')
        ->when($id, function ($query, $id) {
            return $query->where('hojas_vidas.id_user', auth()->user()->id);
        })
        ->get();
        $pre = Preguntares::select('*')->get();
        return view('evaluacionRendimiento.listar', compact('eval','pre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $hojasdevida = Hojadevida::join('contratos', 'id_hojavida', '=', 'hojas_vidas.id')
        //         ->pluck('hojas_vidas.p_nombre','hojas_vidas.id')
        //         ->all();
        $hojasdevida
            = Hojadevida::join('contratos', 'id_hojavida', '=', 'hojas_vidas.id')
                ->where('hojas_vidas.id_user', auth()->user()->id)
                ->pluck('hojas_vidas.p_nombre','hojas_vidas.id')
                ->all();
        $preguntas = Preguntares::select('*')->get();
        $totalPreguntas = count($preguntas);
        return view('evaluacionRendimiento.crear', compact('hojasdevida','preguntas', 'totalPreguntas'));
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/hoja_de_vida'; \n";
        $script.= "</script>\n";
        echo $script;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $resp = evaluacionRendimiento::create($request->all());
        return $this->index();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $evaluar = Evaluar::select('p_psicologica','p_conocimiento','entrevista','evaluars.created_at as fecha')
                ->join('hojadevidas','hojadevidas.id','=','evaluars.id_hojadevidas')
                ->where('hojadevidas.id',$id)
                ->get();
        return view('evaluar.administrar', compact('evaluar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }



    public function setRespuesta($idres, $idhoja, $idp)
    {
        $resp
            = evaluacionRendimiento::create(['rta'=>$idres,'id_hojadevida'=>$idhoja,'id_preguntare'=>$idp]);
    }


}
