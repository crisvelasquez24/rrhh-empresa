<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contratos;
use App\Hojadevida;
use App\Cargos;
use App\User;
use App\Paises;
use App\Departamentos;
use App\Ciudades;
use App\TiposContratos;
use App\TiposSalarios;
use App\Administradoras;
use App\Archivos;
use Session;
use Redirect;
use DB;

class ContratosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $id = auth()->user()->id_perfil == 4  ? auth()->user()->id : false;
        $contratos = Contratos::select(
                    'hojas_vidas.*',
                    'contratos.*',
                    'contratos.id as id_contrato',
                    'tc.descripcion AS descripcion_tipo'
                    )
                ->join('hojas_vidas', 'hojas_vidas.id', '=', 'contratos.id_hojavida')
                ->join('tipos_contratos AS tc', 'tc.id', '=', 'contratos.id_tipo_contrato')
            ->when($id, function ($query, $id) {
                return $query->where('hojas_vidas.id_user', auth()->user()->id);
            })
                ->get();
        return view('contratos.administrar', compact('contratos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $modificando = 0;
        $paises = Paises::pluck('nombre','id')->all();
        $departamentos = Departamentos::pluck('nombre','id')->all();
        $ciudades = Ciudades::pluck('nombre','id')->all();
        $cargos = Cargos::pluck('nombre', 'id')->toArray();
        $tiposContratos = TiposContratos::pluck('descripcion', 'id')->toArray();
        $tiposSalarios = TiposSalarios::pluck('descripcion', 'id')->toArray();
        $administradoraSalud = Administradoras::where('tipo',1)->pluck('nombre','id')->toArray();
        $administradoraPension = Administradoras::where('tipo',2)->pluck('nombre','id')->toArray();
        return view('contratos.crear',
            compact(
                'hojasdevida',
                'modificando',
                'cargos',
                'tiposContratos',
                'administradoraSalud',
                'administradoraPension',
                'paises',
                'departamentos',
                'tiposSalarios',
                'ciudades'
            )
        );
    }

    public function cargar_postulados() {
        $hojasdevida
            = Hojadevida::select(
                'hojas_vidas.id',
                'p_nombre',
                's_nombre',
                'p_apellido',
                's_apellido',
                'tipo_doc',
                'doc'
            )
            ->join('users AS u', 'u.id', '=', 'hojas_vidas.id_user')
            ->get();
        return response()->json($hojasdevida->toArray());
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/contratos'; \n";
        $script.= "</script>\n";
        echo $script;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $archivos = Archivos::create([
            'contenido' => \File::get($request['documento']),
            'extension' => '',
            'nombre_archivo' => $request['documento']->getClientOriginalName(),
        ])->id;

        $request['documentos'] = $archivos;

        $contratos = Contratos::create($request->all());
        if ($contratos) {
            $id_user
                = Hojadevida::select('id_user')->where('id', $request['id_hojavida'])->get();
            $id_usuario = $id_user[0]->id_user;
            $usuario = User::find($id_usuario);
            $usuario->fill([
                'id_perfil' => 4
            ]);
            $usuario->save();
            Session::flash('message-success', 'El contrato fue creado correctamente');
        } else {
            Session::flash('message-error', 'El contrato no se pudo crear');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $contratos = Contratos::select()
                ->join('hojas_vidas AS h', 'h.id', '=', 'contratos.id_hojavida')
                ->join('cargos', 'cargos.id', '=', 'contratos.id_cargo')
                ->where('contratos.id', $id)
                ->get();
        $contrato = $contratos[0];
        return view('contratos.tipos.contrato', compact('contrato'));
    }

    public function download($file) {
        // \Storage::disk('imgContrato')->delete('*');
        $datos = explode(',', $file);
        $contenido = DB::table('archivos')->select('contenido','nombre_archivo')->where('id', '=', $datos[0])->get()->toArray();
        \Storage::disk('imgContrato')->put($datos[1].'-'.$contenido[0]->nombre_archivo, $contenido[0]->contenido);
        $file_path = public_path('fotos/contratos/'.$datos[1].'-'.$contenido[0]->nombre_archivo);
        return response()->download($file_path);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $modificando = 1;
        $paises = Paises::pluck('nombre','id')->all();
        $departamentos = Departamentos::pluck('nombre','id')->all();
        $ciudades = Ciudades::pluck('nombre','id')->all();
        $cargos = Cargos::pluck('nombre', 'id')->toArray();
        $tiposContratos = TiposContratos::pluck('descripcion', 'id')->toArray();
        $tiposSalarios = TiposSalarios::pluck('descripcion', 'id')->toArray();
        $administradoraSalud = Administradoras::where('tipo',1)->pluck('nombre','id')->toArray();
        $administradoraPension = Administradoras::where('tipo',2)->pluck('nombre','id')->toArray();
        $contratos = Contratos::find($id);
        return view(
            'contratos.actualizar',
            compact(
                'contratos',
                'modificando',
                'hojasdevida',
                'cargos',
                'tiposContratos',
                'administradoraSalud',
                'administradoraPension',
                'paises',
                'departamentos',
                'tiposSalarios',
                'ciudades'
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $contratos = Contratos::find($id);
        $contratos->fill($request->all());
        if($contratos->save()){
            Session::flash('message-success','El contrato fue actualizado correctamente');
        }else {
            Session::flash('message-error','El contrato no pudo ser actualizado');
        }
        return $this->retorno();
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($p_nombre, $p_apellido, $s_apellido, $id) {
        $contratos = Contratos::find($id);
        $contratos->fill([
            'estado' => 0
        ]);
        if($contratos->save()){
            Session::flash('message-success','El contrato de '.$p_nombre.' '.$p_apellido.' '.$s_apellido.' fue finalizado correctamente');
        }else{
            Session::flash('message-error','El contrato no se pudo finalizar');
        }
        return Redirect::to('/contratos');
    }

    public function getDepartamentos(Request $request, $id) {
        if ($request->ajax()) {
            $departamentos = Departamentos::where('id_pais', $id)->get();
            return response()->json($departamentos);
        }
    }

    public function getCiudades(Request $request, $id) {
        if ($request->ajax()) {
            $ciudades = Ciudades::where('id_departamento', $id)->get();
            return response()->json($ciudades);
        }
    }
}
