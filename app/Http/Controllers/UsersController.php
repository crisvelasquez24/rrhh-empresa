<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use App\User;
use App\Perfiles;
use Hash;
use Redirect;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $inactivos = User::select('users.*','perfiles.nombre as nom_perfil')
                ->join('perfiles','perfiles.id','=','users.id_perfil')
                ->where('estado',0)
                ->paginate(10);
        $publicos = User::select('users.*','perfiles.nombre as nom_perfil')
                ->join('perfiles','perfiles.id','=','users.id_perfil')
                ->where('estado',1)
                ->paginate(10);
        $activos = User::select('users.*','perfiles.nombre as nom_perfil')
                ->join('perfiles','perfiles.id','=','users.id_perfil')
                ->where('estado',2)
                ->paginate(10);
        return view('users.administrar', compact('publicos','activos','inactivos'));
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/users'; \n";
        $script.= "</script>\n";
        echo $script;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $perfiles = Perfiles::pluck('nombre','id')->all();
        $users = array();
        return view('users.crear', compact('perfiles','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $users
            = User::create([
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'estado' => 1,
                'id_perfil' => $request['id_perfil'],
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
            ]);
        if($users){
            Session::flash('message-success','El usuario '.$request['name'].' fue creado correctamente');
        }else{
            Session::flash('message-error','No pudeo ser creado');
        }
        return $this->retorno();
    }



    public function retorno2() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/'; \n";
        $script.= "</script>\n";
        echo $script;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    public function estado($name, $id) {
        $users = User::find($id);
        if($users->estado == 2){
            $users->fill([
                'estado' => 0
            ]);
            $users->save();
            Session::flash('message-success','El usuario '.$name.' fue inactivado correctamente');
        }
        else if($users->estado == 0){
            $users->fill([
                'estado' => 2
            ]);
            $users->save();
            Session::flash('message-success','El usuario '.$name.' fue activado correctamente');
        }
        return Redirect::to('/users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $userss = User::find($id);
        $users = array();
        $perfiles = Perfiles::pluck('nombre','id')->all();
        return view('users.actualizar', compact('userss','perfiles', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $users = User::find($id);
        $users->fill($request->all());
        if($users->save()){
            Session::flash('message-success','El usuario '.$request['name'].' fue actualizado correctamente');
        }else{
            Session::flash('message-error','El usuario no fua actualizado');
        }
        return $this->retorno();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function validar_email(Request $request){
         if ($request->ajax()) {
            $username = User::select()
            ->where('email',$request['username'])
            ->get();
            if(count($username) > 0){
                  return  '1';
            }
        }
    }

    public function cambiar_password($id) {
        return view('users.cambiar_password', compact('id'));
    }

    public function update_password(Request $request) {
        if(Hash::check($request['password_actual'],Auth::user()->password)){
            $users = User::find($request['id']);
            $users->fill([
                'password'=>$request['password_new']
                ]);
            $users->save();
            Session::flash('message-success','Cambio la contraseña correctamente');
        }
        else {
            Session::flash('message-error', 'No se ha podido cambiar la contraseña por que la contraseña actual no es correcta');
        }
        return $this->retorno2();
    }

}
