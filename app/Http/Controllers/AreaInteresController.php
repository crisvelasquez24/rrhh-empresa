<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Area_interes;
use Session;
use Redirect;

class AreaInteresController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $area_interes = Area_interes::all();
        return view('area_interes.administrar', compact('area_interes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('area_interes.crear');
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/area_interes'; \n";
        $script.= "</script>\n";
        echo $script;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $area_interes = Area_interes::create($request->all());
        if($area_interes){
            Session::flash('message-success','El Área de Interés '.$request['nombre']. ' fue creada correctamente');
        }else{
            Session::flash('message-error','El Área de Interés no fue creada');
        }
        
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $area_interes = Area_interes::find($id);
        return view('area_interes.actualizar', compact('area_interes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $area_interes = Area_interes::find($id);
        $area_interes->fill($request->all());
        if($area_interes->save()){
            Session::flash('message-success','El Área de Interés '.$request['nombre'].' fue actualizada correctamente');
        }else{
            Session::flash('message-error','El Área de Interés no se pudo actualizar');
        }
        return $this->retorno();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre,$id) {
        if(Area_interes::destroy($id)){
            Session::flash('message-success','El Área de Interés '.$nombre.' se ha eliminado');
        }else{
            Session::flash('message-error','El Área de Interés '.$nombre.' no pudo ser eliminada');
        }
        return Redirect::to('/area_interes');
    }

}
