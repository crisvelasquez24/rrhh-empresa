<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Hojadevida;
use DB;

class CarnetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datosHoja = Hojadevida::where('id_user', "=" ,$id)->get();
        $file_path 
            = "hojas_vidas/" . $this->download(
                $datosHoja[0]->foto . "," . $datosHoja[0]->doc.$datosHoja[0]->p_nombre
            );
        if ("hojas_vidas/" == $file_path) {
            $file_path = "user.png";
        }
        return view("carnetizacion.carnets", compact('datosHoja', 'file_path'));
    }

    public function download($file) {
        // \Storage::disk('imgContrato')->delete('*');
        $datos = explode(',', $file);
        $contenido 
            = DB::table('archivos')
                ->select('contenido', 'nombre_archivo')
                ->where('id', '=', $datos[0])
                ->get()->toArray()
            ;
        if (!$contenido) {
            return false;
        }
        \Storage::disk('imgHojas')->put($contenido[0]->nombre_archivo, $contenido[0]->contenido);
        $file_path = public_path('fotos/hojas_vidas/'.$datos[1].'-'.$contenido[0]->nombre_archivo);
        if (!$file_path) {
            return false;
        }
        return $contenido[0]->nombre_archivo;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
