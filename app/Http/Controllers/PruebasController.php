<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Festivos;
use DB;
use Carbon\Carbon;

class PruebasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $fecha1 = Carbon::createFromFormat('d-m-Y', '14-10-2018')->format('Y-m-d');
        $numDias = 15;

        $v_festivo = DB::table('festivos')->select('dia_festivo')->where('dia_festivo', '>', '2018-10-14')->get()->toArray();

        for ($i=0; $i < count($v_festivo); $i++) {
            $festivo[] = $v_festivo[$i]->dia_festivo;
        }
        $dias_habiles = $numDias;

        while (true) {
            $fecha1 = Carbon::createFromFormat('Y-m-d', $fecha1)->addDays(1)->format('Y-m-d');
            $n_dia = Carbon::createFromFormat('Y-m-d', $fecha1)->dayOfWeek;

            if (!in_array($fecha1, $festivo) and $n_dia != 0 and $n_dia != 6) {
                $dias_habiles -= 1;
            }
            if ($dias_habiles == 0) {
                break;
            }
        }
        dd($fecha1);
        // dd(Carbon::now()->addDays(1)->format('d-m-Y'));
        // dd(Carbon::now()->dayOfWeek);
        // $festivos = new \App\Custom\archivos(22);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pruebas.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $festivos = new \App\Custom\archivos($request['documentos'],22);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
