<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Hojadevida;
use App\Contratos;
use App\Vacaciones;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use Hash;
use Redirect;
use Carbon\Carbon;
use DB;
class VacacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = Hojadevida::select('hojas_vidas.p_nombre','hojas_vidas.s_nombre','hojas_vidas.doc','hojas_vidas.id AS empleado_id','hojas_vidas.p_apellido','hojas_vidas.s_apellido')
            ->join('contratos','id_hojavida',  '=', 'hojas_vidas.id')
            ->get();

    return view('vacaciones.administrar', compact('empleados'));
    }

    public function listar_vacaciones()
    {
        $vacaciones = Vacaciones::select('vacaciones.*','hojas_vidas.id AS empleado_id')
            ->join('hojas_vidas','hojas_vidas.id', '=', 'vacaciones.id_hojas_vidas' )
            ->join('contratos','hojas_vidas.id',  '=', 'contratos.id_hojavida')
            ->where('hojas_vidas.id_user', auth()->user()->id)
            ->get();

        return view('vacaciones.listar', compact('vacaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vacaciones = Hojadevida::pluck('id')->all();
          return view('vacaciones.crear',compact('vacaciones'));
    }

    public function retorno() {
        $script = "<script>\n";
        // $script.= "window.parent.location.href = 'registrar_vacaciones/'" . Session::get('id_empleado') . "; \n";
        $script.= "window.parent.location.href = 'registrar_vacaciones/'; \n";
        $script.= "</script>\n";
        echo $script;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $registro
            = Vacaciones::create([
                'id_hojas_vidas'=> Session::get('id_empleado'),
                'numero_dias' => $request['numero_dias'],
                'fecha_salida' => $request['fecha_salida'],
                'fecha_entrada' => $request['fecha_entrada'],
                'remuneracion' => $request['remuneracion'],
                'observaciones' => $request['observaciones'],
                'estado' => $request['estado'],
            ]);
        if($registro){
           // Session::forget('id_empleado');
            Session::flash('message-success','El registro  fue creado correctamente');
        }else{
            Session::flash('message-error','No pudeo ser creado');
        }
        return $this->retorno();
        // return Redirect::to('registrar_vacaciones/'. Session::get('id_empleado'));
    }

        public function getFechaEntrada(Request $request, $numDias, $fecha) {
        if ($request->ajax()) {
            $fecha1 = Carbon::createFromFormat('d-m-Y', $fecha)->format('Y-m-d');
            $numDias = $numDias;

            $v_festivo = DB::table('festivos')
                        ->select('dia_festivo')
                        ->where('dia_festivo', '>', '2018-10-14')
                        ->get()
                        ->toArray();

            for ($i=0; $i < count($v_festivo); $i++) {
                $festivo[] = $v_festivo[$i]->dia_festivo;
            }
            $dias_habiles = $numDias;

            while (true) {
                $fecha1 = Carbon::createFromFormat('Y-m-d', $fecha1)->addDays(1)->format('Y-m-d');
                $n_dia = Carbon::createFromFormat('Y-m-d', $fecha1)->dayOfWeek;

                if (!in_array($fecha1, $festivo) and $n_dia != 0 and $n_dia != 6) {
                    $dias_habiles -= 1;
                }
                if ($dias_habiles == 0) {
                    break;
                }
            }
            $fecha1 = Carbon::createFromFormat('Y-m-d', $fecha1)->format('d-m-Y');
            return response()->json($fecha1);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Session::put('id_empleado', $id);
        $empleados = Vacaciones::select('vacaciones.*','hojas_vidas.id AS empleado_id')
            ->join('hojas_vidas','hojas_vidas.id', '=', 'vacaciones.id_hojas_vidas' )
            ->join('contratos','hojas_vidas.id',  '=', 'contratos.id_hojavida')
            ->where('contratos.id_hojavida',$id)
            ->get();

        //return dd($empleados);
        return view('vacaciones.registrar', compact('empleados','id'));
    }

    /**
     * Metodo para cambiar estado
     */

    public function estado($name, $id) {
        $vac = Vacaciones::find($id);
        if($vac->estado == 1){
            $vac->fill([
                'estado' => 2
            ]);
            $vac->save();
            Session::flash('message-success','El registro '.$id.' fue cambiado de estado correctamente');
        }
        else if($vac->estado == 2){
            $vac->fill([
                'estado' => 1
            ]);
            $vac->save();
            Session::flash('message-success','El registro '.$id.' fue cambiado de estado correctamente');
        }
        return Redirect::to('registrar_vacaciones/'. Session::get('id_empleado'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
