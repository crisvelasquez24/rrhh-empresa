<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Vacantes;
use App\Postulados;
use App\Hojadevida;
use Session;

class VacantesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $vacantes = Vacantes::all();
        return view('vacantes.administrar', compact('vacantes'));
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/vacantes'; \n";
        $script.= "</script>\n";
        echo $script;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('vacantes.crear');
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $vacantes = Vacantes::create($request->all()); 
        if ($vacantes) {
            Session::flash('message-success', 'La vacante ' . $request['nombre'] . ' fue creada correctamente');
        } else {
            Session::flash('message-error', 'La vacante ' . $request['nombre'] . ' no fue creada');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    public function ver_vacantes() {
        $fecha_actual = date('Y-m-d');
        $vacantes = Vacantes::select('vacantes.*')
                ->where('vigencia', '>=', $fecha_actual)
                ->get();
        return view('vacantes.kardex', compact('vacantes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $vacantes = Vacantes::find($id);
        return view('vacantes.actualizar', compact('vacantes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $vacantes = Vacantes::find($id);
        $vacantes->fill($request->all());
        if ($vacantes->save()) {
            Session::flash('message-success', 'La vacante ' . $request['nombre'] . ' fue actualizada correctamente');
        } else {
            Session::flash('message-error', 'La vacante ' . $request['nombre'] . ' no fue actualizada');
        }
        return $this->retorno();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre, $id) {
        if (Vacantes::destroy($id)) {
            Session::flash('message-success', 'La vacante ' . $nombre . ' fue eliminada correctamente');
        } else {
            Session::flash('message-error', 'La vacante ' . $nombre . ' no fue eliminada');
        }
        return redirect('/vacantes');
    }

}
