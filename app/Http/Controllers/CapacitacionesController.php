<?php

namespace App\Http\Controllers;

use App\capacitaciones;
use App\cargos;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class capacitacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $capa = capacitaciones::select('capacitaciones.*', 'cargos.nombre as nombre_cargo')
            ->join('cargos', 'cargos.id', '=', 'capacitaciones.id_cargo')
            ->get();

        return view('capacitaciones.administrar', compact('capa'));
    }

    public function fecha()
    {
        return Carbon::now();

    }

    public function retorno()
    {
        $script = "<script>\n";
        $script .= "window.parent.location.href = '/capa'; \n";
        $script .= "</script>\n";
        echo $script;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cargos = cargos::lists('nombre', 'id')->toArray();
        return view('capacitaciones.crear', compact('cargos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $capa = capacitaciones::create($request->all());
        if ($capa) {
            Session::flash('message-success', 'La Capacitacion ' . $request['nombre'] . ' fue creado correctamente');
        } else {
            Session::flash('message-error', 'La Capacitacion ' . $request['nombre'] . ' no fue creado');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cargos = cargos::lists('nombre', 'id')->toArray();
        $capa   = capacitaciones::find($id);
        return view('capacitaciones.actualizar', compact('capa', 'cargos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $capa = capacitaciones::find($id);
        $capa->fill($request->all());
        if ($capa->save()) {
            Session::flash('message-success', 'La Capacitaciones ' . $request['nombre'] . ' fue actualizado correctamente');
        } else {
            Session::flash('message-error', 'La Capacitaciones ' . $request['nombre'] . ' no fue actualizado');
        }
        return $this->retorno();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre, $id)
    {
        if (capacitaciones::destroy($id)) {
            Session::flash('message-success', 'La Capacitaciones ' . $nombre . ' fue eliminado correctamente');
        } else {
            Session::flash('message-error', 'La Capacitaciones ' . $nombre . ' no fue eliminado');
        }
        return redirect('/capa');
    }
}
