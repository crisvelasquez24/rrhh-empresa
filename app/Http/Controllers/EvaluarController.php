<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Evaluar;
use App\Hojadevida;
use Session;
use Redirect;

class EvaluarController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('evaluacionRendimiento.crear');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/hoja_de_vida'; \n";
        $script.= "</script>\n";
        echo $script;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $evaluar = Evaluar::create([
            'p_psicologica' => $request['p_psicologica'],
            'p_conocimiento' => $request['p_conocimiento'],
            'entrevista' => $request['entrevista'],
            'id_hojadevidas' => $request['id'],
        ]);
        if($evaluar){
            Session::flash('message-success','Evalúo correctamente');
        }else{
            Session::flash('message-error','No pudo Evaluar');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $trab = Hojadevida::pluck('nombre','id')->all();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $evaluar = Evaluar::select('p_psicologica','p_conocimiento','entrevista','evaluars.created_at as fecha')
                ->join('hojadevidas','hojadevidas.id','=','evaluars.id_hojadevidas')
                ->where('hojadevidas.id',$id)
                ->get();
        return view('evaluar.administrar', compact('evaluar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) { 

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }


    


}
