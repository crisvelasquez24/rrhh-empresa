<?php

namespace App\Http\Controllers;

use App\TiposAltasBajas;
use App\Hojadevida;
use App\Contratos;
use App\RegistroAltasBajas;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class RegistroAltasBajasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $empleados = Hojadevida::select('hojas_vidas.p_nombre','hojas_vidas.s_nombre','hojas_vidas.doc','hojas_vidas.id AS empleado_id','hojas_vidas.p_apellido','hojas_vidas.s_apellido')
            ->join('contratos','id_hojavida',  '=', 'hojas_vidas.id')
            ->get();

    return view('registro_altas_bajas.administrar', compact('empleados'));
        //return dd($registro);
    }

    public function retorno() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/aplicar_altas_bajas'; \n";
        $script.= "</script>\n";
        echo $script;
    }
    public function retorno2() {
        $script = "<script>\n";
        $script.= "window.parent.location.href = '/aplicar_altas_bajas/". Session::get('id_empleado') ."'; \n";
        $script.= "</script>\n";
        echo $script;
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

          $tipo_altas_bajas = TiposAltasBajas::pluck('nombre','id')->all();
          return view('registro_altas_bajas.crear',compact('tipo_altas_bajas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idContrato = Contratos::select('id')->where('id_hojavida',Session::get('id_empleado'))->get();
        $registro
            = RegistroAltasBajas::create([
                'id_contrato'=> $idContrato[0]->id,
                'id_tipos_altas_bajas' => $request['id_tipos_altas_bajas'],
                'fecha' => $request['fecha'],
                'comentario' => $request['comentario'],
                'dias_trabajados' => $request['dias_trabajados'],
            ]);
        if($registro){
           // Session::forget('id_empleado');
            Session::flash('message-success','El registro  fue creado correctamente');
        }else{
            Session::flash('message-error','No pudeo ser creado');
        }
        return $this->retorno();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Session::put('id_empleado', $id);
        $empleados = RegistroAltasBajas::select('registro_altas_bajas.*','hojas_vidas.id AS empleado_id','tipo.nombre as nombre_tipo')
            ->join('contratos','contratos.id',  '=', 'registro_altas_bajas.id_contrato')
            ->join('hojas_vidas','hojas_vidas.id',  '=', 'contratos.id_hojavida')
            ->join('tipos_altas_bajas AS tipo','tipo.id',  '=', 'registro_altas_bajas.id_tipos_altas_bajas')
            ->where('contratos.id_hojavida',$id)
            ->get();


        return view('registro_altas_bajas.registrar', compact('empleados','id'));

    }

    public function show2()
    {
        $id = Hojadevida::where('id_user', auth()->user()->id)->first();
        $id = $id->id;
        Session::put('id_empleado', $id);
        $empleados = RegistroAltasBajas::select('registro_altas_bajas.*','hojas_vidas.id AS empleado_id','tipo.nombre as nombre_tipo')
            ->join('contratos','contratos.id',  '=', 'registro_altas_bajas.id_contrato')
            ->join('hojas_vidas','hojas_vidas.id',  '=', 'contratos.id_hojavida')
            ->join('tipos_altas_bajas AS tipo','tipo.id',  '=', 'registro_altas_bajas.id_tipos_altas_bajas')
            ->where('contratos.id_hojavida',$id)
            ->get();


        return view('registro_altas_bajas.registrar', compact('empleados','id'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $tipo_altas_bajas = TiposAltasBajas::pluck('nombre','id')->all();
        $registro = RegistroAltasBajas::find($id);
        return view('registro_altas_bajas.actualizar', compact('registro','tipo_altas_bajas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $registro = RegistroAltasBajas::find($id);
        $registro->fill($request->all());
        if ($registro->save()) {
            Session::flash('message-success', 'Registro '.$request['nombre'].' fue actualizado correctamente');
        } else {
            Session::flash('message-error', 'Error al editar el registro '.$request['nombre']);
        }
        return $this->retorno2();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nombre,$id)
    {
           if (RegistroAltasBajas::destroy($id)) {
            Session::flash('message-success', 'Registro '.$nombre.' fue eliminadado correctamente');
        } else {
            Session::flash('message-error', 'Error al eliminar el registro '.$nombre);
        }
        return redirect('aplicar_altas_bajas');

    }

}
