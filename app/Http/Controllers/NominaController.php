<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contratos;
use App\Hojadevida;
use App\Configuraciones;
use App\RegistroAltasBajas;
use App\PeriodosNomina;
use App\NominaGeneradas;
use DB;

class NominaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = auth()->user()->id_perfil == 4  ? auth()->user()->id : false;
        $empleados
            = DB::table('hojas_vidas')
            ->select(
                'p_nombre',
                'hojas_vidas.id AS id_hoja',
                'p_nombre',
                'p_apellido'
            )
             ->join('contratos', 'hojas_vidas.id', '=', 'contratos.id_hojavida')
            ->when($id, function ($query, $id) {
                return $query->where('id_user', auth()->user()->id);
            })
            ->get();

        $anoActual = date("Y");

        $peri = DB::select("SELECT periodo FROM nomina_generadas WHERE ano = '$anoActual'");

        $datos = [];
        for ($i=0; $i < count($peri); $i++) {
            $datos[] = $peri[$i]->periodo;
        }

        $PeriodosNomina = DB::table('periodos_nominas')
            ->whereNotIn('periodo', $datos)
            ->pluck('mes','periodo')->all();
        return view('nomina.administrar', compact('empleados', 'PeriodosNomina'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        for ($i=0; $i < count($request->get('empleado')); $i++) {

            $salario_basico = Hojadevida::select("c.salario_basico")
                ->join("contratos AS c", "hojas_vidas.id" ,"=", "c.id")
                ->where("c.id", $request->get('empleado')[$i]);

            $contratos
                = Contratos::where(
                    'id_hojavida',
                    $request->get('empleado')[$i])->first();

            $salarioBasico = $contratos->salario_basico;
            $salud = Configuraciones::select('valores')->where('siglas', 'SAL')->first()->valores;
            $pension = Configuraciones::select('valores')->where('siglas', 'PEN')->first()->valores;
            $salario = Configuraciones::select('valores')->where('siglas', 'SALMIN')->first()->valores;
            $transporte = Configuraciones::select('valores')->where('siglas', 'AUX')->first()->valores;
            $dias = RegistroAltasBajas::select('dias_trabajados')->where('id_tipos_altas_bajas', '5')->first()->dias_trabajados;
            $salarioBasico = ($salarioBasico / 30) * $dias;
            $altas
                = RegistroAltasBajas::select(
                    DB::raw('SUM(valor) As altas')
                )
                     ->Join('tipos_altas_bajas AS tab', 'tab.id', '=', 'registro_altas_bajas.id_tipos_altas_bajas')
                     ->Join('contratos AS c', 'c.id', '=', 'registro_altas_bajas.id_contrato')
                     ->where('tipo', 1)
                     ->where('id_hojavida', $request->get('empleado')[$i])
                     ->first()->altas;

            $bajas = RegistroAltasBajas::select(
                DB::raw('SUM(valor) As bajas'))
                     ->Join('tipos_altas_bajas AS tab', 'tab.id', '=', 'registro_altas_bajas.id_tipos_altas_bajas')
                     ->Join('contratos AS c', 'c.id', '=', 'registro_altas_bajas.id_contrato')
                     ->where('tipo', 2)
                     ->where('id_hojavida', $request->get('empleado')[$i])
                     ->first()->bajas;

            $nominaTotal = 0;
            $salud   = ($salarioBasico * $salud) / 100;
            $pension = ($salarioBasico * $pension) / 100;

            if ($salarioBasico <= (2 * $salario)) {
                $nominaTotal += $transporte;
            }

            $nominaTotal += $salarioBasico - $salud - $pension + $altas - $bajas;
            $anoActual = date("Y");

            $NominaGeneradas
                = NominaGeneradas::create(
                [
                    'total'       => $nominaTotal,
                    'salud'       => $salud,
                    'pension'     => $pension,
                    'auxilio'     => $transporte,
                    'periodo'     => $request->get('periodo'),
                    'ano'         => $anoActual,
                    'id_empleado' => $request->get('empleado')[$i],
                ]
            );
        }
        return response()->json(true);
    }

    public function generarNomina(Request $request)
    {
        return response()->json("hola");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $NominaGeneradas
            = NominaGeneradas::select(
                'nomina_generadas.*',
                'h.p_nombre',
                'h.s_nombre',
                'h.s_apellido',
                'h.tipo_doc',
                'h.doc',
                'h.p_apellido'
            )
            ->join('hojas_vidas AS h', 'h.id', '=', 'nomina_generadas.id_empleado')
            ->where('id_empleado', $id)
            ->get();

            return view('nomina.imprimir_nomina', compact('NominaGeneradas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
