<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capacitaciones extends Model {

    protected $table = 'capacitaciones';
    protected $fillable = ['id', 'nombre', 'fecha_inicio', 'fecha_fin', 'lugar', 'hora', 'tema', 'id_cargo'];

}
