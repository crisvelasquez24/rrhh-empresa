<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposAltasBajas extends Model
{
    protected $table = 'tipos_altas_bajas';
    protected $fillable = ['id','nombre','tipo','valor'];
}
