<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class RegistroAltasBajas extends Model
{
    protected $table = 'registro_altas_bajas';
    protected $fillable = ['id','id_contrato','id_tipos_altas_bajas','fecha','comentario','dias_trabajados'];

      public function setFechaAttribute($value)
    {
        $this->attributes['fecha'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }
}
