<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postulados extends Model {
    
    protected $table = 'postulados';
    protected $fillable = ['id','id_hoja_vida','id_vacante'];
    
}
