<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Festivos extends Model
{
    protected $table = 'festivos';
    protected $fillable = [
        'id',
        'dia_festivo',
        'nombre_festivo',
        'ano',
    ];
}
