<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Vacaciones extends Model
{
    protected $table = 'vacaciones';
    protected $fillable = ['id','id_hojas_vidas','numero_dias','fecha_salida','fecha_entrada','remuneracion','observaciones','estado'];

    public function setFechaSalidaAttribute($value)
    {
        $this->attributes['fecha_salida'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');

    }   

    public function setFechaEntradaAttribute($value)
    {
        $this->attributes['fecha_entrada'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');

    }
}
