<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class formaciones extends Model
{
    protected $table = 'formaciones';
    protected $fillable = [
        'id',
        'nivel_estudios',
        'area_estudios',
        'titulo_estudios',
        'institucion_estudios',
        'pais_estudios',
        'departamento_estudios',
        'ciudad_estudios',
        'funciones_estudios',
        'id_hoja',
    ];

}
