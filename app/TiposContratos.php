<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposContratos extends Model
{
    protected $table = 'tipos_contratos';
    protected $fillable = ['id','descripcion','caracteristicas'];
}
