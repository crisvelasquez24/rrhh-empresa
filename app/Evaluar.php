<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluar extends Model {
    
    protected $table = 'evaluars';
    protected $fillable = ['id','p_psicologica','p_conocimiento','entrevista','id_hojadevidas'];
    
}
