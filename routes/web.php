<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', function () {
    return redirect(route('dashboard'));
});

Route::resource('barcode', 'barcodeController');
Route::resource('configuracion', 'ConfiguracionesController');
Route::resource('control', 'ControlEmpleadosContorller');
Route::get('controlListar', 'ControlEmpleadosContorller@controlListar');

    Route::resource('pruebas', 'PruebasController');
    Route::resource('postulados', 'PostuladosController');

    Route::resource('convocatoria', 'ConvocatoriasController');
Route::group(['middleware'=>'auth'],function (){

    Route::get('dashboard','HomeController@index')->name('dashboard');
    Route::resource('perfiles', 'PerfilesController');
    Route::get('perfilesdel/{nombre}/{id}', 'PerfilesController@destroy')->name('perfilesdel');
    Route::resource('users', 'UsersController');
    Route::get('estado/{nombre}/{id}', 'UsersController@estado')->name('estado');

    Route::resource('evaluarR', 'EvaluarRController');
    Route::get('evaluarRen', 'EvaluarRController@listing');
    Route::get('respuesta/{idres}/{idhoja}/{idp}', 'EvaluarRController@setRespuesta')->name('respuesta');
    Route::resource('preguntare', 'preguntareController');

    Route::resource('hoja_de_vida', 'HojadeVidaController');

    Route::get('listarHojas', 'HojadeVidaController@listarHojas');

    Route::resource('liquidacion', 'LiquidacionController');

    Route::resource('administradora', 'AdministradorasController');
    Route::get('admindel/{nombre}/{id}', 'AdministradorasController@destroy')->name('admindel');

    Route::resource('tipoContrato', 'TiposContratosController');
    Route::get('tiposContratosdel/{nombre}/{id}', 'TiposContratosController@destroy')->name('tiposContratosdel');

    Route::resource('tipoSalario', 'TiposSalariosController');
    Route::get('tiposSalariosdel/{nombre}/{id}', 'TiposSalariosController@destroy')->name('tiposSalariosdel');

    /**
    * Rutas para crud altas y bajas
    */

    Route::get('altasbajasdel/{nombre}/{id}', 'TiposAltasBajasController@destroy')->name('altasbajasdel');
    Route::resource('tipos_altas_bajas', 'TiposAltasBajasController');

    /**
    * Rutas para aplicar las altas y bajas a un empleado
    */

    Route::get('registrodel/{nombre}/{id}', 'RegistroAltasBajasController@destroy')->name('registrodel');
    Route::resource('aplicar_altas_bajas', 'RegistroAltasBajasController');
    Route::get('listarNovedades', 'RegistroAltasBajasController@show2');
    Route::get('novedadescreate/{id}', 'RegistroAltasBajasController@create')->name('novedadescreate');


    Route::resource('contratos', 'ContratosController');
    Route::get('contratosdel/{p_nombre}/{p_apellido}/{s_apellido}/{id}/', 'ContratosController@destroy')->name('contratosdel');
    Route::get('contratosdownload/{file}', 'ContratosController@download')->name('contratosdownload');

    Route::get('cargar_postulados', 'ContratosController@cargar_postulados');
    Route::get('paisContrato/{id}', 'ContratosController@getDepartamentos')->name('paisContrato');


    Route::get('departamentoContrato/{id}', 'ContratosController@getCiudades')->name('departamentoContrato');
    Route::get('contratosshow/{id}', 'ContratosController@show')->name('contratosshow');



    Route::resource('festivos', 'FestivosController');
    Route::get('festivodel/{nombre}/{id}', 'FestivosController@destroy')->name('festivodel');

    /**
    * Rutas para registrar vacaciones
    */

    Route::resource('registrar_vacaciones', 'VacacionesController');
    Route::get('listar_vacaciones', 'VacacionesController@listar_vacaciones');
    Route::get('registrar_vacaciones/estadoVacaciones/{nombre}/{id}', 'VacacionesController@estado')->name('registrar_vacaciones/estadoVacaciones');
    Route::get('getfechaentrada/{dias}/{fecha}', 'VacacionesController@getFechaEntrada')->name('getfechaentrada');

    /**
     * Departamentos
     */
    Route::resource('depar', 'DepartamentosController');
    Route::get('deparE/{nombre}/{id}/', 'DepartamentosController@destroy')->name('deparE');

    /**
     * Vacantes
     */
    Route::resource('vacantes', 'VacantesController');
    Route::get('vacantesdel/{nombre}/{id}/', 'VacantesController@destroy')->name('vacantesdel');
    Route::get('ver_vacantes', 'VacantesController@ver_vacantes')->name('ver_vacantes');

    /**
     * Portulados
     */
    Route::resource('postulados', 'PostuladosController');
    Route::get('postular/{id_user}/{id_vacante}', 'PostuladosController@postular')->name('postular');

    /**
     * Areas de Interes
     */
    Route::resource('area_interes', 'AreaInteresController');
    Route::get('area_interesdel/{nombre}/{id}/', 'AreaInteresController@destroy')->name('area_interesdel');
    /**
     * Evaluaciones
     */
    Route::resource('evaluacion', 'EvaluacionesController');
    Route::resource('evalR', 'EvaluacionesController@cargar');
    Route::get('asignarEval/{id}', 'EvaluacionesController@asignarEval')->name('asignarEval');
    Route::get('asignar_empleado', 'EvaluacionesController@listing');
    Route::resource('asignar_evaluacion', 'AsignarEvaluacionController');

    /**
     * Nomina
     */
    Route::resource('nomina', 'NominaController');
    Route::post('generarNomina', 'NominaController@generarNomina')->name('generarNomina');

    Route::get('NominaShow/{id}', 'NominaController@show')->name('NominaShow');


    Route::resource('liquidacion', 'LiquidacionesController');
    Route::get('LiquidacionShow/{id}', 'LiquidacionesController@show')->name('LiquidacionShow');

    Route::resource('changePass', 'ResetPassController');
    Route::resource('carnets', 'CarnetsController');

    Route::get('gethijodepar/{id}', 'DepartamentosController@gethijodepar')->name('gethijodepar');
});




Route::get('cargar_postulados', 'ContratosController@cargar_postulados');
// Route::get('contratosshow/{id}/{tipo}', 'ContratosController@show')->name('contratosshow');

Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

Route::get('cambiarpassword/{id}', 'UsersController@cambiar_password')->name('cambiarpassword');
Route::post('updatepassword', 'UsersController@update_password')->name('updatepassword');
Route::get('estado/{nombre}/{id}', 'UsersController@estado')->name('estado');
Route::get('validaremail','UsersController@validar_email')->name('validaremail');

Route::resource('evaluar', 'EvaluarController');


Route::resource('convocatorias', 'ConvocatoriasController');
Route::get('v_postulados/{id}', 'ConvocatoriasController@getPostulados')->name('v_postulados');
Route::get('v_postulados_area_int/{id}', 'ConvocatoriasController@getPostulados_area_int')->name('v_postulados_area_int');




// Route::resource('login', 'LoginController');
Route::get('logout', 'LoginController@logout')->name('logout');

Route::resource('mail', 'MailController');

Route::get('paisanidado/{id}', 'HojadeVidaController@getDepartamentos')->name('paisanidado');
Route::get('departamentoanidado/{id}', 'HojadeVidaController@getCiudades')->name('departamentoanidado');
Route::get('suspostulaciones/{id}', 'HojadeVidaController@susPostulaciones')->name('suspostulaciones');
Route::get('susconvocatorias/{id}', 'HojadeVidaController@susConvocatorias')->name('susconvocatorias');


Route::get('estado_postulado','VacantesController@estado_postulado')->name('estado_postulado');



Route::resource('cargos', 'CargosController');
Route::get('cargosE/{nombre}/{id}/', 'CargosController@destroy')->name('cargosE');

Route::resource('capa', 'CapacitacionesController');
Route::get('capaE/{nombre}/{id}/', 'CapacitacionesController@destroy')->name('capaE');

Route::resource('respuesta', 'RespuestasController');
Route::get('respuestasAdmin', 'RespuestasController@administrar_respuestas')->name('respuestasAdmin');
Route::get('respuestasAdmin2', 'RespuestasController@listing')->name('respuestasAdmin2');
Route::get('responder', 'RespuestasController@index')->name('responder');
Route::get('cargar_evaluaciones', 'RespuestasController@cargar_evaluaciones');
Route::post('presentar_evaluacion', 'RespuestasController@presentar_evaluacion')->name('presentar_evaluacion');

Route::get('realizarEval/{id}', 'EvaluacionesController@realizarEval')->name('realizarEval');






Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
