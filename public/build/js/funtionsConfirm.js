// function del(id, controlador, msn) {
//     alert(controlador);
//     $.msgbox("¿ Está seguro que desea borrar el registro ? <br />" + msn, {
//         type: "confirm",
//         buttons: [
//             {type: "submit", value: "Aceptar"},
//             {type: "cancel", value: "Cancelar"}
//         ]
//     }, function (result) {
//         if (result === "Aceptar") {
//             window.location.href = controlador + id;
//         }
//     });
// }
function del(id, controlador, msn) {
     $.get("/gethijodepar/" + id + "", function (response, torres) {
        if(response.length > 0){
            $.msgbox(
                `<p style="color:#FF0000";>
                    Registro Tiene datos Asociados Imposible Continuar</p>`,
                 {
                type: "confirm",
                buttons: [                  
                    {type: "cancel", value: "Aceptar"}
                ]
            },function (result) {
             
            });
        }else{
            $.msgbox("¿ Está seguro que desea borrar el registro ? <br />" + msn, {
                type: "confirm",
                buttons: [
                    {type: "submit", value: "Aceptar"},
                    {type: "cancel", value: "Cancelar"}
                ]
            }, function (result) {
                if (result === "Aceptar") {
                    window.location.href = controlador + id;
                }
            });
        }
      
    });
}

function changeEstado(id, controlador, msn) {
    console.log(id);
    $.msgbox("¿ Está seguro que desea cambiar estado al registro " + msn + "?", {
        type: "confirm",
        buttons: [
            {type: "submit", value: "Aceptar"},
            {type: "cancel", value: "Cancelar"}
        ]
    }, function (result) {
        if (result === "Aceptar") {
            window.location.href = controlador + id;
        }
    });
}

function finContrato(id, controlador, msn) {
    console.log(id);
    $.msgbox("¿ Está seguro que desea Finalizar El contrato " + msn + "?", {
        type: "confirm",
        buttons: [
            {type: "submit", value: "Aceptar"},
            {type: "cancel", value: "Cancelar"}
        ]
    }, function (result) {
        if (result === "Aceptar") {
            window.location.href = controlador + id;
        }
    });
}
