@extends('layouts.app')
@section('titleForm','Crear Tipos Contratos')
@section('titleTab')
@endsection
@section('content')
    {!! Form::open(['route' => 'tipoContrato.store', 'method'=>'POST','id'=>'demo-form2','name'=>'frm',
    'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('tipos_contratos.forms.form_tiposContratos')
        <div class="form-group" align='center'>
            {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}
            <button type="button"  onclick="window.parent.location.href = '/tipoContrato'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection