<div class="item form-group">
    {!!Html::decode(Form::label('id_tipos_altas_bajas', 'Nombre de la novedad <span class="required obligatorio">*</span>',
        ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
       {!!Form::select('id_tipos_altas_bajas',[''=>'Seleccione...']+$tipo_altas_bajas ,
       null,['required'=>'required' ,'id'=> 'idTipoBaja','onchange'=>"myFunction(this)",'class'=>'form-control col-md-7 col-xs-12'])!!}
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha">Fecha:
        <span class="required obligatorio">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" class="form-control single_cal2" placeholder='Ingrese Fecha' name="fecha" required>
        <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('comentario', 'Comentario <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::text('comentario', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa el comentario '])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group" style="display: none;" id="diasTrabajados">
    {!!Html::decode(Form::label('comentario', 'Días Trabajados <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::text('dias_trabajados', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12',
        'placeholder'=>'Ingresa el comentario '])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>