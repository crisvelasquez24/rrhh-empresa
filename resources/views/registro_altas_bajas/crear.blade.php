@extends('layouts.formModal')
@section('titulo','Usuarios | Administrar')
@section('nombrepagina','Nuevo usuario')
@section('contenido')
<script type="text/javascript">
    function myFunction(caja){
        if (5 == caja.value) {
            diasTrabajados.style.display = 'block';
        } else {
            diasTrabajados.style.display = 'none';
            diasTrabajados.value = '30';
        }
    }
</script>
{!! Form::open(['route' => 'aplicar_altas_bajas.store', 'method'=>'POST','id'=>'frm','name'=>'frm', 'class'=>'form-horizontal']) !!}
@include('registro_altas_bajas.forms.form_registro')
<div class="form-group" align='center'>
    {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success','id'=>'btnuser'])!!}
    <button type="button"  onclick="window.parent.location.href = '/users'" class="btn btn-sm btn-warning">Cancelar</button>
</div>
{!!Form::close()!!}

@stop

@section('includesCss')
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
<script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('vendors/validator/validator.js')}}"></script>
<script type="text/javascript">
</script>
@endsection
{{-- @extends('layouts.formModal')


@section('content')
    {!! Form::open(['route' => 'aplicar_altas_bajas.store', 'method'=>'POST','id'=>'demo-form2','name'=>'frm',
    'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('registro_altas_bajas.forms.form_registro')
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}
            <button type="button"  onclick="window.parent.location.href = '/registro_altas_bajas'" class="btn btn-sm btn-warning">Cancelar</button>
            </div>
        </div>
    {!!Form::close()!!}
@endsection

 --}}