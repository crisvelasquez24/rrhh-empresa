@extends('layouts.formModal')
@section('titulo','Usuarios | Administrar')
@section('nombrepagina','Nuevo usuario')
@section('contenido')

{!! Form::model($registro,['route' => ['aplicar_altas_bajas.update',$registro->id], 'id'=>'frm','name'=>'frm',
    'method'=>'PUT', 'class'=>'form-horizontal form-label-left','novalidate']) !!}    
        @include('registro_altas_bajas.forms.form_registro')
        <div class="form-group" align='center'>
           {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}     
            <button type="button"  onclick="window.parent.location.href = '/aplicar_altas_bajas'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}

@stop

@section('includesCss')
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
<script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection