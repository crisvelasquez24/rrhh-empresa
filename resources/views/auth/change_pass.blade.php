<div class="modal inmodal" id="changePass" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
        <section class="login_content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-laptop modal-icon"></i>
                <h4 class="modal-title">Restablecer Contraseña</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div class="form-group"><label>Contraseña</label>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                <input id="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                </div>
            </div>            
            <div class="modal-body{{ $errors->has('password_confirmation') ? ' has-error' : '' }}"
                style="height: 100px;">
                <div class="form-group"><label>Confirmar Contraseña</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" id="botonContinuarPass" class="btn btn-primary">Continuar</button>
            </div>
        </section>
        </div>
    </div>
</div>