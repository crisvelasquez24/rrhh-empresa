@extends('layouts.auth')
@section('title','Login')
@section('content')

    <div class="animate form login_form">
        <section class="login_content">
            <form role="form" method="POST" action="{{ route('password.email') }}">
                {{csrf_field()}}
                <h1>Restaurar contraseña</h1>
                <div class="form-group{{ $errors->has('email') ? '  has-error' : '' }}">
                    <input type="email" class="form-control" name="email" placeholder="email" required="" />
                    @if ($errors->has('email'))
                        <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="clearfix"></div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Enviar enlace para restablecer contraseña') }}
                        </button>
                    </div>
                </div>

                    <div class="clearfix"></div>
                    <br />

                    <div>
                        <h1><i class="fa fa-plus-circle"></i> {{config('app.name')}}</h1>
                        <p>©{{date('Y')}} </p>
                    </div>
                </div>
            </form>
        </section>
    </div>
@endsection
