@extends('layouts.app')
@section('titleForm','Administrar Contratos')
@section('titleTab')
<div class="title_right">
    <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
        <div class="input-group">
            <a class="form-control" placeholder="Search for...">Crear Contratos</a>
            <span class="input-group-btn">
            {!!Html::decode(
                link_to_route(
                    'contratos.create',
                    '<button class="btn btn-sm btn-warning"><i class="fa fa-plus-square"></i> <span style="color:#FFFFFF";>Agregar</span></button>',
                    [],
                    ['class'=>'']
                )
            )!!}
            </span>
        </div>
    </div>
</div>
@endsection
@section('content')
<!-- page content -->
<div class="center_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <br/>
                                    <table class="table table-striped table-hover table-bordered">
                                        <tbody>
                                            <tr>
                                                <th style="color:black">EMPRESA</th>
                                                <td style="color:black">UNIDADES TECNOLOGICAS DE SANTANDER</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">NIT</th>
                                                <td style="color:black">900.127.675-2</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">DIRECCION</th>
                                                <td style="color:black">Cra.4 n° 5-04 Buearamanga</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">TELEFONO</th>
                                                <td style="color:black">031-6484891</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">REPRESENTANTE LEGAL</th>
                                                <td style="color:black">Daniela Jaramillo Correa</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">CARGO</th>
                                                <td style="color:black">Gerente Comercial</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                @foreach($LiquidacionesGeneradas as $LiquidacionesGenerada)
                                                    <th style="color:black">NOMBRE EMPLEADO</th>
                                                    <td style="color:black">
                                                        {{$LiquidacionesGenerada->p_nombre}} {{$LiquidacionesGenerada->s_nombre}}
                                                        {{$LiquidacionesGenerada->p_apellido}} {{$LiquidacionesGenerada->s_apellido}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="color:black">IDENTIFICACION</th>
                                                    <td style="color:black">{{$LiquidacionesGenerada->tipo_doc}} {{$LiquidacionesGenerada->doc}}</td>
                                                </tr>
                                                <?php
                                                $valorLiquidacion = number_format($LiquidacionesGenerada->valor_liquidacion, 2);
                                                $cesantias = number_format($LiquidacionesGenerada->cesantias, 2);
                                                $prima = number_format($LiquidacionesGenerada->prima, 2);
                                                $intereses = number_format($LiquidacionesGenerada->intereses, 2);
                                                $vacaciones = number_format($LiquidacionesGenerada->vacaciones, 2);
                                                ?>
                                                <tr>
                                                    <th style="color:black">Total a pagar</th>
                                                    <td style="color:black">$ {{$valorLiquidacion}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="color:black">Cesantias</th>
                                                    <td style="color:black">$ {{$cesantias}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="color:black">Prima</th>
                                                    <td style="color:black">$ {{$prima}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="color:black">Intereses</th>
                                                    <td style="color:black">$ {{$intereses}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="color:black">Vacaciones</th>
                                                    <td style="color:black">$ {{$vacaciones}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div align='center' id="btn_print">
                                        <button type="button" onclick="window.print()" class="btn btn-sm btn-success"><i class="fa fa-print"></i>&nbsp;Imprimir</button>
                                        <button type="button"  onclick="window.parent.location.href = '/contratos'" class="btn btn-sm btn-warning">Atras</button>
                                    </div>
                                    <style type="text/css" media="print">
                                        @media print {
                                        #btn_print{display: none}
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('includesCss')
<link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
<link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
<link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
<link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendors/msgbox/jquery.msgbox.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
<script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
<script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
<script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
<script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
<script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
<script src="{{asset('vendors/msgbox/jquery.msgbox.js')}}"></script>
<script src="{{asset('build/js/datatables.js')}}"></script>
<script src="{{asset('build/js/funtionsConfirm.js')}}"></script>
@endsection