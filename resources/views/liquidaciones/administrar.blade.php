@extends('layouts.app')
@section('titleForm','Administrar Liquidaciones')
@section('titleTab')
    <div class="title_right">
        <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <span class="input-group-btn">

                </span>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- page content -->
    <div class="center_col" role="main">
      <div class="">

        <div class="clearfix"></div>

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_content">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                            <thead>
                                <tr>
                                    <th>
                                    <th><input type="checkbox" id="check-all"
                                        class="flat">
                                    </th>
                                    </th>
                                    <th>Nombres</th>
                                    <th>Apelidos</th>
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($empleados as $data)
                                    <tr>
                                        <td>
                                        <th>
                                            <input type="checkbox" value="{{ $data->id_hoja }}"
                                                name="empleado"
                                            class="flat">
                                        </th>
                                        </td>
                                        <td>{{ $data->p_nombre }} {{ $data->s_nombre }}</td>
                                        <td>{{ $data->p_apellido }} {{ $data->s_apellido }}</td>
                                        <td>
                                            {!!Html::decode(
                                                link_to_route(
                                                    'LiquidacionShow',
                                                    '<i class="fa fa-eye" title="Ver Liquidaciones"></i>',
                                                    [$data->id_hoja],
                                                    ['class'=>'']
                                            ))!!}&nbsp;
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                       <button  id="btnGenerar" class="btn btn-sm btn-primary">
                            <i class="fa fa-plus-square"></i>
                        <span style="color:#FFFFFF";>Generar Liquidaciones</span>
                        </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('includesCss')
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/msgbox/jquery.msgbox.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/msgbox/jquery.msgbox.js')}}"></script>
    <script src="{{asset('build/js/datatables.js')}}"></script>
    <script src="{{asset('build/js/funtionsConfirm.js')}}"></script>
    <script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>
    <script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>
    <link href="{{asset('vendors/color-box/colorbox.css') }}" rel="stylesheet">
    <script type="text/javascript">
    $(document).ready(function() {
        $("#btnGenerar").click(function() {
            var empleado = [];
            $.each($("input[name='empleado']:checked"), function(){
                empleado.push($(this).val());
            });

            if (0 == empleado.length) {
                $.msgbox('<p style="color:#FF0000";>Seleciones empleados Imposible Continuar</p>', {
                    type: "confirm",
                    buttons: [
                    {type: "cancel", value: "Aceptar"}
                    ]
                },function (result) {

                });
                return;
            } else {
                $.msgbox("¿ Está seguro que desea Generar liquidación ? <br />", {
                    type: "confirm",
                    buttons: [
                        {type: "submit", value: "Aceptar"},
                        {type: "cancel", value: "Cancelar"}
                    ]
                }, function (result) {
                    if (result === "Aceptar") {
                        var route = "liquidacion/";
                        var token = $("#token").val();
                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'POST',
                            dataType: 'json',
                            data:{empleado: empleado},

                            success:function(response){
                                alert(response);
                                $.msgbox('<p style="color:#FF0000";>Liquidaciones generadas Correctamente</p>', {
                                    type: "confirm",
                                    buttons: [
                                    {type: "cancel", value: "Aceptar"}
                                    ]
                                },function (result) {
                                    window.location.href = 'liquidacion';
                                });
                            },
                            error:function(msj){
                                $.msgbox('<p style="color:#FF0000";>Error al crear Liquidación</p>', {
                                    type: "confirm",
                                    buttons: [
                                    {type: "cancel", value: "Aceptar"}
                                    ]
                                },function (result) {

                                });
                            }
                        });
                    }
                });

            }

        });
    });

    </script>
@endsection