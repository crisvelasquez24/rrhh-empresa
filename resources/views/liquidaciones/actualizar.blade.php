@extends('layouts.app')
@section('titleForm','Registrar Usuarios')
@section('content')
    {!! Form::model($users,['route' => ['users.update',$users->id],'id'=>'frm','name'=>'frm',
         'method'=>'PUT', 'class'=>'form-horizontal']) !!}
        @include('users.forms.form_users')
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
               {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}
                <button type="button"  onclick="window.parent.location.href = '/users'"
                    class="btn btn-sm btn-warning">Cancelar</button>
            </div>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#idemail').blur(function () {
                var username = $("#idemail").val();
                $.ajax({
                    type: "GET",
                    url: "/validaremail/",
                    data: {username: username},
                    success: function (data) {
                        if (data === "1") {
                            validateAjax = 1;
                            validator.checkField.apply($("#idemail"));
                            return;
                        } else {
                            validateAjax = 0;
                            $("#divEmail").find("div")[1].remove();
                            $( "#divEmail" ).removeClass( "bad" );
                            return;
                        }
                    }
                });

            });
        });
    </script>
@endsection