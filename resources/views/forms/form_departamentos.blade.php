<div class="item form-group">
    {!!Html::decode(Form::label('nombre', 'Nombre <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::text('nombre', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa el nombre del departamento'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('funciones', 'Funciones <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::textarea('funciones', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa las funciones del departamento', 'style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>



