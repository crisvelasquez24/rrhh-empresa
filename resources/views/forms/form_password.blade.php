<div class="item form-group">
    {!!Html::decode(Form::label('password_actual', 'Contraseña actual <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::password('password_actual',['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa la contraseña actual'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('password_new', 'Nueva contraseña <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::password('password_new',['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa la nueva contraseña','data-minlength'=>'6','data-minlength-error'=>'Minimo de 6 caracteres','id'=>'input_password'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('password_confirm', 'Confirmar contraseña <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::password('password_confirm', ['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Confirma la nueva contraseña','data-match'=>'#input_password','data-match-error'=>'Las contraseñas no coinciden.'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>

