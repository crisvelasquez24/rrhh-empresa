<div class="item form-group">
    {!!Html::decode(Form::label('tipo_contrato', 'Tipo de contrato <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('tipo_contrato', [''=>'Seleccione un tipo de contrato','1'=>'Contrato Definido','2'=>'Contrato Indefinido','3'=>'Contrato de Aprendizaje','4'=>'Contrato por prestación de servicios','5'=>'Contrato Temporal'],null,['required'=>'required' , 'class'=>'form-control', 'id'=>'contrato'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('meses', 'Tiempo <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('meses', [''=>'Seleccione la cantidad de meses'],null,['required'=>'required' , 'class'=>'form-control', 'id'=>'tipocontrato'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('eps', 'Eps <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('eps',[''=>'Selecione eps...','Salud Total'=>'Salud Total','Cafe Salud'=>'Cafe Salud','Coomeva'=>'Coomeva','Nueva Eps'=>'Nueva Eps','Caprecom'=>'Caprecom'], null,['required'=>'required' , 'class'=>'form-control '])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('arl', 'Arl <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('arl',[''=>'Selecione arl...','Colpatria'=>'Colpatria','Sura'=>'Sura'], null,['required'=>'required' , 'class'=>'form-control '])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('documentos', 'Documentos <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::file('documentos', null,['required'=>'required' , 'class'=>'form-control'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
