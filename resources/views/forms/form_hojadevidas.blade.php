<div role="container">
    <div class="row">
        {{-- <div class="col-md-2 col-sm-2 col-xs-12"></div> --}}
        <div class="col-xs-10 col- md-10 col-sm-10 col-xs-offset-1 col-col-offset-1">
            <div class="x_panel">
                <div class="x_content">
                    <!-- Ini SmartWizard Content -->              
                    <div id="wizard" class="form_wizard wizard_horizontal">
                        <ul class="wizard_steps">
                            <li>
                                <a href="#step-1">
                                <span class="step_no">1</span>
                                <span class="step_descr">
                                Datos Basicos<br />
                                <small>paso 1 descripción</small>
                                </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-2">
                                <span class="step_no">2</span>
                                <span class="step_descr">
                                Información laboral<br />
                                <small>Paso 2 descripción</small>
                                </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-3">
                                <span class="step_no">3</span>
                                <span class="step_descr">
                                Experiencia laboral<br />
                                <small>Paso 3 description</small>
                                </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-4">
                                <span class="step_no">4</span>
                                <span class="step_descr">
                                Educación formal<br />
                                <small>Paso 4 descripción</small>
                                </span>
                                </a>
                            </li>
                        </ul>
                        <div id="step-1">
                            <span style="text-align: center;" class="section">
                            Información Personal
                            </span>
                            <button type="button" class="btn btn-round btn-primary">Primary</button>
       
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="documento">Foto
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?php if (isset($hojadevida)) { ?>
                                        {!!Html::image('fotos/'.$hojadevida->foto,null,['class'=>'img-thumbnail profile_img', 'style'=>'width:150px'])!!}
                                    <?php } ?>
                                    {!!Form::file('foto',['required'=>'required','class'=>''])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_area_interes">Area de Interes 
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                       {!!Form::select('id_area_interes', [''=>'Seleccione...']+$area_interes,null,
                                        ['required'=>'required','class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="p_nombre">Primer Nombre
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text('p_nombre', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Segundo Nombre
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text('s_nombre', null,['class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Primer apellido
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text('p_apellido', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Segundo apellido
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text('s_apellido', null,['class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Tipo de Identificación
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('tipo_doc', [''=>'Seleccione...','C.C'=>'Cedula de Ciudadania','C.E'=>'Cedula Extranjera','T.I'=>'Tarjeta de Identidad'] ,null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Identificación
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::number('doc', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Estado Civil:
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::select('estado_civil', [''=>'Seleccione...','Casado(a)'=>'Casado(a)','Divorciado(a)'=>'Divorciado(a)','Soltero(a)'=>'Soltero(a)','Union Libre'=>'Union Libre','Viudo(a)'=>'Viudo(a)'], null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Genero:
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('genero', [''=>'Seleccione...','Masculino'=>'Masculino','Femenino'=>'Femenino','Otro'=>'Otro'], null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Email:
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::email('email', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','id'=>'idemail'])!!}
                                </div>
                            </div>
                            <span style="text-align: center;" class="section">
                                Datos de Nacimiento
                            </span>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Fecha:
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     <input type="text" class="form-control" name="fecha_nac" data-inputmask="'mask': '99/99/9999'" required>
                                    <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Pais:
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('id_paises',[''=>'Seleccione...']+$paises, null,['required'=>'required', 'id'=>'pais', 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Departamento:
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('id_departamentos',[''=>'Seleccione...']+$departamentos, null,['required'=>'required','id'=>'departamento', 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Ciudad:
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('id_ciudades',[''=>'Seleccione...']+$ciudades, 
                                    null,['required'=>'required','id'=>'ciudad', 'class'=>'form-control'])!!}
                                </div>
                            </div>

                            <span style="text-align: center;" class="section">
                                Recidencia Actual
                            </span>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Dirección
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::text('dir', null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div> 

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Pais
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('paises_hab',[''=>'Seleccione...']+$paises, null,['required'=>'required' ,'id'=>'pais_2', 'class'=>'form-control'])!!}
                                </div>
                            </div> 

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Departamento
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('departamentos_hab',[''=>'Seleccione...']+$departamentos, null,['required'=>'required','id'=>'departamento_2', 'class'=>'form-control'])!!}
                                </div>
                            </div> 

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Ciudad
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('ciudades_hab',[''=>'Seleccione...']+$ciudades, null,['required'=>'required','id'=>'ciudad_2', 'class'=>'form-control'])!!}
                                </div>
                            </div> 

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Telefono
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::number('tel', null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div> 

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Movil
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::number('movil', null,['class'=>'form-control'])!!}
                                </div>
                            </div>                        
                          {{--   <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion">Dirección 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="direccion" type="text" name="direccion" id="direccion" 
                                        data-validate-length-range="5,20" 
                                        class="optional form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">E-Mail 
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" id="email" id="email" name="email" required="required" 
                                        class="form-control col-md-7 col-xs-12">
                                </div>
                            </div> --}}
                        </div>
                        <div id="step-2">
                            <span style="text-align: center;" class="section">
                                Paso 2 Información Laboral
                            </span>
                            
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Profesión u oficio 
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('profesion', [''=>'Seleccione...','Asministración de Empresas'=>'Asministración de Empresas',
                                        'Tecnologo en Desarollo de Sistemas Imformaticos'=>'Tecnologo en Desarollo de Sistemas Imformaticos',
                                        'Contador Publico'=>'Contador Publico','Ingeniero en Sistemas'=>'Ingeniero en Sistemas',
                                        'Sin Porfeción'=>'Sin Porfeción'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Perfil Laboral 
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::textarea('perfil_laboral', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12',
                                        'placeholder'=>'Ingresa la información del Perfil Porfecional','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Años de experiencia 
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                   {!!Form::select('anos_experiencia', [''=>'Seleccione...','sin experiencia'=>'Sin Experiencia',
                                    'menos de un año'=>'menos de un año','1'=>'1 año','2'=>'2 Años','3'=>'3 Años','4'=>'4 Años','5'=>'5 Años',
                                    '6'=>'6 Años','7'=>'7 Años','8'=>'8 Años','9'=>'9 Años','10'=>'10 Años','mas de 10 años'=>'Mas de 10 Años'], 
                                        null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Aspiración salarial (millones) 
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('aspiracion_salarial', [''=>'Seleccione...','menos de 1$'=>'Menos de 1$','1$ a 2$'=>'1$ a 2$','2$ a 3$'=>'2$ a 3$','mas de 3$'=>'Mas de 3$'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trabaja_actualmente"> ¿Trabaja actualmente?:
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p>
                                    SI:
                                    <input type="radio" class="flat" name="trabaja_actualmente" id="" value="0" 
                                    required/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;
                                    No:
                                    <input id="radio1" type="radio" class="flat" name="trabaja_actualmente" id="" 
                                    checked="" value="1" />
                                    </p>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trabaja_actualmente"> 
                                    Posibilidad de trasladarse (a otra ciudad o país):
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p>
                                    SI:
                                    <input type="radio" class="flat" name="posibilidad_translado" id="" value="0" 
                                    required/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;
                                    No:
                                    <input id="radio1" type="radio" class="flat" name="posibilidad_translado" id="" 
                                    checked="" value="1" />
                                    </p>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trabaja_actualmente"> 
                                    Posibilidad de viajar 
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p>
                                    SI:
                                    <input type="radio" class="flat" name="posibilidad_viajar" id="" value="0" 
                                    required/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;
                                    No:
                                    <input id="radio1" type="radio" class="flat" name="posibilidad_viajar" id="" 
                                    checked="" value="1" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="step-3">
                            <span style="text-align: center;" class="section">
                                paso 3 Experiencia laboral
                            </span>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Empresa
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::text('empresa', null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Teléfono de la empresa
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::text('telefono_empresa', null,['class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Sector de la empresa
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::select('sector_empresa', [''=>'Seleccione...','financiero'=>'Financiero','telecomunicaciones'=>'Telecomunicaciones','sistemas'=>'Sistemas','agropecuario'=>'Agropecuario'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Cargo
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::text('cargo_empresa', null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Jefe Inmediato
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text('jefe_empresa', null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Fecha Ingreso
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control" name="fecha_ingreso_empresa" data-inputmask="'mask': '99/99/9999'" required>       
                                    <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Fecha Finalización
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="fecha_finalizacion_empresa" data-inputmask="'mask': '99/99/9999'" required>
                                    <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Tiempo Experiencia
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p id="esperiencia" class="form-control"></p>
                                    <input type="hidden" id="esperiencia2"  name="tiempo_experiencia">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Pais
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('pais_empresa',[''=>'Seleccione...']+$paises, null,['required'=>'required', 'id'=>'paisexperiencia', 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Departamento
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('departamento_empresa',[''=>'Seleccione...']+$departamentos, null,['required'=>'required','id'=>'departamentoexperiencia', 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Ciudad
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('ciudad_empresa',[''=>'Seleccione...']+$ciudades, null,['required'=>'required','id'=>'ciudadexperiencia', 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Funciones y logros alcanzadosl
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::textarea('funciones_empresa', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingrese Logros Alcanzados','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
                                </div>
                            </div>
                       
                        </div>

                        <div id="step-4">

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Nivel de estudios
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::select('nivel_estudios', [''=>'Seleccione...','prescolas'=>'Basica Primaria','basica segundaria'=>'Basica Segundaria','tecnico laboral'=>'Tecnico Laboral','tecnologia'=>'Tecnología','ingenieria'=>'Ingenieria','especializacion'=>'Especialización','maestria'=>'Maestria','doctorado'=>'Doctorado'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Área de estudios
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::select('area_estudios', [''=>'Seleccione...','financiero'=>'Financiero','telecomunicaciones'=>'Telecomunicaciones','sistemas'=>'Sistemas','agropecuario'=>'Agropecuario'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Título
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::text('titulo_estudios', null,['class'=>'form-control','placeholder'=>'Ingrese Titulo'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Institución en la que estudió
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text('institucion_estudios', null,['class'=>'form-control','placeholder'=>'Ingrese Institución'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Pais
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('pais_estudios',[''=>'Seleccione...']+$paises, null,['required'=>'required', 'id'=>'paisestudio', 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Departamento
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('departamento_estudios',[''=>'Seleccione...']+$departamentos, null,['required'=>'required','id'=>'departamentoestudio', 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Ciudad
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('ciudad_estudios',[''=>'Seleccione...']+$ciudades, null,['required'=>'required','id'=>'ciudadestudio', 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Funciones y logros alcanzadosl
                                <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::textarea('funciones_estudios', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingrese Logros Alcanzados','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
                                </div>
                            </div>
                        </div>
                        <!-- End SmartWizard Content -->
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-2 col-sm-2 col-xs-12"></div> --}}
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>