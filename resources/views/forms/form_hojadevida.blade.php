@section('tabs')
<ul class="nav nav-tabs">
    <li class="active"><a href="#d" data-toggle="tab">Datos Basicos <i class="fa"></i></a></li>
    <li><a href="#ca" data-toggle="tab">Información laboral <i class="fa"></i></a></li>
    <li><a href="#cd" data-toggle="tab">Perfil Profesional <i class="fa"></i></a></li>
    <li><a href="#a" data-toggle="tab">Formación <i class="fa"></i></a></li>
    
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="d">

        <br/>
        <h2 align='center'>Información Personal</h2>
        <br/>        

        <div class="x_panel" style="background: #F7F7F9">
            <div class="item form-group">
                {!!Html::decode(Form::label('foto', 'Fotografia: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                    <?php if (isset($hojadevida)) { ?>
                        {!!Html::image('fotos/'.$hojadevida->foto,null,['class'=>'img-thumbnail profile_img', 'style'=>'width:150px'])!!}
                    <?php } ?>
                    {!!Form::file('foto')!!}
                </div>
                {!!Html::decode(Form::label('id_area_interes', 'Área de Interés: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::select('id_area_interes', [''=>'Seleccione...']+$area_interes,null,['required'=>'required','class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
            </div>        
            <div class="item form-group">
                {!!Html::decode(Form::label('p_nombre', 'Primer Nombre: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::text('p_nombre', null,['required'=>'required' , 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
                {!!Html::decode(Form::label('s_nombre', 'Segundo Nombre: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::text('s_nombre', null,['class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
            </div>        
            <div class="item form-group">
                {!!Html::decode(Form::label('p_apellido', 'Primer Apellido: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::text('p_apellido', null,['required'=>'required' , 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
                {!!Html::decode(Form::label('s_apellido', 'Segundo Apellido: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::text('s_apellido', null,['required'=>'required' , 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="item form-group">
                {!!Html::decode(Form::label('tipo_doc', 'Tipo de Identificación: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::select('tipo_doc', [''=>'Seleccione...','C.C'=>'Cedula de Ciudadania','C.E'=>'Cedula Extranjera','T.I'=>'Tarjeta de Identidad'] ,null,['required'=>'required' , 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
                {!!Html::decode(Form::label('doc', 'Identificación: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::number('doc', null,['required'=>'required' , 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="item form-group">
                {!!Html::decode(Form::label('estado_civil', 'Estado Civil: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::select('estado_civil', [''=>'Seleccione...','Casado(a)'=>'Casado(a)','Divorciado(a)'=>'Divorciado(a)','Soltero(a)'=>'Soltero(a)','Union Libre'=>'Union Libre','Viudo(a)'=>'Viudo(a)'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
                {!!Html::decode(Form::label('genero', 'Genero: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::select('genero', [''=>'Seleccione...','Masculino'=>'Masculino','Femenino'=>'Femenino','Otro'=>'Otro'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="item form-group">
                {!!Html::decode(Form::label('email', 'Email: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::email('email', null,['required'=>'required' , 'class'=>'form-control','id'=>'idemail'])!!}
                    <div class="help-block with-errors" style="color: #90111A" id="info"></div>
                </div>
                {!!Html::decode(Form::label('email', 'Confirmar Email : ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::email('email', null,['required'=>'required' , 'class'=>'form-control','data-match'=>'#idemail', 'data-match-error'=>'El email no coincide.'])!!}
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>

        <br/>
        <h2 align='center'>Datos de Nacimiento</h2>
        <br/>
        <div class="x_panel" style="background: #F7F7F9">
            <div class="item form-group">
                {!!Html::decode(Form::label('fecha_nac', 'Fecha: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2 ']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::text('fecha_nac', null,['required'=>'required' , 'class'=>'form-control fecha','placeholder'=>'aaaa/mm/dd'])!!}
                    <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
                {!!Html::decode(Form::label('pais_nac', 'Pais: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::select('id_paises',[''=>'Seleccione...']+$paises, null,['required'=>'required', 'id'=>'pais', 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="item form-group">
                {!!Html::decode(Form::label('departamento_nac', 'Departamento: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">                
                    {!!Form::select('id_departamentos',[''=>'Seleccione...']+$departamentos, null,['required'=>'required','id'=>'departamento', 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
                {!!Html::decode(Form::label('ciudad_nac', 'Ciudad: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group"> 
                    {!!Form::select('id_ciudades',[''=>'Seleccione...']+$ciudades, null,['required'=>'required','id'=>'ciudad', 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>

        <br/>
        <h2 align='center'>Recidencia Actual</h2>
        <br/>

        <div class="" style="background: #F7F7F9">
            <div class="item form-group">
                {!!Html::decode(Form::label('dir', 'Dirección: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::text('dir', null,['required'=>'required' , 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
                {!!Html::decode(Form::label('pais_hab', 'Pais: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::select('paises_hab',[''=>'Seleccione...']+$paises, null,['required'=>'required' ,'id'=>'pais_2', 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="item form-group">
                {!!Html::decode(Form::label('departamento_hab', 'Departamento: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::select('departamentos_hab',[''=>'Seleccione...']+$departamentos, null,['required'=>'required','id'=>'departamento_2', 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
                {!!Html::decode(Form::label('ciudad_hab', 'Ciudad: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::select('ciudades_hab',[''=>'Seleccione...']+$ciudades, null,['required'=>'required','id'=>'ciudad_2', 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="item form-group">
                {!!Html::decode(Form::label('tel', 'Telefono: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::number('tel', null,['required'=>'required' , 'class'=>'form-control'])!!}
                    <div class="help-block with-errors"></div>
                </div>
                {!!Html::decode(Form::label('movil', 'Movil: ', ['class' => 'control-label col-md-2 col-sm-2 col-xs-2']))!!}
                <div class="col-md-4 col-sm-4 col-xs-4 form-group">        
                    {!!Form::number('movil', null,['class'=>'form-control'])!!}
                </div>
            </div>        
        </div>
    </div>

    <div class="tab-pane active" id="cd">
        <table class="table table-striped table-hover table-bordered" >
            <thead>

            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

</div>    