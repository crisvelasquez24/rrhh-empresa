<div class="item form-group">
    {!!Html::decode(Form::label('id_departamentos_rhs', 'Departamento <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::select('id_departamentos_rhs',[''=>'Seleccione un departamento']+$departamentos ,null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('nombre', 'Nombre <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::text('nombre', null,['required'=>'required' , 'class'=>'form-control','placeholder'=>'Ingresa el nombre del cargo'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('funciones', 'Funciones <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::textarea('funciones', null,['required'=>'required' , 'class'=>'form-control','placeholder'=>'Ingresa las funciones del cargo', 'style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('objetivos', 'Objetivos <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::textarea('objetivos', null,['required'=>'required' , 'class'=>'form-control','placeholder'=>'Ingresa los objetivos del cargo', 'style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
