<div class="item form-group">
    {!!Html::decode(Form::label('vacante', 'Vacantes  <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">       
        {!!Form::select('vacante', [''=>'Seleccione...']+$vacantes ,null,['class'=>'form-control','id'=>'vacante'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('a_interes', 'Area de Interes <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::select('a_interes',[''=>'Seleccione...']+$area_interes, null,['id'=>'a_interes','class'=>'form-control'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('postulados', 'Postulados <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::select('postulados[]', [''=>'Seleccione...'] ,null,['required'=>'required' , 'class'=>'select2_multiple form-control','id'=>'postulados','multiple'=>'multiple'])!!}
        <div class="help-block with-errors"></div>
    </div>    
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('fecha', 'Fecha <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::text('fecha', null,['class'=>'form-control col-md-7 col-xs-12 fecha','placeholder'=>'aaaa/mm/dd'])!!}
        <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('hora', 'Hora <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::time('hora', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa la hora de la convocatoria'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('lugar', 'Lugar <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::text('lugar', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa el lugar de la convocatoria'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('citacion', 'Citación <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::textarea('citacion', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa la información de la convocatoria','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>