Estimado usuario:<br/><br/>

Usted ha solicitado el restablecimiento de su contraseña en RRHH. Para ingresar una nueva contraseña haga clic en el siguiente enlace <br/><br/>

<?= url('password/reset/'.$token); ?><br/><br/>

Recuerde que...<br/>

Debe ingresar una contraseña que tenga como minimo 6 caracteres.<br/>

Este enlace funciona para restablecer su contraseña solo una vez.

