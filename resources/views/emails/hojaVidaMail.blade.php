@component('mail::message')
# Hola,{{$obj['p_nombre']}} {{$obj['s_nombre']}} {{$obj['p_apellido']}} {{$obj['s_apellido']}}, Bienvenido

Señor(a): 

En nombre de las UNIDADES TECNOLOGICAS DE SANTANDER, agradecemos su interés de registar Tu hoja de vida.
<br/><br/>


Cordialmente,<br/><br/>
DEPARTAMENTO GESTION HUMANA - UTS<br/><br/>

PBX: 6386000 – 6382828, Exts. 4113 – 4114

{{ config('app.name') }}
@endcomponent
