@extends('layouts.app')
@section('title','Evaluacion del Personal')
@section('content')

{!! Form::open(['route' => 'evaluarR.store', 'method'=>'POST','name'=>'frme', 
    'class'=>'form-horizontal','files'=>true]) !!}

    @include('liquidacion.form.form_liquidacion')

    {!!Form::close()!!}



@section('includesCss')
<link rel="stylesheet" type="text/css" href="{{ asset('css/evaluacion.css') }}"/>
<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.css">
@endsection
@section('includesScripts')
<script src = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"> </script>

@endsection



@endsection