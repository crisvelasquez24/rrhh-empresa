@extends('layouts.app')
@section('titleForm','Administrar Nomina')
@section('titleTab')
@include('nomina.forms.form_modal_nomina')
    <div class="title_right">
        <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                {{-- <a class="form-control" placeholder="Search for...">Crear Perfiles</a> --}}
                <span class="input-group-btn">

                </span>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- page content -->
    <div class="center_col" role="main">
      <div class="">

        <div class="clearfix"></div>

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_content">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                            <thead>
                                <tr>
                                    <th>
                                    <th><input type="checkbox" id="check-all"
                                        class="flat">
                                    </th>
                                    </th>
                                    <th>Nombre</th>
                                    <th>apellidos</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($empleados as $data)
                                <tr>
                                        <td>
                                        <th>
                                            <input type="checkbox" value="{{ $data->id_hoja }}" name="empleado"
                                            class="flat">
                                        </th>
                                        </td>
                                        <td>{{ $data->p_nombre }}</td>
                                        <td>{{ $data->p_apellido }}</td>
                                        <td>
                                        {!!Html::decode(
                                            link_to_route(
                                                'NominaShow',
                                                '<i class="fa fa-eye" title="Ver Nomina"></i>',
                                                [$data->id_hoja],
                                                ['class'=>'']
                                        ))!!}&nbsp;

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if(2 == auth()->user()->id_perfil || 3 == auth()->user()->id_perfil)
                            <button type="button" id="btnGenerar" class="btn btn-primary">
                                    Generar Nomina
                            </button>
                        @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('includesCss')
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/msgbox/jquery.msgbox.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <script src="{{asset('vendors/color-box/jquery.colorbox.js') }}"></script>
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/msgbox/jquery.msgbox.js')}}"></script>
    <script src="{{asset('build/js/datatables.js')}}"></script>
    <script src="{{asset('build/js/funtionsConfirm.js')}}"></script>
    <script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>
    <link href="{{asset('vendors/color-box/colorbox.css') }}" rel="stylesheet">
    <script type="text/javascript">
    $(document).ready(function() {
        var empleado = [];
        $("#btnGenerar").click(function() {
            $.each($("input[name='empleado']:checked"), function(){
                empleado.push($(this).val());
            });

            if (0 == empleado.length) {
                $.msgbox('<p style="color:#FF0000";>Seleciones empleados Imposible Continuar</p>', {
                    type: "confirm",
                    buttons: [
                    {type: "cancel", value: "Aceptar"}
                    ]
                },function (result) {

                });
                return;
            } else {
                $('#myModal').modal('toggle');
            }
        });

        $("#botonContinuar").click(function() {
            var route = "nomina/";
            var token = $("#token").val();

            if ($("#periodo").val()) {
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        empleado: empleado,
                        periodo: $("#periodo").val()
                    },

                    success:function(response){
                        $.msgbox('<p style="color:#FF0000";>Nomina Creada Correctamente</p>', {
                            type: "confirm",
                            buttons: [
                            {type: "cancel", value: "Aceptar"}
                            ]
                        },function (result) {
                            $('#myModal').modal('hide');
                        });
                    },
                    error:function(msj){
                        alert("Algo Salio mal");
                    }
                });
            }
        });
    });

    </script>
@endsection