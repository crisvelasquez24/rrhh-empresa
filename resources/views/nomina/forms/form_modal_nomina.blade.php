<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-laptop modal-icon"></i>
                <h4 class="modal-title">Tu Nomina</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body" style="">
                <div class="form-group"><label>Selecione Periodo</label>
                    {!!Form::select('periodo', [''=>'Seleccione...']+$PeriodosNomina,null,
                    ['required'=>'required', 'id'=>'periodo' ,'class'=>'form-control col-md-7 col-xs-12'])!!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" id="botonContinuar" class="btn btn-primary">Continuar</button>
            </div>
        </div>
    </div>
</div>