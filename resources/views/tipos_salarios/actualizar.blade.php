@extends('layouts.app')
@section('title','Actualizar Tipo Contrato')
@section('content')
{!! Form::model($tiposSalarios,['route' => ['tipoSalario.update',$tiposSalarios->id], 
        'id'=>'frm','name'=>'frm',
    'method'=>'PUT', 'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('tipos_salarios.forms.form_tiposSalarios')
        <div class="form-group" align='center'>
           {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}
            <button type="button"  onclick="window.parent.location.href = '/tipoSalario'" 
                class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection