<div class="x_content">
    <div class="item form-group">
        {!!Html::decode(Form::label('descripcion', 'Descripcion <span class="required obligatorio">*</span>',
            ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!!Form::text('descripcion', null,['placeholder'=>'Ingrese Descripción','required'=>'required' , 'class'=>'form-control'])!!}
        </div>
    </div>
</div>
<div class="x_content">
    <div class="item form-group">
        {!!Html::decode(Form::label('caracteristicas', 'Caracteristicas <span class="required obligatorio">*</span>',
            ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!!Form::textarea('caracteristicas', null,['maxlength'=>'2000','required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12',
                                        'placeholder'=>'Ingrese Caracteristicas del contrato','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
        </div>
    </div>
</div>

