@extends('layouts.app')

@section('title','Listado de altas y bajas')

@section('content')

	<div class="row">
	@foreach($tipos_altas_bajas as $tipos_altas_bajas)

		<div class="col-sm">
		<div class="card text-center" style="width: 18rem; margin-top: 70px">
			
  			<div class="card-body">
    			<h5 class="card-title">{{ $tipos_altas_bajas->id}}</h5>
    			<h5 class="card-title">{{ $tipos_altas_bajas->nombre}}</h5>
    			<h5 class="card-title">{{ $tipos_altas_bajas->tipo}}</h5>
    			<h5 class="card-title">{{ $tipos_altas_bajas->valor}}</h5>
    				
  			</div>
		</div>
	</div>
	@endforeach
	</div>

@endsection
