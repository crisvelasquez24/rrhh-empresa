@extends('layouts.app')
@section('title','Actualizar Altas y Bajas')
@section('content')

{!! Form::model($tipos,['route' => ['tipos_altas_bajas.update',$tipos->id], 'id'=>'frm','name'=>'frm',
    'method'=>'PUT', 'class'=>'form-horizontal form-label-left','novalidate']) !!}    
        @include('tipos_altas_bajas.forms.form_tipos_altas_bajas')
        <div class="form-group" align='center'>
           {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}     
            <button type="button"  onclick="window.parent.location.href = '/tipos_altas_bajas'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
    
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection