@extends('layouts.app')
@section('title','Registrar Altas y Bajas')
@section('content')
    {!! Form::open(['route' => 'tipos_altas_bajas.store', 'method'=>'POST','id'=>'demo-form2','name'=>'frm', 
    'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('tipos_altas_bajas.forms.form_tipos_altas_bajas')
    <div class="form-group" align='center'>
            {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}     
            <button type="button"  onclick="window.parent.location.href = '/tipos_altas_bajas'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
       
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection