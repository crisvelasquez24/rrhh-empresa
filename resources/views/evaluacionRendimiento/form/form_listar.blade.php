    
 

    <!-- page content -->
    <div class="center_col" role="main">
      <div class="">

        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_content">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            
                            <div id="myTabContent2" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content33" aria-labelledby="home-tab">
                                    <div class="card-box table-responsive">
                                        <table id="datatable-keytable" class="table table-stripe table-bordered  ">
                                            <thead>
                                                <tr>
                                                    <th>Id eva</th>
                                                    <th>Nombre</th>
                                                    <th>Radar</th>
                                                </tr>
                                            </thead>

                                            <tbody id="canvasRadar2">
                                                 @foreach($eval as $data)
                                                <tr>
                                                    <div class="row">
                                                        <td>{{$data->id}}</td> 
                                                        <td>{{$data->p_nombre." ".$data->p_apellido." ".$data->s_apellido}}</td> 
                                                        <td>
                                                            <div  class="col-md-6 col-sm-6 col-xs-12"> 
                                                                <div class="x_panel" >
                                                                    <div class="x_title">
                                                                        <h2>Radar <small>Sessions</small></h2>
                                                                    <div class="clearfix"></div>
                                                                    </div>
                                                                <div class="x_content" >  
                                                                    <canvas class="canvasRadar"></canvas>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </div>
                                                </tr>
                                                @endforeach        
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                                     
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


