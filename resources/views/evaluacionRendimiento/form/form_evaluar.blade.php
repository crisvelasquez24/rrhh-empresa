


<div class="row">
    <div class="col-xs-10 col-md-10 col-sm-10 col-xs-offset-1 col-md-offset-1">
        <div class="x_panel">
            <div class="x_content"><br>
            
                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval=0 >
                  <!-- Indicators -->
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner " id="preg" align="center">
                    <div class="item active" >
                            
                        <div class="x_title col-md-10 col-sm-12 col-xs-12 col-sm-offset col-md-offset-1">
                            <div id="idtitle" class="">
                                <h2 id="titulocontenido">Datos Trabajador</h2><br>
                                

                            </div>
                        </div><br>
                        <section id="section2">
                            
                        <div id="divselect" class="col-md-6 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-2 col-xs-offset-1"  align="center">
                        

                            <h3 >Lista de Trabajadores </h3>
                            <div class="item ">
                                <div class=" col-xl-10 col-md-12 col-sm-12 col-xs-12">
                                    <span class="select-box">
                                    {!!Form::select('id_hojadevida',
                                    [''=>'--Seleccione un trabajador--']+$hojasdevida, null,['required'=>'required', 
                                    'id'=>'hojavida', 'class'=>'selectpicker form-control ', 'data-style'=>'btn-info', 'data-live-search'=>'true'])!!}
                                    </span>
                                </div>

                            </div><br><br><br><br><br><br><br><br>
                            <div align="right" id="btnright">
                             <button  type="button"  class="btn btn-primary" id="botonSiguiente" 
                                href="#myCarousel" data-slide="next">Siguiente</button>
                            </div>
                            
                        </div>
                        </section>

                    </div>

                    @foreach($preguntas as $key => $data)
                    <div class="item ">
                        <div class="x_title col-md-10 col-sm-12 col-xs-12  col-md-offset-1">
                            <div class="col-md-11 col-sm-10 col-xs-10 col-sm-offset-1 " id="idbarra" align="right">
                                <p style="margin-right: 70px">Total Completado  {{(($key + 1)/$totalPreguntas)*100}}%</p>
                            <progress align="left" value="{{(($key + 1) / $totalPreguntas)*100}}" max="100" ></progress>  
                            </div>
                        </div>

                        <input id="idPregunta" value="{{$data->id}}" type="hidden">

                        <div class="x_content ">
                            <section id="section">
                                <h3 id="titulocontenido">{{$data->p1}}</h3><br>

                                <label id="labelradio" class="btn btn-primary"><input value="1" name="r1" id="radio" type="radio"  value="1"><h3 id="h3radio" class="checkbox-inline">Nunca</h3></label>
                                <label id="labelradio" class="btn btn-primary "><input value="2" name="r1" id="radio" type="radio"  value="2"><h3 id="h3radio" class="checkbox-inline"> Rara vez</h3></label>
                                <label id="labelradio" class="btn btn-primary "><input value="3" name="r1" id="radio" type="radio"  value="3"><h3 id="h3radio" class="checkbox-inline"> A veces</h3></label>
                                <label id="labelradio" class="btn btn-primary "><input value="4" name="r1" id="radio" type="radio"  value="4"><h3 id="h3radio" class="checkbox-inline"> A menudo</h3></label>
                                <label id="labelradio" class="btn btn-primary "><input value="5" name="r1" id="radio" type="radio"  value="5"><h3 id="h3radio" class="checkbox-inline"> Siempre</h3></label>
                            </section>
                            <section id="section">

                                <h3 id="titulocontenido">{{$data->p2}}</h3><br>

                                <label id="labelradio" class="btn btn-primary not-active" ><input  name="r2" id="radio" type="radio"  value="1"><h3 id="h3radio" class="checkbox-inline" >Nunca</h3></label>
                                <label id="labelradio" class="btn btn-primary not-active"><input  name="r2" id="radio" type="radio"  value="2"><h3 id="h3radio" class="checkbox-inline"> Rara vez</h3></label>
                                <label id="labelradio" class="btn btn-primary not-active"><input  name="r2" id="radio" type="radio"  value="3"><h3 id="h3radio" class="checkbox-inline"> A veces</h3></label>
                                <label id="labelradio" class="btn btn-primary not-active"><input  name="r2" id="radio" type="radio"  value="4"><h3 id="h3radio" class="checkbox-inline"> A menudo</h3></label>
                                <label id="labelradio" class="btn btn-primary not-active"><input  name="r2" id="radio" type="radio"  value="5"><h3 id="h3radio" class="checkbox-inline"> Siempre</h3></label>
                            </section>
                        </div>
                        <div align="left" id="btnleft">
                            <button  type="button"  class="btn btn-primary" id="botonAnterior" 
                                href="#myCarousel" data-slide="prev">Anterior</button>
                        </div>
                        <div align="right" id="btnright">
                            <button  type="button"  class="btn btn-primary" id="botonSigu" 
                                href="#myCarousel" data-slide="next">Siguiente</button>
                        </div>
                    </div>

                    @endforeach

                    

                    


                  </div><!-- Fin carousel-inner -->
                    
                    
                  <!-- Left and right controls -->
                    
                </div><!-- Fin mycarousel -->
                


</div>
    </div>
        </div>
            </div>  