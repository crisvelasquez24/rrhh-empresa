@extends('layouts.app')
@section('title','Evaluacion del Personal')
@section('content')

{!! Form::open(['route' => 'evaluarR.store', 'method'=>'POST','name'=>'frme', 
    'class'=>'form-horizontal','files'=>true]) !!}

    @include('evaluacionRendimiento.form.form_evaluar')

    {!!Form::close()!!}



@section('includesCss')
<link rel="stylesheet" type="text/css" href="{{ asset('css/evaluacion.css') }}"/>
<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.css">
@endsection
@section('includesScripts')
<script src = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"> </script>
<script type="text/javascript">
	$(document).ready(function(){
        $( "#botonSiguiente" ).click(function() {
            if (!$("#hojavida").val()) {
                $("#botonSiguiente").removeAttr("href");
            } else {
                $("#botonSiguiente").attr("href", "#myCarousel");
            }
        });

        $( "#botonAnterior" ).click(function() {
            if (!$("#hojavida").val()) {
                $("#botonAnterior").removeAttr("href");
            } else {
                $("#botonAnterior").attr("href", "#myCarousel");
            }
        });

        $( "#botonSigu" ).click(function() {
            if (!$("#hojavida").val()) {
                $("#botonSigu").removeAttr("href");
            } else {
                $("#botonSigu").attr("href", "#myCarousel");
            }
        });        
     
		$('input:radio').change(function(){
            var hoja = $("#hojavida").val();
            var pre = $("#idPregunta").val();
			var valor= event.target.value;


    	    $.get("/respuesta/" + event.target.value + "/"+ $("#hojavida").val() +"/"+ $("#idPregunta").val() +"/" , function (response, torres) {
                

            });


    	});          

	});
</script>

@endsection


@endsection
