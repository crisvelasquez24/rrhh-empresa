@extends('layouts.app')
@section('title','Evaluacion del Personal')
@section('content')

{!! Form::open(['route' => 'evaluarR.store', 'method'=>'POST','name'=>'frme',
    'class'=>'form-horizontal','files'=>true]) !!}

    @include('evaluacionRendimiento.form.form_listar')

    {!!Form::close()!!}


@section('includesCss')
<link rel="stylesheet" type="text/css" href="{{ asset('css/listevaluacion.css') }}"/>

    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/msgbox/jquery.msgbox.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/msgbox/jquery.msgbox.js')}}"></script>
    <script src="{{asset('build/js/datatables.js')}}"></script>
    <script src="{{asset('build/js/funtionsConfirm.js')}}"></script>

    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>

    <script src="{{ asset('css/listevaluacion.css') }}"></script>

    <script type="text/javascript">

    	$(document).ready(function(){


    	// 	var canRadar = $("#canvasRadar2");
			  // var route = "http://localhost:8000/evaluarRen";
			  // $.get(route, function(res){
			  // 		$(res).each(function(key,value){
			  // 			canRadar.append('<tr><div class="row"><td>'+value.id+'</td> <td>'+value.p_nombre+'</td> <td><div  class="col-md-6 col-sm-6 col-xs-12"> <div class="x_panel" ><div class="x_title"><h2>Radar <small>Sessions</small></h2><div class="clearfix"></div></div><div class="x_content" >  <canvas class="canvasRadar"></canvas></div></div></div></td></div></tr>')
			  // 		});

			  // });

    		 // if ($('.canvasRadar').length ){

			  // for (var i = ctx.length - 1; i >= 0; i--) {


			  	<?php for($i=count($eval)-1;$i>=0;$i--){ ?>

			  	var ctx = document.getElementsByClassName("canvasRadar");



			  	var r1=<?php echo $eval[$i]->rta?>;
			  	var r2=<?php echo $eval[$i]->rta?>;
			  	var r3=<?php echo $eval[$i]->rta?>;
			  	var r4=<?php echo $eval[$i]->rta?>;




			  	//alert("si entra "+p1);


			  var data = {
				labels: ["Puntualidad", "Responsabilidad", "Oportunismo", "Cal Trabajo", "Confiabilidad", "social", "Comp Laborales"],
				datasets: [{
				  label: "Rendimiento",
				  backgroundColor: "rgba(3, 88, 106, 0.2)",
				  borderColor: "rgba(3, 88, 106, 0.80)",
				  pointBorderColor: "rgba(3, 88, 106, 0.80)",
				  pointBackgroundColor: "rgba(3, 88, 106, 0.80)",
				  pointHoverBackgroundColor: "#fff",
				  pointHoverBorderColor: "rgba(220,220,220,1)",
				  data: [r1, r2, r3, r4]
				}]
			  };

			  var options = {
				    scale: {
				        ticks: {
				            beginAtZero: true,
				            max: 5
				        }
				    }
				};

			  var canvasRadar = new Chart(ctx[<?php echo $i; ?>], {
				type: 'radar',
				data: data,
			    options: options
			  });


 				<?php } ?>

			//}//fin for
			 //}// fin if


    	});
    </script>


@endsection



@endsection
