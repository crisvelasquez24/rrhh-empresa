@extends('layouts.app')
@section('title','Registrar Área Interés')
@section('content')
{!! Form::model($area_interes,['route' => ['area_interes.update',$area_interes->id], 
        'id'=>'frm','name'=>'frm',
    'method'=>'PUT', 'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('area_interes.forms.form_area_interes')
        <div class="form-group" align='center'>
           {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}
            <button type="button"  onclick="window.parent.location.href = '/area_interes'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection
