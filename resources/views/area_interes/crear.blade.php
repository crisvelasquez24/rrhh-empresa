@extends('layouts.app')
@section('titleForm','Crear Área de Interés')
@section('titleTab')
@endsection
@section('content')
    {!! Form::open(['route' => 'area_interes.store', 'method'=>'POST',
            'id'=>'demo-form2','name'=>'frm',
    'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('area_interes.forms.form_area_interes')
        <div class="form-group" align='center'>
            {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}
            <button type="button"  onclick="window.parent.location.href = '/area_interes'" 
                class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection
{{-- @extends('layouts.form')
@section('titulo','Ofertas | Área de Interés')
@section('namepage') <h2>Nueva Área de Interés <small></small></h2> @stop
@section('contenido')
{!! Form::open(['route' => 'area_interes.store', 'method'=>'POST', 'id'=>'frm','name'=>'frm','class'=>'form-horizontal']) !!}
@include('forms.form_area_interes')
<div class="form-group" align='center'>
    {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}     
    <button type="button"  onclick="window.parent.location.href = '/area_interes'" class="btn btn-sm btn-warning">Cancelar</button>
</div>
{!! Form::close() !!}
@stop

 --}}