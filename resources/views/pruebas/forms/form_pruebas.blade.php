<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto">Foto
        <span class="required obligatorio">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php if (isset($hojadevida)) { ?>
            {!!Html::image('fotos/'.$hojadevida->foto,null,['class'=>'img-thumbnail profile_img', 'style'=>'width:150px'])!!}
        <?php } ?>
        {!!Form::file('documentos',['required'=>'required','class'=>''])!!}
    </div>
</div>
