<div class="item form-group">
     {!!Html::decode(Form::label('nombre', 'Nombre <span>*</span>', 
     ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::text('nombre', null,['required'=>'required' , 
        'class'=>'form-control col-md-7 col-xs-12',
        'placeholder'=>'Ingresa el nombre de la vacante'])!!}
    </div>
</div>

<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_nac">Vigencia:
        <span class="required obligatorio">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::text('vigencia', null,['required'=>'required' , 
        'class'=>'form-control col-md-7 col-xs-12 single_cal2',
        'placeholder'=>'Ingresa Vigencia de la vacante'])!!}  
        <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
    </div>
</div>


<div class="item form-group">
    {!!Html::decode(Form::label('descripcion', 'Descripción <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::textarea('descripcion', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa la descripción de la vacante', 'style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('localizacion', 'Localizacion <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::text('localizacion', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa la localizacion de la vacante'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('requerimientos', 'Requerimientos <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::textarea('requerimientos', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa los requerimientos de la vacante','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('salario', 'Salario <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::text('salario', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa el salario de la vacante'])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>




