@extends('layouts.app')
@section('title','Registrar Vacantes')
@section('content')
    {!! Form::open(['route' => 'vacantes.store', 'method'=>'POST','id'=>'demo-form2',
        'name'=>'frm',
    'class'=>'form-horizontal','files'=>true,'novalidate']) !!}
        @include('vacantes.forms.form_vacantes')
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}
            <button type="button"  onclick="window.parent.location.href = '/vacantes'" 
            class="btn btn-sm btn-warning">Cancelar</button>
            </div>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesCss')
<link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

@endsection
@section('includesScripts')
<script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection
