@extends('layouts.app')
@section('titleForm','Registrar Usuarios')
@section('content')
    {!! Form::model($userss,['route' => ['users.update',$userss->id],'id'=>'frm','name'=>'frm',
         'method'=>'PUT', 'class'=>'form-horizontal']) !!}
        @include('users.forms.form_users')
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
               {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}
                <button type="button"  onclick="window.parent.location.href = '/users'"
                    class="btn btn-sm btn-warning">Cancelar</button>
            </div>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
    <script type="text/javascript">
     
    </script>
@endsection