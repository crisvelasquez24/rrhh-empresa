<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Recurso Humano | </title>

    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/build/css/custom.min.css" rel="stylesheet">

    <link href="/css/styleControl.css" rel="stylesheet">

  </head>

  <body class="login fondo">
    <div class="">
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper  ">
        <div class="animate form login_form ">
          <section class="" align="center">
            <form>
              <div class="cajaletra">
              <h1 class=" letra1" >CONTROL LLEGADA</h1>
              </div>
              <h3>Ingresa tu código de Barras</h3><br>
                <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('1050920050', 'S25')}}"
                width="350px;" height="75px;" alt="barcode" />
                <br/>
                <br/>
                <br/>
              <div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                <textarea
                    class="resizable_textarea form-control"
                    autofocus
                    style="height: 100px; "
                    onKeyPress=""
                    id="textControl"
                >

                </textarea>
              </div>
              <br>
              <div class="clearfix"></div>
              <div class="separator">

                <br>
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1 class="letra2"><i class="fa fa-paw"></i> Bienvenido!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
  <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $( "#textControl" ).on( "keyup", function( event ) {
            if (13 == event.which) {
                var route = "control/";
                var token = $("#token").val();
                var valor = $("#textControl").val();
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    dataType: 'json',
                    data:{id_user: valor},

                    success:function(response){
                        if (response != 1) {
                            alert(response);
                        } else {
                            alert("Ingreso realizado con existo");
                        }
                        $("#textControl").val('');
                    },
                    error:function(msj){
                    // $("#msj").html(msj.responseJSON.genre);
                    // $("#msj-error").fadeIn();
                    }
                });
            }
        });
    });
    </script>
</html>
