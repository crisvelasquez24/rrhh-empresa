@extends('layouts.form')
@section('titulo','Convocatorias | Administrar')
@section('nombrepagina','Nueva convocatoria')
@section('contenido')
{!! Form::open(['route' => 'convocatorias.store', 'method'=>'POST','id'=>'frm','name'=>'frm', 'class'=>'form-horizontal']) !!}
@include('forms.form_convocatorias')
<div class="form-group" align='center'>
    {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}     
    <button type="button"  onclick="window.parent.location.href = '/convocatorias'" class="btn btn-sm btn-warning">Cancelar</button>
</div>
{!!Form::close()!!}
<script>
    $(document).ready(function () {
        $(".select2_multiple").select2({
            placeholder: "Agregue producto...",
            allowClear: true
        });
    });
</script>
@stop