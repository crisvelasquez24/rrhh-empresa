@extends('layouts.base')
@section('titulo','Convocatorias | Administrar')
@section('namepage','<h2>Convocatorias</h2>')
@section('contenido')
<div>
    {!!Html::decode(link_to_route('convocatorias.create', '<button class="btn btn-sm btn-warning"><i class="fa fa-plus-square"></i> Agregar</button>', [], ['class'=>'iframe']))!!}
</div>
<br/>
<table class="table table-striped table-hover table-bordered dt-responsive" id="order_convocatoria">
    <thead>
        <tr>
            <th>Vacante</th>
            <th>Área de Interés</th>
            <th>Citación</th>
            <th>Fecha Creación</th>
            <th class="none">Postulados:</th>
            <th class="none">Fecha:</th>
            <th class="none">Hora:</th>
            <th class="none">Lugar:</th>
        </tr>
    </thead>
    <tbody>
        @foreach($convocatorias as $data)
        <tr>
            <td title="Ver más Información">{{$data->vacante}}</td>
            <td>{{$data->a_interes}}</td>
            <td>{{$data->citacion}}</td>
            <td>{{substr($data->created_at,0,16)}}</td>
            <td>{{$data->convocados($data->id)}}</td>
            <td>{{$data->fecha}}</td>
            <td>{{$data->hora}}</td>
            <td>{{$data->lugar}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop