@extends('layouts.app')
@section('titleForm','Administrar Suscritos')
@section('content')
<br/>
<ul class="nav nav-tabs">
    <li class="active"><a href="#emple" data-toggle="tab">Empelados <i class="fa"></i></a></li>
    <li><a href="#aspirantes" data-toggle="tab">Aspirantes <i class="fa"></i></a></li>
    <li><a href="#inactivos" data-toggle="tab">Inactivos <i class="fa"></i></a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="emple">
        <table class="table table-striped table-hover table-bordered" id="datatable-responsive">
            <thead>
                <tr>
                    <th>Documento</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Área de Interés</th>
                    <th>Ciudad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($empleado as $data)
                <tr>
                    <td>{{$data->tipo_doc}} {{$data->doc}}</td>
                    <td>{{$data->p_nombre}} {{$data->s_nombre}}</td>
                    <td>{{$data->p_apellido}} {{$data->s_apellido}}</td>
                    <td>{{$data->nom_area_interes}}</td>
                    <td>{{$data->nom_ciudad}}</td>
                    <td>
                        {!!Html::decode(link_to_route('hoja_de_vida.show', '<i class="fa fa-eye-slash" title="Ver hoja de vida"></i>', [$data->id], ['class'=>'iframe_hojadevida']))!!}&nbsp;
                        {!!Html::decode(
                            link_to_route(
                                'carnets.show',
                                '<i class="fa fa-credit-card" title="Carnetizar"></i>',
                                 [$data->id_user],
                                 ['class'=>'iframe_hojadevida']
                                 )
                                 )!!}&nbsp;
                     {{--    {!!Html::decode(link_to_route('evaluar.show', '<i class="fa fa-check-square-o" title="Evaluar"></i>', [$data->id], ['class'=>'iframe']))!!}&nbsp;
                        {!!Html::decode(link_to_route('evaluar.edit', '<i class="fa fa-folder-open-o" title="Resultados"></i>', [$data->id], ['class'=>'iframe']))!!}&nbsp;
                        {!!Html::decode(link_to_route('suspostulaciones', '<i class="fa fa-newspaper-o" title="Sus postulaciones"></i>', [$data->id], ['class'=>'iframe']))!!}&nbsp;
                        {!!Html::decode(link_to_route('susconvocatorias', '<i class="fa fa-bell-o" title="Sus convocatorias"></i>', [$data->id], ['class'=>'iframe']))!!}&nbsp;                --}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <div class="tab-pane" id="aspirantes">
        <table class="table table-striped table-hover table-bordered" id="datatable-responsive">
            <thead>
                <tr>
                    <th>Documento</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Área de Interés</th>
                    <th>Ciudad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($aspirantes as $data)
                <tr>
                    <td>{{$data->tipo_doc}} {{$data->doc}}</td>
                    <td>{{$data->first_name}} {{$data->s_nombre}}</td>
                    <td>{{$data->last_name}} {{$data->s_apellido}}</td>
                    <td>{{$data->nom_area_interes}}</td>
                    <td>{{$data->nom_ciudad}}</td>
                    <td>
                        {!!Html::decode(link_to_route('hoja_de_vida.show', '<i class="fa fa-eye-slash" title="Ver hoja de vida"></i>', [$data->id], ['class'=>'iframe_hojadevida']))!!}&nbsp;

                     {{--    {!!Html::decode(link_to_route('evaluar.show', '<i class="fa fa-check-square-o" title="Evaluar"></i>', [$data->id], ['class'=>'iframe']))!!}&nbsp;
                        {!!Html::decode(link_to_route('evaluar.edit', '<i class="fa fa-folder-open-o" title="Resultados"></i>', [$data->id], ['class'=>'iframe']))!!}&nbsp;
                        {!!Html::decode(link_to_route('suspostulaciones', '<i class="fa fa-newspaper-o" title="Sus postulaciones"></i>', [$data->id], ['class'=>'iframe']))!!}&nbsp;
                        {!!Html::decode(link_to_route('susconvocatorias', '<i class="fa fa-bell-o" title="Sus convocatorias"></i>', [$data->id], ['class'=>'iframe']))!!}&nbsp;                --}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="tab-pane" id="inactivos">

        <table class="table table-striped table-hover table-bordered" id="datatable-responsive">
            <thead>
                <tr>
                    <th>Documento</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Área de Interés</th>
                    <th>Ciudad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($aspirantes as $data)
                <tr>
                    <td>{{$data->tipo_doc}} {{$data->doc}}</td>
                    <td>{{$data->p_nombre}} {{$data->s_nombre}}</td>
                    <td>{{$data->p_apellido}} {{$data->s_apellido}}</td>
                    <td>{{$data->nom_area_interes}}</td>
                    <td>{{$data->nom_ciudad}}</td>
                    <td>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop