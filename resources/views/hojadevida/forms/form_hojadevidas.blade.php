<div role="container">
    <div class="row">
        {{-- <div class="col-md-2 col-sm-2 col-xs-12"></div> --}}
        <div class="col-xs-10 col- md-10 col-sm-10 col-xs-offset-1 col-col-offset-1">
            <div class="x_panel">
                <div class="x_content">
                    <!-- Ini SmartWizard Content -->
                    <div id="wizard" class="form_wizard wizard_horizontal">
                        <ul class="wizard_steps">
                            <li>
                                <a href="#step-1">
                                <span class="step_no">1</span>
                                <span class="step_descr">
                                Datos Basicos<br />
                                <small>paso 1 descripción</small>
                                </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-2">
                                <span class="step_no">2</span>
                                <span class="step_descr">
                                Información laboral<br />
                                <small>Paso 2 descripción</small>
                                </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-3">
                                <span class="step_no">3</span>
                                <span class="step_descr">
                                Experiencia laboral<br />
                                <small>Paso 3 description</small>
                                </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-4">
                                <span class="step_no">4</span>
                                <span class="step_descr">
                                Educación formal<br />
                                <small>Paso 4 descripción</small>
                                </span>
                                </a>
                            </li>
                        </ul>
                        <div id="step-1">
                            <span style="text-align: center;" class="section">
                            Información Personal
                            </span>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto">Foto
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?php if (isset($hojaVida)) { ?>
                                        {!!Html::image(
                                            'fotos/' . $nombreArchivo,
                                            null,
                                            ['class'=>'img-thumbnail profile_img',
                                        'style'=>'width:150px'])!!}
                                        <input type="hidden" name="foto" value="{{ $hojaVida->foto }}">
                                    <?php } ?>
                                    {!!Form::file('foto_old',['class'=>''])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_area_interes">Area de Interes
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                       {!!Form::select('id_area_interes', [''=>'Seleccione...']+$area_interes,null,
                                        ['required'=>'required','class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="p_nombre" >Primer Nombre
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text('p_nombre', null,
                                    ['placeholder'=>'Ingrese primer nombre','maxlength'=>"20",'required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="s_nombre">Segundo Nombre
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text('s_nombre', null,['placeholder'=>'Ingrese segundo nombre','maxlength'=>"20",'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="p_apellido">Primer apellido
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text('p_apellido', null,['placeholder'=>'Ingrese primer apellido','maxlength'=>"20",'required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="s_apellido">Segundo apellido
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text(
                                        's_apellido',
                                        null,
                                        [
                                            'placeholder'=>'Ingrese segundo apellido',
                                            'maxlength'=>"20",'class'=>'form-control col-md-7 col-xs-12'
                                        ])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo_doc">Tipo de Identificación
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select(
                                        'tipo_doc',
                                        [
                                            ''=>'Seleccione...',
                                            'C.C'=>'Cedula de Ciudadania',
                                            'C.E'=>'Cedula Extranjera',
                                            'T.I'=>'Tarjeta de Identidad'
                                        ] ,null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="doc">Identificación
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::number('doc', null,['placeholder'=>'Ingrese Identificación','maxlength'=>"10",'required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="estado_civil">Estado Civil:
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::select('estado_civil', [''=>'Seleccione...','Casado(a)'=>'Casado(a)','Divorciado(a)'=>'Divorciado(a)','Soltero(a)'=>'Soltero(a)','Union Libre'=>'Union Libre','Viudo(a)'=>'Viudo(a)'], null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="genero">Genero:
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('genero', [''=>'Seleccione...','Masculino'=>'Masculino','Femenino'=>'Femenino','Otro'=>'Otro'], null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email:
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::email('email', null,['placeholder'=>'Ingrese email','maxlength'=>"30",'required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','id'=>'idemail'])!!}
                                </div>
                            </div>
                            <span style="text-align: center;" class="section">
                                Datos de Nacimiento
                            </span>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_nac">Fecha:
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::text('fecha_nac', null,['placeholder'=>'Ingrese Fecha',
                                        'maxlength'=>"20",'required'=>'required' ,
                                        'class'=>'form-control single_cal2 col-md-7 col-xs-12']
                                    )
                                    !!}
                                    <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_pais_nacimiento">
                                    Pais:
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('id_pais_nacimiento',[''=>'Seleccione...'] + $paises, null,
                                    ['required'=>'required', 'id'=>'pais', 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_departamento_nacimiento">
                                    Departamento:
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('id_departamento_nacimiento',[''=>'Seleccione...'] + $departamentos, null,
                                    ['required'=>'required','id'=>'departamento', 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_ciudad_nacimiento">
                                    Ciudad:
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('id_ciudad_nacimiento',[''=>'Seleccione...'] + $ciudades,
                                    null,['required'=>'required','id'=>'ciudad', 'class'=>'form-control'])!!}
                                </div>
                            </div>

                            <span style="text-align: center;" class="section">
                                Residencia Actual
                            </span>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dir">Dirección
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                     {!!Form::text('dir', null,
                                     ['placeholder'=>'Ingrese Dirección','maxlength'=>'30','required'=>'required' ,
                                        'class'=>'form-control'])!!}
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_pais_actual">Pais
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('id_pais_actual',[''=>'Seleccione...']+$paises, null,['required'=>'required' ,
                                    'id'=>'paises_hab', 'class'=>'form-control'])!!}
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_departamento_actual">Departamento
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('id_departamento_actual',[''=>'Seleccione...'] + $departamentos, null,
                                    ['required'=>'required','id'=>'departamentos_hab', 'class'=>'form-control'])!!}
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_ciudad_actual">Ciudad
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('id_ciudad_actual',[''=>'Seleccione...'] + $ciudades, null,
                                    ['required'=>'required','id'=>'ciudades_hab', 'class'=>'form-control'])!!}
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tel">Teléfono
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::number('tel', null,['placeholder'=>'Ingrese teléfono','required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="movil">Celular
                                    <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::number('movil', null,['placeholder'=>'Ingrese Celular','class'=>'form-control'])!!}
                                </div>
                            </div>

                        </div>
                        <div id="step-2">
                            <span style="text-align: center;" class="section">
                                Paso 2 Información Laboral
                            </span>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Profesión u oficio
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('profesion', [''=>'Seleccione...','Asministración de Empresas'=>'Asministración de Empresas',
                                        'Tecnologo en Desarollo de Sistemas Imformaticos'=>'Tecnologo en Desarollo de Sistemas Imformaticos',
                                        'Contador Publico'=>'Contador Publico','Ingeniero en Sistemas'=>'Ingeniero en Sistemas',
                                        'Sin Porfeción'=>'Sin Porfeción'], null,['maxlength'=>'50','required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Perfil Laboral
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::textarea('perfil_laboral', null,['maxlength'=>'2000','required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12',
                                        'placeholder'=>'Ingresa la información del Perfil Porfecional','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Años de experiencia
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                   {!!Form::select('anos_experiencia', [''=>'Seleccione...','sin experiencia'=>'Sin Experiencia',
                                    'menos de un año'=>'menos de un año','1'=>'1 año','2'=>'2 Años','3'=>'3 Años','4'=>'4 Años',
                                    '5'=>'5 Años',
                                    '6'=>'6 Años','7'=>'7 Años','8'=>'8 Años','9'=>'9 Años','10'=>'10 Años',
                                    'mas de 10 años'=>'Mas de 10 Años'],
                                        null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Aspiración salarial (millones)
                                <span class="required obligatorio">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!!Form::select('aspiracion_salarial', [''=>'Seleccione...',
                                    'menos de 1$'=>'Menos de 1$','1$ a 2$'=>'1$ a 2$','2$ a 3$'=>'2$ a 3$','mas de 3$'=>'Mas de 3$'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trabaja_actualmente"> ¿Trabaja actualmente?:
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p>
                                    SI:
                                    <input type="radio" class="flat" name="trabaja_actualmente" id="" value="0"
                                    required/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;
                                    No:
                                    <input id="radio1" type="radio" class="flat" name="trabaja_actualmente" id=""
                                    checked="" value="1" />
                                    </p>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trabaja_actualmente">
                                    Posibilidad de trasladarse (a otra ciudad o país):
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p>
                                    SI:
                                    <input type="radio" class="flat" name="posibilidad_translado" id="" value="0"
                                    required/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;
                                    No:
                                    <input id="radio1" type="radio" class="flat" name="posibilidad_translado" id=""
                                    checked="" value="1" />
                                    </p>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trabaja_actualmente">
                                    Posibilidad de viajar
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p>
                                    SI:
                                    <input type="radio" class="flat" name="posibilidad_viajar" id="" value="0"
                                    required/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;
                                    No:
                                    <input id="radio1" type="radio" class="flat" name="posibilidad_viajar" id=""
                                    checked="" value="1" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="step-3">
                            <span style="text-align: center;" class="section">
                                paso 3 Experiencia laboral
                            </span>
                            <div id="laboral_fields"></div>
                            @if(0 == $modificando)
                                <div class="row">
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa">Empresa
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                             {!!Form::text('empresa[]', null,
                                             ['placeholder'=>'Ingrese empresa','maxlength'=>'25','required'=>'required' , 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono_empresa">Teléfono de la empresa
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                             {!!Form::number('telefono_empresa[]', null,
                                             ['placeholder'=>'Ingrese Teléfono','maxlength'=>'11','class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sector_empresa">Sector de la empresa
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                             {!!Form::select('sector_empresa[]',
                                             [''=>'Seleccione...','financiero'=>'Financiero','telecomunicaciones'=>'Telecomunicaciones','sistemas'=>'Sistemas','agropecuario'=>'Agropecuario'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cargo_empresa">Cargo
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                             {!!Form::text('cargo_empresa[]', null,['placeholder'=>'Ingrese Cargo','maxlength'=>'25','required'=>'required' , 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jefe_empresa">Jefe Inmediato
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!!Form::text('jefe_empresa[]', null,['placeholder'=>'Ingrese jefe Inmediato','maxlength'=>'25','required'=>'required' , 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_ingreso_empresa">Fecha Ingreso
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!!Form::text('fecha_ingreso_empresa[]', null,['placeholder'=>'Ingrese Fecha',
                                                'maxlength'=>"20",'required'=>'required' ,
                                                'class'=>'form-control single_cal2 col-md-7 col-xs-12']
                                            )
                                            !!}
                                            <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_finalizacion_empresa">Fecha Finalización
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                             <input type="text" class="form-control single_cal2" name="fecha_finalizacion_empresa[]"
                                                placeholder="Fecha Finalización" >
                                            <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    {{-- <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tiempo_experiencia">Tiempo Experiencia
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <p id="esperiencia" class="form-control"></p>
                                        </div>
                                    </div> --}}
                                            <input type="hidden" id="esperiencia2"  name="tiempo_experiencia" value="0">
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pais_empresa">Pais
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!!Form::select('pais_empresa[]',[''=>'Seleccione...']+$paises, 114,
                                            ['onchange'=>'getPaisLaboral(this,1)', 'required'=>'required', 'id'=>'paisexperiencia1', 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="departamento_empresa">Departamento
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!!Form::select('departamento_empresa[]',[''=>'Seleccione...']+$departamentos, 18,[
                                                'onchange'=>'getDepartamentoLaboral(this,1)','required'=>'required','id'=>'departamentoexperiencia1', 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ciudad_empresa">Ciudad
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!!Form::select('ciudad_empresa[]',[''=>'Seleccione...']+$ciudades, 138,
                                            ['onchange'=>'getCiudad(1)','required'=>'required','id'=>'ciudadexperiencia1', 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funciones_empresa">Funciones y logros alcanzadosl
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                             {!!Form::textarea('funciones_empresa[]', null,
                                             ['maxlength'=>'2000','required'=>'required' ,
                                             'class'=>'form-control col-md-7 col-xs-12',
                                             'placeholder'=>'Ingrese Logros Alcanzados','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                    </div>
                                    <div class="item form-group">
                                        <div class="col-md-3 col-sm-3 col-xs-12" ></div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <a class="btn btn-success" type="button" onclick="laboral_fields();">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(1 == $modificando)
                                @foreach($experienciasLaborales as $key => $data)
                                <input type="hidden" name="id_experiencias[]" value="{{ $data->id }}">
                                <br/>
                                    <div class="row">
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa">Empresa
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                 {!!Form::text('empresa[]', $data->empresa,
                                                 ['placeholder'=>'Ingrese empresa','maxlength'=>'25','required'=>'required' , 'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono_empresa">Teléfono de la empresa
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                 {!!Form::number('telefono_empresa[]', $data->telefono_empresa,
                                                 ['placeholder'=>'Ingrese Teléfono','maxlength'=>'11','class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sector_empresa">Sector de la empresa
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                 {!!Form::select('sector_empresa[]',
                                                 [''=>'Seleccione...',
                                                 'financiero'=>'Financiero','telecomunicaciones'=>'Telecomunicaciones',
                                                 'sistemas'=>'Sistemas','agropecuario'=>'Agropecuario'], $data->sector_empresa,['required'=>'required' ,
                                                 'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cargo_empresa">Cargo
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                 {!!Form::text('cargo_empresa[]', $data->cargo_empresa,
                                                 ['placeholder'=>'Ingrese Cargo','maxlength'=>'25','required'=>'required' , 'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jefe_empresa">Jefe Inmediato
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {!!Form::text('jefe_empresa[]', $data->jefe_empresa,
                                                ['placeholder'=>'Ingrese jefe Inmediato','maxlength'=>'25','required'=>'required' , 'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_ingreso_empresa">Fecha Ingreso
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {!!Form::text('fecha_ingreso_empresa[]', $data->fecha_ingreso_empresa,['placeholder'=>'Ingrese Fecha',
                                                    'maxlength'=>"20",'required'=>'required' ,
                                                    'class'=>'form-control single_cal2 col-md-7 col-xs-12']
                                                )
                                                !!}
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_finalizacion_empresa">Fecha Finalización
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                 <input type="text" class="form-control single_cal2" value="{{ $data->fecha_finalizacion_empresa }}"
                                                    name="fecha_finalizacion_empresa[]"
                                                    placeholder="Fecha Finalización" >
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tiempo_experiencia">Tiempo Experiencia
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <p id="esperiencia" class="form-control"></p>
                                                <input type="hidden" id="esperiencia2"  name="tiempo_experiencia">
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pais_empresa">Pais
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {!!Form::select('pais_empresa[]',[''=>'Seleccione...'] + $paises, $data->pais_empresa,
                                                ['onchange'=>"getPaisLaboral(this, $key)", 'required'=>'required', 'id'=>"paisexperiencia$key",
                                                'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="departamento_empresa">Departamento
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {!!Form::select('departamento_empresa[]',
                                                    [''=>'Seleccione...']+$departamentos, $data->departamento_empresa,[
                                                    'onchange'=>"getDepartamentoLaboral(this, $key)",'required'=>'required',
                                                    'id'=>"departamentoexperiencia$key", 'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ciudad_empresa">Ciudad
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {!!Form::select('ciudad_empresa[]',[''=>'Seleccione...']+$ciudades, $data->ciudad_empresa,
                                                ['onchange'=>'getCiudad(1)','required'=>'required','id'=>"ciudadexperiencia$key",
                                                'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funciones_empresa">Funciones y logros alcanzadosl
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                 {!!Form::textarea('funciones_empresa[]', $data->funciones_empresa,
                                                 ['maxlength'=>'2000','required'=>'required' ,
                                                 'class'=>'form-control col-md-7 col-xs-12',
                                                 'placeholder'=>'Ingrese Logros Alcanzados','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div id="step-4">
                            <dir id="educacion_fields"></dir>
                            @if(0 == $modificando)
                                <div class="row">
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">
                                            Nivel de estudios
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                             {!!Form::select('nivel_estudios[]', [''=>'Seleccione...',
                                             'prescolas'=>'Basica Primaria',
                                             'basica segundaria'=>'Basica Segundaria',
                                             'tecnico laboral'=>'Tecnico Laboral',
                                             'tecnologia'=>'Tecnología',
                                             'ingenieria'=>'Ingenieria',
                                             'especializacion'=>'Especialización',
                                             'maestria'=>'Maestria',
                                             'doctorado'=>'Doctorado'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Área de estudios
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                             {!!Form::select('area_estudios[]', [''=>'Seleccione...','financiero'=>'Financiero','telecomunicaciones'=>'Telecomunicaciones','sistemas'=>'Sistemas','agropecuario'=>'Agropecuario'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Título
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                             {!!Form::text('titulo_estudios[]', null,['maxlength'=>'45','class'=>'form-control','placeholder'=>'Ingrese Titulo'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Institución en la que estudió
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!!Form::text('institucion_estudios[]', null,['maxlength'=>'50','class'=>'form-control','placeholder'=>'Ingrese Institución'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Pais
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!!Form::select('pais_estudios[]',[''=>'Seleccione...']+$paises, 114,
                                            ['onchange'=>'getPaisEstudio(this,1)','required'=>'required', 'id'=>'paisestudio1', 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Departamento
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!!Form::select('departamento_estudios[]',[''=>'Seleccione...']+$departamentos, 18,[
                                                'onchange'=>'getDepartamentoEstudio(this,1)','required'=>'required',
                                                'id'=>'departamentoestudio1', 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Ciudad
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!!Form::select('ciudad_estudios[]',[''=>'Seleccione...']+$ciudades, 138,['required'=>'required','id'=>'ciudadestudio1', 'class'=>'form-control'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Funciones y logros alcanzadosl
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!!Form::textarea('funciones_estudios[]', null,['maxlength'=>'2000','required'=>'required' ,
                                                'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingrese Logros Alcanzados',
                                                'style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <div class="col-md-3 col-sm-3 col-xs-12" ></div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <a class="btn btn-success" type="button" onclick="educacion_fields();">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(1 == $modificando)
                                @foreach($formaciones as $key => $data)
                                <input type="hidden" name="id_formaciones[]" value="{{ $data->id }}">
                                    <div class="row">
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">
                                                Nivel de estudios
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                 {!!Form::select('nivel_estudios[]', [''=>'Seleccione...',
                                                 'prescolas'=>'Basica Primaria',
                                                 'basica segundaria'=>'Basica Segundaria',
                                                 'tecnico laboral'=>'Tecnico Laboral',
                                                 'tecnologia'=>'Tecnología',
                                                 'ingenieria'=>'Ingenieria',
                                                 'especializacion'=>'Especialización',
                                                 'maestria'=>'Maestria',
                                                 'doctorado'=>'Doctorado'], $data->nivel_estudios,['required'=>'required' , 'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Área de estudios
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                 {!!Form::select('area_estudios[]',
                                                 [''=>'Seleccione...',
                                                 'financiero'=>'Financiero',
                                                 'telecomunicaciones'=>'Telecomunicaciones',
                                                 'sistemas'=>'Sistemas',
                                                 'agropecuario'=>'Agropecuario'], $data->area_estudios,
                                                 ['required'=>'required' , 'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Título
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                 {!!Form::text('titulo_estudios[]', $data->titulo_estudios,
                                                 ['maxlength'=>'45','class'=>'form-control',
                                                 'placeholder'=>'Ingrese Titulo']
                                                 )!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Institución en la que estudió
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {!!Form::text('institucion_estudios[]', $data->institucion_estudios,
                                                ['maxlength'=>'50','class'=>'form-control','placeholder'=>'Ingrese Institución'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Pais
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {!!Form::select('pais_estudios[]',[''=>'Seleccione...'] + $paises, $data->pais_estudios,
                                                ['onchange'=>"getPaisEstudio(this, $key)",'required'=>'required', 'id'=>"paisestudio$key", 'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Departamento
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {!!Form::select('departamento_estudios[]',[''=>'Seleccione...'] + $departamentos, $data->departamento_estudios,
                                                    ['onchange'=>"getDepartamentoEstudio(this, $key)",'required'=>'required',
                                                    'id'=>"departamentoestudio$key", 'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Ciudad
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {!!Form::select('ciudad_estudios[]',[''=>'Seleccione...'] + $ciudades, $data->ciudad_estudios,
                                                ['required'=>'required','id'=>"ciudadestudio$key", 'class'=>'form-control'])!!}
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Funciones y logros alcanzadosl
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {!!Form::textarea('funciones_estudios[]', $data->funciones_estudios,['maxlength'=>'2000','required'=>'required' ,
                                                    'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingrese Logros Alcanzados',
                                                    'style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <input type="hidden" name="modificando" value="{{ $modificando }}">
                        </div>
                        <!-- End SmartWizard Content -->
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-2 col-sm-2 col-xs-12"></div> --}}
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>