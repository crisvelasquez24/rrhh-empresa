@extends('layouts.app')
@section('titleForm','Actualizar Hoja')
@section('content')
    {!! Form::model($hojaVida,['route' => ['hoja_de_vida.update',$hojaVida->id],'id'=>'frm','name'=>'frm',
         'method'=>'PUT','files'=>true ,'class'=>'form-horizontal']) !!}
        @include('hojadevida.forms.form_hojadevidas')
        <div class="ln_solid"></div>
    {!!Form::close()!!}
@endsection
@section('includesCss')
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

@endsection
@section('includesScripts')
<script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js')}}"></script>
<script src="{{asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('vendors/validator/validator.js')}}"></script>
<script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>

    <script type="text/javascript">

        var room = 1;
        var room2 = 1;

        function laboral_fields() {

            room++;
            var objTo = document.getElementById('laboral_fields')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass" + room);
            var rdiv = 'removeclass' + room;
            divtest.innerHTML
                = `
                    <div class="row">
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa">Empresa
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                 {!!Form::text('empresa[]', null,['placeholder'=>'Ingrese empresa','maxlength'=>'25','required'=>'required' ,
                                 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono_empresa">Teléfono de la empresa
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                 {!!Form::text('telefono_empresa[]', null,['placeholder'=>'Ingrese Teléfono','maxlength'=>'11','class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sector_empresa">Sector de la empresa
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                 {!!Form::select('sector_empresa[]', [''=>'Seleccione...',
                                 'financiero'=>'Financiero','telecomunicaciones'=>'Telecomunicaciones',
                                 'sistemas'=>'Sistemas','agropecuario'=>'Agropecuario'], null,
                                 ['required'=>'required' , 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cargo_empresa">Cargo
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                 {!!Form::text('cargo_empresa[]', null,['placeholder'=>'Ingrese cargo','maxlength'=>'25','required'=>'required' ,
                                 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jefe_empresa">Jefe Inmediato
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {!!Form::text('jefe_empresa[]', null,['placeholder'=>'Ingrese jefe Inmediato','maxlength'=>'25','required'=>'required' ,
                                'class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_ingreso_empresa">Fecha Ingreso
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                               <input type="text" class="form-control single_cal2" name="fecha_ingreso_empresa[]" required>
                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_finalizacion_empresa">
                                Fecha Finalización
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control single_cal2" name="fecha_finalizacion_empresa[]"
                                            placeholder="Fecha Finalización" >
                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tiempo_experiencia">Tiempo Experiencia
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p id="esperiencia" class="form-control"></p>
                                <input type="hidden" id="esperiencia2" placeholder = 'Experiencia'  name="tiempo_experiencia[]">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pais_empresa">Pais
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {!!Form::select('pais_empresa[]',[''=>'Seleccione...']+$paises, 114,
                                ['onchange'=>'getPaisLaboral(this,`+room+`)','required'=>'required', 'id'=>'paisexperiencia`+room+`', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="departamento_empresa">Departamento
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {!!Form::select('departamento_empresa[]',[''=>'Seleccione...']+$departamentos, null,
                                ['onchange'=>'getDepartamentoLaboral(this,`+room+`)','required'=>'required','id'=>'departamentoexperiencia`+room+`', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ciudad_empresa">Ciudad
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {!!Form::select('ciudad_empresa[]',[''=>'Seleccione...']+$ciudades, null,
                                ['onchange'=>'getCiudadLaboral(`+room+`)','required'=>'required','id'=>'ciudadexperiencia`+room+`', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funciones_empresa">Funciones y logros alcanzadosl
                            <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                 {!!Form::textarea('funciones_empresa[]', null,['maxlength'=>'2000','required'=>'required' ,
                                 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingrese Logros Alcanzados','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <div class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button class="btn btn-danger" type="button" onclick="remove_laboral_fields(` + room + `);"> <i class="fa fa-minus"></i> </button>
                            </div>
                        </div>

                    </div>
                    <span class="section"></span>
                    `;

            objTo.appendChild(divtest);
            $('.single_cal2').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_2"
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        }

        function remove_laboral_fields(rid) {
            $('.removeclass' + rid).remove();
        }


        function getPaisLaboral(caja, dato){
            alert(dato);
            $.get("/paisanidado/" + caja.value + "", function (response, torres) {
                $("#departamentoexperiencia" + dato).empty();
                if (response.length !== 0) {
                    $("#departamentoexperiencia" + dato).append("<option value=''>" + 'Seleccione...' + "</option>");
                    for (i = 0; response.length > i; i++) {
                        $("#departamentoexperiencia"+dato).append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>");
                    }
                } else {
                    $("#departamentoexperiencia"+dato).append("<option value=''> No hay registros </option>");
                }
            });



        }

        function getDepartamentoLaboral(caja,dato){
            $.get("/departamentoanidado/" + caja.value + "", function (response, torres) {
                $("#ciudadexperiencia"+dato).empty();
                if (response.length !== 0) {
                    $("#ciudadexperiencia"+dato).append("<option value=''>" + 'Seleccione...' + "</option>");
                    for (i = 0; response.length > i; i++) {
                        $("#ciudadexperiencia"+dato).append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>");
                    }
                } else {
                    $("#ciudadexperiencia"+dato).append("<option value=''> No hay registros </option>");
                }
            });
        }


        function getCiudadLaboral(dato){

        }

        var room2 = 1;

        function educacion_fields() {

            room2++;
            var objTo = document.getElementById('educacion_fields')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass" + room2);
            var rdiv = 'removeclass' + room2;
            divtest.innerHTML
                = `
                    <div class="row">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Nivel de estudios
                                    <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                         {!!Form::select('nivel_estudios[]', [''=>'Seleccione...','prescolas'=>'Basica Primaria','basica segundaria'=>'Basica Segundaria','tecnico laboral'=>'Tecnico Laboral','tecnologia'=>'Tecnología','ingenieria'=>'Ingenieria','especializacion'=>'Especialización','maestria'=>'Maestria','doctorado'=>'Doctorado'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Área de estudios
                                    <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                         {!!Form::select('area_estudios[]', [''=>'Seleccione...','financiero'=>'Financiero','telecomunicaciones'=>'Telecomunicaciones','sistemas'=>'Sistemas','agropecuario'=>'Agropecuario'], null,['required'=>'required' , 'class'=>'form-control'])!!}
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Título
                                    <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                         {!!Form::text('titulo_estudios[]', null,['maxlength'=>'45','class'=>'form-control','placeholder'=>'Ingrese Titulo'])!!}
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Institución en la que estudió
                                    <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!!Form::text('institucion_estudios[]', null,['maxlength'=>'45','class'=>'form-control','placeholder'=>'Ingrese Institución'])!!}
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Pais
                                    <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!!Form::select('pais_estudios[]',[''=>'Seleccione...']+$paises, 114,
                                        ['onchange'=>'getPaisEstudio(this,`+room2+`)','required'=>'required', 'id'=>'paisestudio`+room2+`',
                                        'class'=>'form-control'])!!}
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Departamento
                                    <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!!Form::select('departamento_estudios[]',[''=>'Seleccione...']+$departamentos, 18,
                                        ['onchange'=>'getDepartamentoEstudio(this,`+room2+`)','required'=>'required','
                                        id'=>'departamentoestudio`+room2+`', 'class'=>'form-control'])!!}
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Ciudad
                                    <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!!Form::select('ciudad_estudios[]',[''=>'Seleccione...']+$ciudades, 138,['required'=>'required','id'=>'ciudadestudio`+room2+`', 'class'=>'form-control'])!!}
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Funciones y logros alcanzadosl
                                    <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!!Form::textarea('funciones_estudios[]', null,['maxlength'=>'2000','required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingrese Logros Alcanzados','style'=>'width: 100%; resize: horizontal; height: 80px'])!!}
                                    </div>
                                </div>
                                <div class="item form-group">
                            <div class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button class="btn btn-danger" type="button" onclick="remove_educacion_fields(` + room2 + `);"> <i class="fa fa-minus"></i> </button>
                            </div>
                        </div>

                    </div>
                    <span class="section"></span>
                    `;

            objTo.appendChild(divtest);
            /**
             * Se debe llamar de nuevo la librearia de fecha
             */
            $('.single_cal2').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_2"
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        }

        function remove_educacion_fields(rid) {
            $('.removeclass' + rid).remove();
        }



        /**
         * Gets the pais estudio.
         *
         * @param {<object>}  caja The caja
         * @param {opcion}  dato The dato
         */
        function getPaisEstudio(caja, dato){
            $.get("/paisanidado/" + caja.value + "", function (response, torres) {
                $("#departamentoestudio"+dato).empty();
                if (response.length !== 0) {
                    $("#departamentoestudio"+dato).append("<option value=''>" + 'Seleccione...' + "</option>");
                    for (i = 0; response.length > i; i++) {
                        $("#departamentoestudio"+dato).append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>");
                    }
                } else {
                    $("#departamentoestudio"+dato).append("<option value=''> No hay registros </option>");
                }
            });
        }

        function getDepartamentoEstudio(caja,dato){
            $.get("/departamentoanidado/" + caja.value + "", function (response, torres) {
                $("#ciudadestudio"+dato).empty();
                if (response.length !== 0) {
                    $("#ciudadestudio"+dato).append("<option value=''>" + 'Seleccione...' + "</option>");
                    for (i = 0; response.length > i; i++) {
                        $("#ciudadestudio"+dato).append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>");
                    }
                } else {
                    $("#ciudadestudio"+dato).append("<option value=''> No hay registros </option>");
                }
            });
        }


        $(document).ready(function () {
            $('#wizard').smartWizard();


            $('#fecha_finalizacion_empresa').change(function (event) {
                var final = $("#fecha_finalizacion_empresa").val();
                var ingreso = $("#fecha_ingreso_empresa").val();
                if(final){
                    var  fechafinal =  final.split('/');
                    var  fechaingreso =  ingreso.split('/');
                    var fecha1 = new Date(fechaingreso);
                    var fecha2 = new Date(fechafinal);
                    var diasDif = fecha2.getTime() - fecha1.getTime();
                    var dias = Math.round(diasDif/(1000 * 60 * 60 * 24));
                    var valor  = (dias /365);
                    var conDecimal = valor.toFixed(1);
                    arr = conDecimal.split(".");
                    if(arr[0] > 0){ var valorreal =  arr[0] +" " + "Años" + " " +arr[1] +" " +"Meses"; }
                    else{
                        valorreal = arr[1] + " "+"Meses";
                    }
                    $("#esperiencia").empty();
                    $("#esperiencia").append(valorreal);
                    frm.tiempo_experiencia.value = valorreal;
                }

            });


            $("#pais").change(function (event) {
                $.get("/paisanidado/" + event.target.value + "", function (response, torres) {
                    $("#departamento").empty();
                    if (response.length !== 0) {
                        $("#departamento").append("<option value=''>" + 'Seleccione...' + "</option>");
                        for (i = 0; response.length > i; i++) {
                            $("#departamento").append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>");
                        }
                    } else {
                        $("#departamento").append("<option value=''> No hay registros </option>");
                    }
                });
            });

            $("#departamento").change(function (event) {
                $.get("/departamentoanidado/" + event.target.value + "", function (response, torres) {
                    $("#ciudad").empty();
                    if (response.length !== 0) {
                        $("#ciudad").append("<option value=''>" + 'Seleccione...' + "</option>");
                        for (i = 0; response.length > i; i++) {
                            $("#ciudad").append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>");
                        }
                    } else {
                        $("#ciudad").append("<option value=''> No hay registros </option>");
                    }
                });
            });


            $("#paises_hab").change(function (event) {
                $.get("/paisanidado/" + event.target.value + "", function (response, torres) {
                    $("#departamentos_hab").empty();
                    if (response.length !== 0) {
                        $("#departamentos_hab").append("<option value=''>" + 'Seleccione...' + "</option>");
                        for (i = 0; response.length > i; i++) {
                            $("#departamentos_hab").append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>");
                        }
                    } else {
                        $("#departamentos_hab").append("<option value=''> No hay registros </option>");
                    }
                });
            });

            $("#departamentos_hab").change(function (event) {
                $.get("/departamentoanidado/" + event.target.value + "", function (response, torres) {
                    $("#ciudades_hab").empty();
                    if (response.length !== 0) {
                        $("#ciudades_hab").append("<option value=''>" + 'Seleccione...' + "</option>");
                        for (i = 0; response.length > i; i++) {
                            $("#ciudades_hab").append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>");
                        }
                    } else {
                        $("#ciudades_hab").append("<option value=''> No hay registros </option>");
                    }
                });
            });

        });
    </script>

@endsection


