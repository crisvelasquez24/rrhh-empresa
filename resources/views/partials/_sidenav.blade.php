<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{url('/')}}" class="site_title"><i class="fa fa-paw"></i>
            <span>{{config('app.name')}}</span></a>
        </div>
        <div class="clearfix"></div>
        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="/images/img.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Bienvenido,</span>
                <h2>{{auth()->user()->name}}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                @if(1 == auth()->user()->id_perfil)
                <li>
                    <a href="/ver_vacantes"><i class="fa fa-newspaper-o "></i> Vacantes </a>

                </li>
                @endif
                @if(2 == auth()->user()->id_perfil)
                <li>
                    <a href="/configuracion"><i class="fa fa-newspaper-o "></i> Configuración </a>
                    <a href="/postulados"><i class="fa fa-newspaper-o "></i> Postulados </a>
                </li>
                @endif
                @if(2 == auth()->user()->id_perfil)
                    <li>
                        <a>

                            <i class="fa fa-user"></i> Usuarios <span class="fa fa-chevron-down"></span>

                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ url('/perfiles') }}">Perfiles</a>
                            </li>
                            <li>
                                <a href="{{ url('/users') }}">Usuarios</a>
                            </li>
                        </ul>
                    </li>

                @endif
                @if(3 == auth()->user()->id_perfil || 2 == auth()->user()->id_perfil)
                    <li>
                        <a>

                            <i class="fa fa-user"></i> Control de Ingreso <span class="fa fa-chevron-down"></span>

                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ url('/controlListar') }}">Administrar</a>
                            </li>                           
                        </ul>
                    </li>

                @endif
                @if(2 == auth()->user()->id_perfil || 3 == auth()->user()->id_perfil || 4 == auth()->user()->id_perfil)
                    <li>
                        <a>
                            <i class="fa fa-file-text-o"></i> Evaluaciones <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            @if(4 == auth()->user()->id_perfil)
                            <li>
                                {!!link_to_route('evaluarR.create', $title = 'Evaluacion de rendimiento')!!}
                            </li>
                            @endif
                            <li>
                                {!!link_to_route('evaluarR.index', $title = 'Resultados')!!}
                            </li>
                            @if(2 == auth()->user()->id_perfil || 3 == auth()->user()->id_perfil)
                                <li>
                                    {!!link_to_route('preguntare.index', $title = 'Crear preguntas')!!}
                                </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    {{-- <li>
                        <a>
                            <i class="fa fa-clipboard"></i> Evaluación Rendimineto
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                {!!link_to_route('evaluarR.create', $title = 'Realizar Evaluaciones')!!}
                            </li>
                            <li>
                                {!!link_to_route('evaluarR.index', $title = 'Ver Evaluaciones')!!}
                            </li>
                            <li>
                                {!!link_to_route('preguntare.index', $title = 'Preguntas')!!}
                            </li>
                        </ul>
                    </li> --}}
                    @if(3 == auth()->user()->id_perfil || 2 == auth()->user()->id_perfil)
                        <li>
                            <a>
                                <i class="fa fa-file-text-o"></i> Contratación <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li>
                                    <a href="{{ url('/contratos') }}">Administar Contratos</a>
                                </li>
                                <li>
                                    <a href="{{ url('/tipoContrato') }}">Administar Tipos de Contratos</a>
                                </li>
                                <li>
                                    <a href="{{ url('/vacantes') }}">Administrar Vacantes</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(3 == auth()->user()->id_perfil || 2 == auth()->user()->id_perfil)
                        <li>
                            <a>
                                <i class="fa fa-file-text-o"></i> Hoja de Vida <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li>
                                    <a href="{{ url('/listarHojas') }}">Listar</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(4 == auth()->user()->id_perfil || 1 == auth()->user()->id_perfil)
                    <li>
                        <a>
                            <i class="fa fa-folder-open-o"></i> Hoja de Vida
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                {!!
                                    link_to_route(
                                        'hoja_de_vida.index',
                                        $title = 'Administrar',
                                        $parameters = [],
                                        $attributes = []
                                    )
                                !!}
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(2 == auth()->user()->id_perfil || 3 == auth()->user()->id_perfil)
                        <li>
                            <a>
                                <i class="fa fa-file-text-o">
                                </i>Gestionar Nomina
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li>
                                    <a href="{{ url('/nomina') }}">Administar</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(4 == auth()->user()->id_perfil)
                        <li>
                            <a>
                                <i class="fa fa-file-text-o">
                                </i>Listado de Nominas
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li>
                                    <a href="{{ url('/nomina') }}">Listado</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(4 == auth()->user()->id_perfil)
                        <li>
                            <a>
                                <i class="fa fa-file-text-o">
                                </i>Listado de liquidación
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li>
                                    <a href="{{ url('/liquidacion') }}">Listado</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(2 == auth()->user()->id_perfil || 3 == auth()->user()->id_perfil)
                        <li>
                            <a>
                                <i class="fa fa-file-text-o">
                                </i>Liquidación
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li>
                                    <a href="{{ url('/liquidacion') }}">Administar</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(2 == auth()->user()->id_perfil || 3 == auth()->user()->id_perfil)
                    <li>
                        <a>
                            <i class="fa fa-file-text-o"></i> Área Interés <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ url('/area_interes') }}">Administar</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(4 == auth()->user()->id_perfil)
                    <li>
                        <a>
                            <i class="fa fa-file-text-o"></i> Contratos <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ url('/contratos') }}">Administar</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(2 == auth()->user()->id_perfil)
                        <li>
                            <a>
                                <i class="fa fa-th-list"></i> Gestionar Dptos <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li>
                                    <a href="{{ url('/depar') }}">Administar</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(3 == auth()->user()->id_perfil || 2 == auth()->user()->id_perfil)
                        <li>
                            <a>
                                <i class="fa fa-usd"></i> tipos Salarios
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li>
                                    <a href="{{ url('/tipoSalario') }}">Administar</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(2 == auth()->user()->id_perfil)
                        <li>
                            <a>
                                <i class="fa fa-usd"></i> Festivos <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li>
                                    <a href="{{ url('/festivos') }}">Administar</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(3 == auth()->user()->id_perfil || 2 == auth()->user()->id_perfil)
                    <li>
                        <a>
                            <i class="fa fa-gears"></i> Promotoras <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ url('/administradora') }}">Administar</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(4 == auth()->user()->id_perfil)
                    <li>
                        <a>
                            <i class="fa fa-gears"></i> Novedades <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ url("/listarNovedades") }}">Administar</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(3 == auth()->user()->id_perfil || 2 == auth()->user()->id_perfil)
                    <li>
                        <a>
                            <i class="fa fa-list-alt"></i> Novedades <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ url('/tipos_altas_bajas') }}">Administrar Novedades</a>
                            </li>
                            <li>
                                <a href="{{ url('/aplicar_altas_bajas') }}">Registrar Novedades</a>
                            </li>
                            <li>

                            </li>
                        </ul>
                    </li>
                    @endif

                    @if(3 == auth()->user()->id_perfil || 2 == auth()->user()->id_perfil)
                    <li>
                        <a>
                            <i class="fa fa-user-times"></i> vacaciones <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ url('/registrar_vacaciones') }}">Administrar</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(4 == auth()->user()->id_perfil)
                     <li>
                        <a>
                            <i class="fa fa-user-times"></i> vacaciones <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ url('/listar_vacaciones') }}">Listar</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
        @include('partials._sidenav_footer')
    </div>
</div>