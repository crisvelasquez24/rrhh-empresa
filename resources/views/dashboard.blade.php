@extends('layouts.app')
@section('title','Dashboard')
@section('content')
    <style type="text/css">
    img{
        padding-left: 20px;
    }
</style>
    <h4>Información</h4>

    <!-- end of user messages -->
    <ul class="messages">
      <li>
        <img src="images/img.jpg" class="avatar" alt="Avatar">
        <div class="message_date">
          <h3 class="date text-info">24</h3>
          <p class="month">May</p>
        </div>
        <div class="message_wrapper">
          <h4 class="heading">Misión</h4>
          <blockquote class="message">
              Fortalecer la gestión y el desarrollo del personal, velando por mantener un clima
              laboral que fomente el buen desempeño, el compromiso, la responsabilidad,
              la excelencia y la mejora continua.
          </blockquote>
          <br />
          <p class="url">
            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
            <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
          </p>
        </div>
      </li>
      <li>
        <img src="images/img.jpg" class="avatar" alt="Avatar">
        <div class="message_date">
          <h3 class="date text-error">21</h3>
          <p class="month">May</p>
        </div>
        <div class="message_wrapper">
          <h4 class="heading">Visión</h4>
          <blockquote class="message">
               “Ser una Dirección que promueva el desarrollo integral de los funcionarios,
               por medio de su realización personal, profesional y laboral para brindar un
               servicio de excelencia en beneficio de la comunidad universitaria”.
          </blockquote>
          <br />
          <p class="url">
            <span class="fs1" aria-hidden="true" data-icon=""></span>
            <a href="#" data-original-title="">Download</a>
          </p>
        </div>
      </li>
    </ul>
    <!-- end of user messages -->

@endsection
