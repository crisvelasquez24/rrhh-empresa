@extends('layouts.app')
@section('titleForm','Crear Contrato')
@section('titleTab')
@endsection
@section('content')
    {!! Form::open(['route' => 'contratos.store', 'method'=>'POST','id'=>'demo-form2','name'=>'frm',
    'class'=>'form-horizontal form-label-left','novalidate','files'=>true]) !!}
        @include('contratos.forms.form_contratos')
        <div class="form-group" align='center'>
            {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}
            <button type="button"  onclick="window.parent.location.href = '/contratos'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesCss')
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/select/select2.min.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>
    <script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
    <script src="{{asset('vendors/select/select2.full.js')}}"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            cargar_postulados();
            $(".select").select2({
                allowClear: true
            });

            $("#fecha_fin").change(function (event) {
                if ($("#fecha_fin").val() < $("#fecha_inicio").val()) {
                    alert("Fceha Final no puede ser menor ala fecha inical");
                    $("#fecha_fin").val('');
                    $("#fecha_fin").focus();
                }
                
            });
            


            $("#pais").change(function (event) {
                $.get("/paisContrato/" + event.target.value + "", function (response, torres) {
                    $("#departamento").empty();
                    if (response.length !== 0) {
                        $("#departamento").append("<option value=''>" + 'Seleccione...' + "</option>");
                        for (i = 0; response.length > i; i++) {
                            $("#departamento").append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>");
                        }
                    } else {
                        $("#departamento").append("<option value=''> No hay registros </option>");
                    }
                });
            });

            $("#departamento").change(function (event) {
                $.get("/departamentoContrato/" + event.target.value + "", function (response, torres) {
                    $("#ciudad").empty();
                    if (response.length !== 0) {
                        $("#ciudad").append("<option value=''>" + 'Seleccione...' + "</option>");
                        for (i = 0; response.length > i; i++) {
                            $("#ciudad").append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>");
                        }
                    } else {
                        $("#ciudad").append("<option value=''> No hay registros </option>");
                    }
                });
            });

        });
        function cargar_postulados() {
            var Datos = $("#cargar_postulados");
            var route = "/cargar_postulados";
            $.get(route, function (res) {
                $(res).each(function (key, values) {
                    Datos.append("<option value='" + values.id + "'>" + values.p_nombre + "" + values.s_nombre + " " + values.p_apellido + " " + values.s_apellido + " " + values.tipo_doc + " " + values.doc + " </option>");

                });
            });
        }
    </script>
@endsection