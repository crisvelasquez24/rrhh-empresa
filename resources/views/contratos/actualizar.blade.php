@extends('layouts.app')
@section('titleForm','Actualizar Contrato')
@section('content')
{!! Form::model($contratos,['route' => ['contratos.update',$contratos->id], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'frm', 'name'=>'frm']) !!}
@include('contratos.forms.form_contratos')
<div class="form-group" align='center'>
    {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}
    <button type="button"  onclick="window.parent.location.href = '/contratos'" class="btn btn-sm btn-warning">Cancelar</button>
</div>
{!! Form::close() !!}
@endsection
@section('includesCss')
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
@endsection

