
<table class="table table-striped table-hover table-bordered" >
    <thead>
        <tr>
            <th>{!!Html::image('fotos/uts.jpg',null,['width'=>'150px'])!!}</th>
            <th style="color:black" class="text-center">CONTRATO A TERMINO DEFINIDO</th>
        </tr>
    </thead>
</table>
<br/>

<table class="table table-striped table-hover table-bordered">
    <tbody>
        <tr>
            <th style="color:black">EMPRESA</th>
            <td style="color:black">UNIDADES TECNOLOGICAS DE SANTANDER</td>
        </tr>
        <tr>
            <th style="color:black">NIT</th>
            <td style="color:black">900.127.675-2</td>
        </tr>
        <tr>
            <th style="color:black">DIRECCION</th>
            <td style="color:black">Cra.4 n° 5-04 Buearamanga</td>
        </tr>
        <tr>
            <th style="color:black">TELEFONO</th>
            <td style="color:black">031-6484891</td>
        </tr>
        <tr>
            <th style="color:black">REPRESENTANTE LEGAL</th>
            <td style="color:black">Daniela Jaramillo Correa</td>
        </tr>
        <tr>
            <th style="color:black">CARGO</th>
            <td style="color:black">Gerente Comercial</td>
        </tr>  
        
        <tr>
            <th></th>
            <td></td>
        </tr>  
        <tr>
            <th></th>
            <td></td>
        </tr>  
        
        <tr>
            <th style="color:black">NOMBRE EMPLEADO</th>
            <td style="color:black">{{$contrato->p_nombre}} {{$contrato->s_nombre}} {{$contrato->p_apellido}} {{$contrato->s_apellido}}</td>
        </tr>
        <tr>
            <th style="color:black">IDENTIFICACION</th>
            <td style="color:black">{{$contrato->tipo_doc}} {{$contrato->doc}}</td>
        </tr>
        <tr>
            <th style="color:black">FECHA NACIMIENTO</th>
            <td style="color:black">{{$contrato->fecha_nac}}</td>
        </tr>
        <tr>
            <th style="color:black">DIRECION</th>
            <td style="color:black">{{$contrato->dir}}</td>
        </tr>
        <tr>
            <th style="color:black">TELEFONO</th>
            <td style="color:black">{{$contrato->tel}} - {{$contrato->movil}} </td>
        </tr>
        <tr>
            <th style="color:black">CORREO ELECTRONICO</th>
            <td style="color:black">{{$contrato->email}}</td>
        </tr>
        <tr>
            <th style="color:black">FECHA INICIACION CONTRATO</th>
            <td style="color:black">{{$contrato->fecha_inicio}}</td>
        </tr>
        <tr>
            <th style="color:black">FECHA TERMINACION CONTRATO</th>
            <td style="color:black">{{$contrato->fecha_fin}}</td>
        </tr>
        <tr>
            <th style="color:black">CARGO</th>
            <td style="color:black">{{$contrato->nombre}}</td>
        </tr>
         <tr>
            <th style="color:black">EPS</th>
            <td style="color:black">{{$contrato->eps}}</td>
        </tr>
         <tr>
            <th style="color:black">ARL</th>
            <td style="color:black">{{$contrato->arl}}</td>
        </tr>
    </tbody>
</table>
<br/>
<br/>

<strong><p align="center" style="color:black">CLÁUSULAS</p></strong>
<p style="color:black" class="text-justify">
    Entre los suscritos a saber <strong>DANIELA JARAMILLO CORREA</strong>, identificada con la cédula de ciudadania No. 63356248 de Bucaramanga, actuando como Representante Legal 
    de la Empresa NET PROD NIT 900.127.675-2 quien para los efectos del presente contrato se denominara EMPRESA y <strong>{{$contrato->p_nombre}} {{$contrato->s_nombre}} {{$contrato->p_apellido}} {{$contrato->s_apellido}}</strong>
    identificado con <strong>{{$contrato->tipo_doc}} {{$contrato->doc}}</strong> quien para los efectos del siguiente contrato se denominara el EMPLEADO, se suscribe el siguiente contrato, conforme a lo preceptuado por la ley 789 de 2002.
</p>

<br/>
<br/>
<br/>
<br/>
<br/>

<table>
    <tr>
        <td style="color:black"><strong>DANIELA JARAMILLO CORREA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
        <td style="color:black"><strong>{{$contrato->p_nombre}} {{$contrato->s_nombre}} {{$contrato->p_apellido}} {{$contrato->s_apellido}}</strong></td>
    </tr>
    <tr>
        <td style="color:black">C.C 63356248</td>
        <td style="color:black">{{$contrato->tipo_doc}} {{$contrato->doc}}</td>
    </tr>
    <tr>
        <td style="color:black">LA EMPRESA</td>
        <td style="color:black">EL EMPLEADO</td>
    </tr>
</table>
<br/>
<br/>

<?php 
//require_once ('dompdf/dompdf_config.inc.php');
//$html = '<td>hola mundo!</td>';
//$pdf = new DOMPDF();
//$pdf->set_paper("A4", "portrait");
//$pdf->load_html(utf8_decode($html));
//$php->render();
//$pdf->stream('fichero.pdf');
?>

<div align='center' id="btn_print">
    <button type="button" onclick="window.print()" class="btn btn-sm btn-success"><i class="fa fa-print"></i>&nbsp;Imprimir</button>  
    <button type="button" onclick="window.print()" class="btn btn-sm btn-danger"><i class="fa fa-file-pdf-o"></i>&nbsp;Pdf</button>  
    <button type="button"  onclick="window.parent.location.href = '/contratos'" class="btn btn-sm btn-warning">Atras</button>
</div>
@stop
<style type="text/css" media="print">
    @media print {
        #btn_print{display: none}
    } 
</style>