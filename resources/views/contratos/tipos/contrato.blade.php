@extends('layouts.app')
@section('titleForm','Administrar Contratos')
@section('titleTab')
<div class="title_right">
    <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
        <div class="input-group">
            <a class="form-control" placeholder="Search for...">Crear Contratos</a>
            <span class="input-group-btn">
            {!!Html::decode(
            link_to_route(
            'contratos.create',
            '<button class="btn btn-sm btn-warning"><i class="fa fa-plus-square"></i> <span style="color:#FFFFFF";>Agregar</span></button>',
            [],
            ['class'=>'']
            ))!!}
            </span>
        </div>
    </div>
</div>
@endsection
@section('content')
<!-- page content -->
<div class="center_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <table class="table table-striped table-hover table-bordered" >
                                        <thead>
                                            <tr>
                                                <th>{!!Html::image('fotos/uts.jpg',null,['width'=>'150px'])!!}</th>
                                                <th style="color:black" class="text-center">CONTRATO A TERMINO DEFINIDO</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <br/>
                                    <table class="table table-striped table-hover table-bordered">
                                        <tbody>
                                            <tr>
                                                <th style="color:black">EMPRESA</th>
                                                <td style="color:black">UNIDADES TECNOLOGICAS DE SANTANDER</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">NIT</th>
                                                <td style="color:black">900.127.675-2</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">DIRECCION</th>
                                                <td style="color:black">Cra.4 n° 5-04 Buearamanga</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">TELEFONO</th>
                                                <td style="color:black">031-6484891</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">REPRESENTANTE LEGAL</th>
                                                <td style="color:black">Daniela Jaramillo Correa</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">CARGO</th>
                                                <td style="color:black">Gerente Comercial</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">NOMBRE EMPLEADO</th>
                                                <td style="color:black">{{$contrato->p_nombre}} {{$contrato->s_nombre}} {{$contrato->p_apellido}} {{$contrato->s_apellido}}</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">IDENTIFICACION</th>
                                                <td style="color:black">{{$contrato->tipo_doc}} {{$contrato->doc}}</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">FECHA NACIMIENTO</th>
                                                <td style="color:black">{{$contrato->fecha_nac}}</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">DIRECION</th>
                                                <td style="color:black">{{$contrato->dir}}</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">TELEFONO</th>
                                                <td style="color:black">{{$contrato->tel}} - {{$contrato->movil}} </td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">CORREO ELECTRONICO</th>
                                                <td style="color:black">{{$contrato->email}}</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">FECHA INICIACION CONTRATO</th>
                                                <td style="color:black">{{$contrato->fecha_inicio}}</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">FECHA TERMINACION CONTRATO</th>
                                                <td style="color:black">{{$contrato->fecha_fin}}</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">CARGO</th>
                                                <td style="color:black">{{$contrato->nombre}}</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">EPS</th>
                                                <td style="color:black">{{$contrato->eps}}</td>
                                            </tr>
                                            <tr>
                                                <th style="color:black">ARL</th>
                                                <td style="color:black">{{$contrato->arl}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br/>
                                    <br/>
                                    <strong>
                                        <p align="center" style="color:black">CLÁUSULAS</p>
                                    </strong>
                                    <p style="color:black" class="text-justify">
                                        Entre los suscritos a saber <strong>DANIELA JARAMILLO CORREA</strong>, identificada con la cédula de ciudadania No. 63356248 de Bucaramanga, actuando como Representante Legal 
                                        de la Empresa NET PROD NIT 900.127.675-2 quien para los efectos del presente contrato se denominara EMPRESA y <strong>{{$contrato->p_nombre}} {{$contrato->s_nombre}} {{$contrato->p_apellido}} {{$contrato->s_apellido}}</strong>
                                        identificado con <strong>{{$contrato->tipo_doc}} {{$contrato->doc}}</strong> quien para los efectos del siguiente contrato se denominara el EMPLEADO, se suscribe el siguiente contrato, conforme a lo preceptuado por la ley 789 de 2002.
                                    </p>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <table>
                                        <tr>
                                            <td style="color:black"><strong>DANIELA JARAMILLO CORREA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
                                            <td style="color:black"><strong>{{$contrato->p_nombre}} {{$contrato->s_nombre}} {{$contrato->p_apellido}} {{$contrato->s_apellido}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="color:black">C.C 63356248</td>
                                            <td style="color:black">{{$contrato->tipo_doc}} {{$contrato->doc}}</td>
                                        </tr>
                                        <tr>
                                            <td style="color:black">LA EMPRESA</td>
                                            <td style="color:black">EL EMPLEADO</td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <br/>
                                    <div align='center' id="btn_print">
                                        <button type="button" onclick="window.print()" class="btn btn-sm btn-success"><i class="fa fa-print"></i>&nbsp;Imprimir</button>  
                                        <button type="button"  onclick="window.parent.location.href = '/contratos'" class="btn btn-sm btn-warning">Atras</button>
                                    </div>
                                    <style type="text/css" media="print">
                                        @media print {
                                        #btn_print{display: none}
                                        } 
                                    </style>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('includesCss')
<link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
<link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
<link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
<link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendors/msgbox/jquery.msgbox.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
<script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
<script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
<script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
<script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
<script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
<script src="{{asset('vendors/msgbox/jquery.msgbox.js')}}"></script>
<script src="{{asset('build/js/datatables.js')}}"></script>
<script src="{{asset('build/js/funtionsConfirm.js')}}"></script>
@endsection