<div class="item form-group">
    {!!Html::decode(Form::label('id_cargo', 'Cargo <span>*</span>',
    ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('id_cargo',[''=>'Seleccione un cargo']+$cargos ,null,
        ['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
    </div>
</div>
@if(0 == $modificando)
    <div class="item form-group">
        {!!Html::decode(Form::label('id_hojavida', 'Empleado <span>*</span>',
            ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!!Form::select('id_hojavida',[''=>'Seleccione postulado...'] ,'null',
                ['required'=>'required' , 'class'=>' form-control col-md-7 col-xs-12',
                'id'=>'cargar_postulados'])!!}
        </div>
    </div>
@endif
@if(1 == $modificando)
    <input type="hidden" name="id_hojavida" value="{{ $contratos->id_hojavida }}">
@endif
<div class="item form-group">
    {!!Html::decode(Form::label('id_tipo_contrato', 'Tipo de contrato <span>*</span>',
        ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('id_tipo_contrato', [''=>'Seleccione un Tipo de Contrato']+$tiposContratos,null,
        ['required'=>'required' , 'class'=>'form-control'])!!}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('id_tipo_salario', 'Tipo Salario <span>*</span>',
        ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('id_tipo_salario', [''=>'Seleccione un Tipo de Contrato']+$tiposSalarios,null,
        ['required'=>'required' , 'class'=>'form-control'])!!}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('id_administradora_salud', 'Promotora de Salud <span>*</span>',
        ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('id_administradora_salud',
            [''=>'Seleccione promotora de Salud']+$administradoraSalud,null,
        ['required'=>'required' , 'class'=>'form-control'])!!}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('id_administradora_pension', 'Promotora de Pensión <span>*</span>',
        ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('id_administradora_pension',
            [''=>'Seleccione promotora de Pensión']+$administradoraPension,null,
        ['required'=>'required' , 'class'=>'form-control'])!!}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('fecha_inicio', 'Fecha de Inicio <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::text('fecha_inicio', null,['placeholder'=>'Ingrese Fecha',
            'maxlength'=>"20",'required'=>'required' ,
            'class'=>'form-control single_cal2 col-md-7 col-xs-12', 'id' => 'fecha_inicio']
        )
        !!}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('fecha_fin', 'Fecha de Fin <span>*</span>',
    ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::text('fecha_fin', null,['placeholder'=>'Ingrese Fecha',
            'maxlength'=>"20",'required'=>'required' ,
            'class'=>'form-control single_cal2 col-md-7 col-xs-12', 'id' => 'fecha_fin']
        )
        !!}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('meses', 'Tiempo <span>*</span>',
        ['placeholder'=>'Ingrese ','class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        <span class='form-control'>Tiempo en meses</span>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('fecha_fin', 'Salario Básico <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::number('salario_basico', null,[
            'required'=>'required','class'=>'form-control','placeholder'=>'Ingrese Salario Básico'])!!}
    </div>
</div>
 <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Pais:
        <span class="required obligatorio">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('id_pais',[''=>'Seleccione...']+$paises, 114,
        ['required'=>'required', 'id'=>'pais', 'class'=>'form-control'])!!}
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Departamento:
        <span class="required obligatorio">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('id_departamento',[''=>'Seleccione...']+$departamentos, 18,['required'=>'required','id'=>'departamento', 'class'=>'form-control'])!!}
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Ciudad:
        <span class="required obligatorio">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::select('id_ciudad',[''=>'Seleccione...']+$ciudades,
        138,['required'=>'required','id'=>'ciudad', 'class'=>'form-control'])!!}
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aporte_salud"> Aporte Pensión
        <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p>
        SI:
        <input type="radio" class="flat" name="aporte_pension" value="0"
        required/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;
        No:
        <input  type="radio" class="flat" name="aporte_pension" checked="" value="1" />
        </p>
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aporte_pension"> Aporte Salud
        <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p>
        SI:
        <input type="radio" class="flat" name="aporte_salud" value="0"
        required/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;
        No:
        <input  type="radio" class="flat" name="aporte_salud" checked="" value="1" />
        </p>
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aporte_riesgos"> Aporte Riesgos
        <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p>
        SI:
        <input type="radio" class="flat" name="aporte_riesgos" value="0"
        required/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;
        No:
        <input  type="radio" class="flat" name="aporte_riesgos"
        checked="" value="1" />
        </p>
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12"
    for="aporte_parafiscales"> Aporte Parafiscales
        <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p>
        SI:
        <input type="radio" class="flat" name="aporte_parafiscales" value="0"
        required/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;
        No:
        <input  type="radio" class="flat" name="aporte_parafiscales" checked="" value="1" />
        </p>
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto">Documentos
        <span class="required obligatorio">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="hidden" name="estado" value="1">
        <?php if (isset($hojadevida)) { ?>
            {!!Html::image('fotos/'.$hojadevida->foto,null,['class'=>'img-thumbnail profile_img', 'style'=>'width:150px'])!!}
        <?php } ?>
        {!!Form::file('documento',['required'=>'required','class'=>''])!!}
    </div>
</div>