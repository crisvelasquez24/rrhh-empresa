<div class="item form-group">
    {!!Html::decode(Form::label('numero_dias', 'Numero de días <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::text('numero_dias', null,['required'=>'required' , 'id'=>'numero_dias' ,'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa el numero de dias '])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_salida">Fecha de salida:
        <span class="required obligatorio">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="fecha_salida" type="text" class="form-control single_cal2" placeholder='Ingrese la fecha de salida' name="fecha_salida" required>
        <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
    </div>
</div>

<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="fecha_entrada">Fecha de entrada:
        <span class="required obligatorio">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="hidden" name="fecha_entrada" id="fecha_entrada">
        <span class="form-control" id="fecha_entradaSpan"></span>
       
    </div>
</div>

 <div class="item form-group">

        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="remuneracion"> Remuneracion
            <span class="required">*</span>
        </label>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <p>
            Si
            <input id="radio1" type="radio" class="flat" name="remuneracion" id="" 
            checked="" value="2" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            No
            <input type="radio" class="flat" name="remuneracion" id="" value="1" 
            required/>
              
              <input type="hidden" name="estado" value="1">                          
            
        </p>
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('observaciones', 'observaciones <span>*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">        
        {!!Form::text('observaciones', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Ingresa observaciones '])!!}
        <div class="help-block with-errors"></div>
    </div>
</div>
