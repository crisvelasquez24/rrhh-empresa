@extends('layouts.formModal')
@section('titulo','Vacaciones | Crear')
@section('nombrepagina','Nuevo registro')
@section('contenido')
{!! Form::open(['route' => 'registrar_vacaciones.store', 'method'=>'POST','id'=>'frm','name'=>'frm', 'class'=>'form-horizontal']) !!}
@include('vacaciones.forms.form_registro')
<div class="form-group" align='center'>
    {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success','id'=>'btnuser'])!!}     
    <button type="button"  onclick="window.parent.location.href = '/vacaciones'" class="btn btn-sm btn-warning">Cancelar</button>
</div>
{!!Form::close()!!}

@stop

@section('includesCss')
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
<script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('vendors/validator/validator.js')}}"></script>

<script type="text/javascript">
	
	$(document).ready(function() {

	    $("#fecha_salida").change(function (event) {
	    	if($("#numero_dias").val() && event.target.value) {
		        $.get("/getfechaentrada/" + $("#numero_dias").val() + "/" + event.target.value, function (response, torres) {
		        	$("#fecha_entradaSpan").text(response);
		        	$("#fecha_entrada").val(response);		    
		        });
	    	}
	    
	    });
	    $("#numero_dias").change(function (event) {
	    	if($("#numero_dias").val() && event.target.value) {
		        $.get("/getfechaentrada/" + event.target.value + "/" + $("#fecha_salida").val(), function (response, torres) {
		        	$("#fecha_entradaSpan").text(response);
		        	$("#fecha_entrada").val(response);		    
		        });
	    	}
	    
	    });

	});
</script>
@endsection