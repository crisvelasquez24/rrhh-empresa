<?php

if (Session::has('message-success')) {
    echo "<script type='text/javascript'>
    $(function () {
    new PNotify({
    title: 'Transaccion Exitosa',
    text: '" . Session::get("message-success") . "',
        type: 'success',
        styling: 'bootstrap3'
        });
        });
        </script>";
}
?>