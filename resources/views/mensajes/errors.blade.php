<?php

if (Session::has('message-error')) {
    echo "<script type='text/javascript'>
    $(function () {
    new PNotify({
    title: 'Transaccion Erronea',
    text: '" . Session::get("message-error") . "',
        type: 'error',
        styling: 'bootstrap3'
        });
        });
        </script>";
}
?>