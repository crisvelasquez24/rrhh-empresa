@extends('layouts.app')
@section('title','Registrar Perfiles')
@section('content')
{!! Form::model($festivos,['route' => ['festivos.update',$festivos->id], 'id'=>'frm','name'=>'frm',
    'method'=>'PUT', 'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('festivos.forms.form_festivos')
        <div class="form-group" align='center'>
           {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}
            <button type="button"  onclick="window.parent.location.href = '/festivos'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection