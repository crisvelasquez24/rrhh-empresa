<div class="x_content">
    <div class="item form-group">
        {!!Html::decode(Form::label('nombre', 'Año <span class="required obligatorio">*</span>',
            ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!!Form::number('ano', null,['required'=>'required' , 'class'=>'form-control'])!!}
        </div>
    </div>
</div>
@if($update)
    <div class="x_content">
        <div class="item form-group">
            {!!Html::decode(Form::label('dia_festivo', 'Día festivo <span class="required obligatorio">*</span>',
                ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {!!Form::text('dia_festivo', null,
            ['required'=>'required','class'=>'form-control single_cal2','placeholder'=>'aaaa/mm/dd'])!!}
            <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
            </div>
        </div>
    </div>
    <div class="x_content">
        <div class="item form-group">
            {!!Html::decode(Form::label('nombre_festivo', 'Nombre Festivo <span class="required obligatorio">*</span>',
                ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {!!Form::text('nombre_festivo', null,['required'=>'required' , 'class'=>'form-control'])!!}
            </div>
        </div>
    </div>
@endif
