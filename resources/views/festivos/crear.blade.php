@extends('layouts.app')
@section('titleForm','Crear Perfiles')
@section('titleTab')
@endsection
@section('content')
    {!! Form::open(['route' => 'festivos.store', 'method'=>'POST','id'=>'demo-form2','name'=>'frm',
    'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('festivos.forms.form_festivos')
        <div class="form-group" align='center'>
            {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}
            <button type="button"  onclick="window.parent.location.href = '/festivos'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesCss')
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
@endsection