@extends('layouts.base')
@section('titulo','Cargos | Administrar')
@section('namepage','<h2>Cargos</h2>')
@section('contenido')
<div>
    {!!Html::decode(link_to_route('cargos.create', '<button class="btn btn-sm btn-warning"><i class="fa fa-plus-square"></i> Agregar</button>', [], ['class'=>'iframe']))!!}
</div>
<br/>
<table class="table table-striped table-hover table-bordered" id="datatable-responsive">
    <thead>
        <tr>
            <th>Departamento</th>
            <th>Cargo</th>
            <th>Funciones</th>
            <th>Objetivos</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($cargos as $data)
        <tr>
            <td>{{$data->nom_depar}}</td>
            <td>{{$data->nombre}}</td>
            <td>{{$data->funciones}}</td>
            <td>{{$data->objetivos}}</td>
            <td>
                {!!Html::decode(link_to_route('cargos.edit', '<button class="btn-primary" title="Editar"><i class="fa fa-edit"></i></button>', [$data->id], ['class'=>'iframe']))!!}
                <a onclick="del({{$data->id}},'cargosE/{{$data->nombre}}/','Cargo: {{$data->nombre}}');"><button class="btn-danger" title="Eliminar"><i class="fa fa-trash-o"></i></button></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop
