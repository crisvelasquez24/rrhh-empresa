@extends('layouts.form')
@section('titulo','Cargos | Actualizar')
@section('nombrepagina','Actualizar Cargo')
@section('contenido')
{!! Form::model($cargos,['route' => ['cargos.update',$cargos->id], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'frm', 'name'=>'frm']) !!}
@include('forms.form_cargos')
<div class="form-group" align='center'>
    {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}
    <button type="button"  onclick="window.parent.location.href = '/cargos'" class="btn btn-sm btn-warning">Cancelar</button>
</div>
{!! Form::close() !!}
@stop
