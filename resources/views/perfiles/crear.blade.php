@extends('layouts.app')
@section('titleForm','Crear Perfiles')
@section('titleTab')
@endsection
@section('content')
    {!! Form::open(['route' => 'perfiles.store', 'method'=>'POST','id'=>'demo-form2','name'=>'frm',
    'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('perfiles.forms.form_perfiles')
        <div class="form-group" align='center'>
            {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}
            <button type="button"  onclick="window.parent.location.href = '/perfiles'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection