@extends('layouts.app')
@section('title','Registrar Perfiles')
@section('content')
{!! Form::model($perfiles,['route' => ['perfiles.update',$perfiles->id], 'id'=>'frm','name'=>'frm',
    'method'=>'PUT', 'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('perfiles.forms.form_perfiles')
        <div class="form-group" align='center'>
           {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}
            <button type="button"  onclick="window.parent.location.href = '/perfiles'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection