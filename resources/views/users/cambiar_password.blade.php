@extends('layouts.form')
@section('titulo','Usuarios | Actualizar')
@section('nombrepagina','Cambiar contraseña')
@section('contenido')
{!! Form::open(['route' => 'updatepassword','id'=>'frm','name'=>'frm', 'method'=>'POST', 'class'=>'form-horizontal']) !!}
{!!Form::hidden('id',$id)!!}
@include('forms.form_password')
<div class="form-group" align='center'>
    {!!Form::submit('Cambiar',['class'=>'btn btn-sm btn-primary'])!!}     
    <button type="button"  onclick="window.parent.location.href = '/'" class="btn btn-sm btn-warning">Cancelar</button>
</div>
{!! Form::close() !!}
@stop


