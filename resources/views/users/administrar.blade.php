@extends('layouts.app')
@section('titleForm','Administrar Usuarios')
@section('titleTab')
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <a class="form-control" placeholder="Search for...">Crear usuarios</a>
                <span class="input-group-btn">
                    {!!Html::decode(
                        link_to_route(
                        'users.create',
                        '<button class="btn btn-sm btn-warning"><i class="fa fa-plus-square"></i> <span style="color:#FFFFFF";>Agregar</span></button>',
                        [],
                        ['class'=>'']
                    ))!!}
                </span>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- page content -->
    <div class="center_col" role="main">
      <div class="">

        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_content">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
                                <li role="presentation" class=""><a href="#tab_content11" id="home-tabb"
                                    role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Inactivos</a>
                                </li>
                                <li role="presentation" class=""><a href="#tab_content22" role="tab" id="profile-tabb"
                                    data-toggle="tab" aria-controls="profile" aria-expanded="false">Publicos</a>
                                </li>
                                <li role="presentation" class="active"><a href="#tab_content33" role="tab" id="profile-tabb3"
                                    data-toggle="tab" aria-controls="profile" aria-expanded="false">Activos</a>
                                </li>
                            </ul>
                            <div id="myTabContent2" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content33" aria-labelledby="home-tab">
                                    <div class="card-box table-responsive">
                                        <table id="datatable-keytable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Perfil</th>
                                                    <th>Nombres</th>
                                                    <th>Email</th>
                                                    <th>Estado</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach($activos as $data)
                                                    <tr>
                                                        <td>{{$data->nom_perfil}}</td>
                                                        <td>{{$data->first_name }}{{ $data->last_name }}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td>
                                                            <a href="#" onclick="changeEstado({{$data->id}}, 'estado/{{$data->first_name}}/',
                                                                '{{$data->first_name}}');">
                                                                    <span class="label label-info" title="Cambiar estado">Desactivar</span>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            {!!Html::decode(link_to_route('users.edit',
                                                            '<button class="btn-primary" title="Editar"><i class="fa fa-edit"></i></button>', [$data->id],
                                                                ['class'=>'iframe']))!!}

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content22" aria-labelledby="profile-tab">
                                    <div class="card-box table-responsive">
                                        <table id="datatable-keytable2" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Perfil</th>
                                                    <th>Nombres</th>
                                                    <th>Email</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach($publicos as $data)
                                                    <tr>
                                                        <td>{{strtoupper($data->nom_perfil)}}</td>
                                                        <td>{{strtoupper($data->first_name)." " }}{{ strtoupper($data->last_name) }}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td>
                                                            {!!Html::decode(link_to_route('users.edit',
                                                            '<button class="btn-primary" title="Editar"><i class="fa fa-edit"></i></button>', [$data->id],
                                                                ['class'=>'iframe']))!!}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content11" aria-labelledby="profile-tab">
                                    <div class="card-box table-responsive">
                                        <table id="datatable-keytable3" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Perfil</th>
                                                    <th>Nombres</th>
                                                    <th>Email</th>
                                                    <th>Estado</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach($inactivos as $data)
                                                    <tr>
                                                        <td>{{$data->nom_perfil}}</td>
                                                        <td>{{$data->first_name}}{{ $data->last_name }}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td>
                                                            <a href="#" onclick="changeEstado({{$data->id}}, 'estado/{{$data->first_name}}/',
                                                                '{{$data->first_name}}');">
                                                                    <span class="label label-info" title="Cambiar estado">Activar</span>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            {!!Html::decode(link_to_route('users.edit',
                                                            '<button class="btn-primary" title="Editar"><i class="fa fa-edit"></i></button>', [$data->id],
                                                                ['class'=>'iframe']))!!}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('includesCss')
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/msgbox/jquery.msgbox.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/msgbox/jquery.msgbox.js')}}"></script>
    <script src="{{asset('build/js/datatables.js')}}"></script>
    <script src="{{asset('build/js/funtionsConfirm.js')}}"></script>
@endsection