<div class="item form-group">
    {!!Html::decode(Form::label('id_perfil', 'Perfil <span class="required obligatorio">*</span>',
        ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
       {!!Form::select('id_perfil',[''=>'Seleccione...']+$perfiles ,
       null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12'])!!}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('nombre', 'Nombre <span class="required obligatorio">*</span>',
        ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::text('first_name', null,['placeholder'=>'Ingresa Nombre','required'=>'required' , 'class'=>'form-control'])!!}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('nombre', 'Apellido <span class="required obligatorio">*</span>',
        ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::text('last_name', null,['placeholder'=>'Ingresa Apellido','required'=>'required' , 'class'=>'form-control'])!!}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('nombre', 'Nombre de Usuario <span class="required obligatorio">*</span>',
        ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!Form::text('name', null,['placeholder'=>'Ingresa Nombre de usuario','required'=>'required' ,
        'id'=>'idnombreUsuaio','class'=>'form-control'])!!}
    </div>
</div>
<div class="item form-group" id="divEmail">
    {!!Html::decode(Form::label('nombre', 'Email <span class="required obligatorio">*</span>',
        ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
    <div class="col-md-6 col-sm-6 col-xs-12">
         {!!Form::email('email', null,['required'=>'required' , 'class'=>'form-control col-md-7 col-xs-12',
            'data-validate-exists'=>"1",
            'placeholder'=>'Ingresa el email', 'id'=>'idemail'])!!}
    </div>
</div>
@if(count($users) == 0)
    <div class="item form-group">
        {!!Html::decode(Form::label('nombre', 'Confirmar Email <span class="required obligatorio">*</span>',
            ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="email" id="email2" name="confirm_email" data-validate-linked="email"
            required="required" placeholder = 'Ingresa confirmación email' class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="item form-group">
        {!!Html::decode(Form::label('nombre', 'Contraseña <span class="required obligatorio">*</span>',
            ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']))!!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!!Form::password('password',['class'=>'form-control col-md-7 col-xs-12','required'=>'required',
                'placeholder'=>'Ingresa la contraseña',
            'data-minlength'=>'6','data-minlength-error'=>'Minimo de 6 caracteres',])!!}
        </div>
    </div>
@endif