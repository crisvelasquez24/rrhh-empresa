@extends('layouts.app')
@section('title','Asignar | Evaluación')
@section('content')
    {!! Form::open(['route' => 'asignar_evaluacion.store', 'method'=>'POST',
        'id'=>'demo-form2','name'=>'frm',
    'class'=>'form-horizontal','files'=>true]) !!}
        @include('evaluaciones.forms.form_asignar_evaluaciones')
        <div class="form-group" align='center'>
            {!!Form::submit('Crear',['class'=>'btn btn-sm btn-success'])!!}
            <button type="button"  onclick="window.parent.location.href = '/evaluacion'" 
                class="btn btn-sm btn-warning">Cancelar</button>
        </div>        
    {!!Form::close()!!}
@endsection
@section('includesCss')
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">

@endsection
@section('includesScripts')
<script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js')}}"></script>
<script src="{{asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('vendors/validator/validator.js')}}"></script>
<script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('vendors/select2/dist/js/select2.full.min.js')}}"></script>
<script>
    function cargar() {        
        var tablaDatos = $("#select_empleados");
        var route = "/asignar_empleado";
        $("#select_empleados").empty();
        $.get(route, function (res) {            
            $(res).each(function (key, values) {
                values.id
                tablaDatos.append("<option value='" + values.id + "'>" + values.p_nombre + " " + values.p_apellido + " " + values.s_apellido + " </option>");

            });
        });
    }
    $(document).ready(function () {
        $(".select2_multiple").select2({
            placeholder: "Seleccione empleados...",
            allowClear: true
        });
        cargar();
    });
</script>
@endsection
