@extends('layouts.app')
@section('title','Actualizar Evaluación')
@section('content')
{!! Form::model($evaluaciones,['route' => ['evaluacion.update',$evaluaciones->id], 'id'=>'frm','name'=>'frm',
    'method'=>'PUT', 'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('evaluaciones.forms.form_evaluaciones')
        <div class="form-group" align='center'>
           {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary','id'=>'btn_evaluacion'])!!}
            <button type="button"  onclick="window.parent.location.href = '/evaluacion'" 
            class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        var parame = '<?= $parame; ?>';
        if (parame == 1) {
            $(':radio:not(:checked)').attr('disabled', true);
            $("input").prop('disabled', true);
            $("select").prop('disabled', true);
            document.getElementById('btn_evaluacion').style.display = 'none';
        }
    });
</script>
@endsection
