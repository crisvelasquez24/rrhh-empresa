<div class="row">

    <div class="item form-group" >
        {!!Html::decode(Form::label('id_cargo', 'Cargo *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        <div class="col-md-6 col-sm-6 col-xs-6 form-group">        
            {!!Form::select('id_cargo', [''=>'Seleccione un cargo...']+$cargos ,null,['required'=>'required' , 'class'=>'form-control'])!!}
            <div class="help-block with-errors"></div>
        </div>
    </div>

    <div class="item form-group">
        {!!Html::decode(Form::label('nombre', 'Nombre Evaluación*', array('for'=>'nombre', 'class' => 'control-label col-md-3 col-sm-3 col-xs-3')))!!}
        <div class="col-md-6 col-sm-6 col-xs-6">
            {!!Form::text('nombre', null,['required'=>'required' , 'class'=>'form-control','maxlength'=>"250",'placeholder'=>'Ingresa el nombre de la evaluación'])!!}
            <div class="help-block with-errors"></div>
        </div>
    </div>
   
    <div class="col-lg-6">
        <div class="input-group">
            <span class="input-group-addon">
                <div></div>
            </span>
            {!!Form::text('p1', null,['id'=>'idp1', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Pregunta #1'])!!}
        </div>
        <div class="col-lg-4" ></div>
        <div class="col-lg-4" align="rigth">
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp1', 'A1',null,['id'=>'idr1a', 'aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op1_a', null,['id'=>'idop1_a', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #1'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp1', 'B1',null,['id'=>'idr1b','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op1_b', null,['id'=>'idop1_b', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #2'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp1', 'C1',null,['id'=>'idr1c','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op1_c', null,['id'=>'idop1_c', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #3'])!!}
            </div>
        </div>
      
    </div>

    <div class="col-lg-6">
        <div class="input-group">
            <span class="input-group-addon">
                <div></div>
            </span>
            {!!Form::text('p2', null,['id'=>'idp2', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Pregunta #2'])!!}
        </div>
        <div class="col-lg-4" ></div>
        <div class="col-lg-4" align="rigth">
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp2', 'A2',null,['id'=>'idr2a','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op2_a', null,['id'=>'idop2_a', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #1'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp2', 'B2',null,['id'=>'idr2b','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op2_b', null,['id'=>'idop2_b', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #2'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp2', 'C2',null,['id'=>'idr2c','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op2_c', null,['id'=>'idop2_c', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #3'])!!}
            </div>
        </div>     
    </div>


 


    <div class="col-lg-6">
        <div class="input-group">
            <span class="input-group-addon">
                <div></div>
            </span>
            {!!Form::text('p3', null,['id'=>'idp3', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Pregunta #3'])!!}
        </div>
        <div class="col-lg-4" ></div>
        <div class="col-lg-4" align="rigth">
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp3', 'A3',null,['id'=>'idr3a','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op3_a', null,['id'=>'idop3_a', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #1'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp3', 'B3',null,['id'=>'idr3b','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op3_b', null,['id'=>'idop3_b', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #2'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp3', 'C3',null,['id'=>'idr3c','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op3_c', null,['id'=>'idop3_c', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #3'])!!}
                
            </div>
        </div>        
    </div>         



    <div class="col-lg-6">
        <div class="input-group">
            <span class="input-group-addon">
                <div></div>
            </span>
            {!!Form::text('p4', null,['id'=>'idp4', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Pregunta #4'])!!}
        </div>
        <div class="col-lg-4" ></div>
        <div class="col-lg-4" align="rigth">
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp4', 'A4',null,['id'=>'idr4a','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op4_a', null,['id'=>'idop4_a', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #1'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp4', 'B4',null,['id'=>'idr4b','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op4_b', null,['id'=>'idop4_b', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #2'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp4', 'C4',null,['id'=>'idr4c','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op4_c', null,['id'=>'idop4_c', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #3'])!!}
            </div>
        </div>
    </div>
    
   

    <div class="col-lg-6">
        <div class="input-group">
            <span class="input-group-addon">
                <div></div>
            </span>
            {!!Form::text('p5', null,['id'=>'idp5', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Pregunta #5'])!!}
        </div>
        <div class="col-lg-4" ></div>
        <div class="col-lg-4" align="rigth">
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp5', 'A5',null,['id'=>'idr5a','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op5_a', null,['id'=>'idop5_a', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #1'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp5', 'B5',null,['id'=>'idr5b','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op5_b', null,['id'=>'idop5_b', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #2'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp5', 'C5',null,['id'=>'idr5c','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op5_c', null,['id'=>'idop5_c', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #3'])!!}
            </div>
        </div>
    </div>

    

    <div class="col-lg-6">
        <div class="input-group">
            <span class="input-group-addon">
                <div></div>
            </span>
            {!!Form::text('p6', null,['id'=>'idp6', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Pregunta #6'])!!}
        </div>
        <div class="col-lg-4" ></div>
        <div class="col-lg-4" align="rigth">
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp6', 'A6',null,['id'=>'idr6a','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op6_a', null,['id'=>'idop6_a', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #1'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp6', 'B6',null,['id'=>'idr6b', 'aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op6_b', null,['id'=>'idop6_b', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #2'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp6', 'C6',null,['id'=>'idr6c','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op6_c', null,['id'=>'idop6_c', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #3'])!!}
            </div>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="input-group">
            <span class="input-group-addon">
                <div></div>
            </span>
            {!!Form::text('p7', null,['id'=>'idp7', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Pregunta #7'])!!}
        </div>
        <div class="col-lg-4" ></div>
        <div class="col-lg-4" align="rigth">
            <div class="input-group">
                <span class="input-group-addon">      
                    {!!Form::radio('rp7', 'A7',null,['id'=>'idr7a', 'aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op7_a', null,['id'=>'idop7_a', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #1'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp7', 'B7',null,['id'=>'idr7b', 'aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op7_b', null,['id'=>'idop7_b', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #2'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp7', 'C7',null,['id'=>'idr7c','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op7_c', null,['id'=>'idop7_c',  'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #3'])!!}
            </div>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="input-group">
            <span class="input-group-addon">
                <div></div>
            </span>
            {!!Form::text('p8', null,['id'=>'idp8', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Pregunta #8'])!!}
        </div>
        <div class="col-lg-4" ></div>
        <div class="col-lg-4" align="rigth">
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp8', 'A8',null,['id'=>'idr8a', 'aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op8_a', null,['id'=>'idop8_a', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #1'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">      
                    {!!Form::radio('rp8', 'B8',null,['id'=>'idr8b','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op8_b', null,['id'=>'idop8_b', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #2'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">      
                    {!!Form::radio('rp8', 'C8',null,['id'=>'idr8c','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op8_c', null,['id'=>'idop8_c', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #3'])!!}
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="input-group">
            <span class="input-group-addon">
                <div></div>
            </span>
            {!!Form::text('p9', null,['id'=>'idp9', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Pregunta #9'])!!}
        </div>
        <div class="col-lg-4" ></div>
        <div class="col-lg-4" align="rigth">
            <div class="input-group">
                <span class="input-group-addon">      
                    {!!Form::radio('rp9', 'A9',null,['id'=>'idr9a' ,'aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op9_a', null,['id'=>'idop9_a', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #1'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp9', 'B9',null,['id'=>'idr9b', 'aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op9_b', null,['id'=>'idop9_b', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #2'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp9', 'C9',null,['id'=>'idr9c','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op9_c', null,['id'=>'idop9_c', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #3'])!!}
            </div>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="input-group">
            <span class="input-group-addon">
                <div></div>
            </span>
            {!!Form::text('p10', null,['id'=>'idp10', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Pregunta #10'])!!}
        </div>
        <div class="col-lg-4" ></div>
        <div class="col-lg-4" align="rigth">
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp10', 'A10',null,['id'=>'idr10a', 'aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op10_a', null,['id'=>'idop10_a', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #1'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp10', 'B10',null,['id'=>'idr10b','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op10_b', null,['id'=>'idop10_b', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #2'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    {!!Form::radio('rp10', 'C10',null,['id'=>'idr10c','aria-label'=>'radio for following text input'])!!}
                </span>
                {!!Form::text('op10_c', null,['id'=>'idop10_c', 'required'=>'required' , 'class'=>'form-control', 'maxlength'=>"250", 'aria-label'=>'Text input with radio' ,'placeholder'=>'Opcion #3'])!!}
            </div>
        </div>
    </div>
</div>

