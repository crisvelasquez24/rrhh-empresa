<!DOCTYPE html>
<html lang="en">
@include('partials._head')

<body class="nav-md" >
<div class="container body">

    <div class="main_container">

    {{--top nav--}}
    @include('partials._sidenav')
    {{--/topnav--}}

    <!-- top navigation -->
    @include('partials._topnav')
    <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" >
            <div class="page-title">
                <div class="title_left">
                    <h3>@yield('title')</h3>
                </div>
                @yield('titleTab')
            </div>
            <div class="clearfix"></div>
            <div class="data-pjax">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>@yield('titleForm') <small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
    @include('partials._footer')
    <!-- /footer content -->
    </div>
</div>



@include('layouts.js')
@section('includesScripts')

@show
{{-- @include('partials._notification') --}}
@stack('scripts')
@include('mensajes.errors')
@include('mensajes.success')
@include('auth.change_pass')
</body>
<script type="text/javascript">
    $(document).ready(function(){
        $("#cambiarPass").click(function() {
            $('#changePass').modal('toggle');
        });

        $("#botonContinuarPass").click(function() {
            if ($("#password").val() != $("#password-confirm").val()) {
                alert("no coincide contraseñas");
                return;
            }            
            $('#changePass').modal('hide');
            var route = "changePass/";
            var token = $("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:{
                    password: $("#password").val()
                },

                success:function(response){
                    alert("Contraseña Cambiada Correctamente");
                },
                error:function(msj){
                    alert("Algo Salio mal");
                }
            });
    });
    });
</script>
</html>