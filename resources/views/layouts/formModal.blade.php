<!DOCTYPE html>
<html >
    <head>
        <title> @yield('titulo')</title>
        @include('partials._head')
    </head>
    <body style="background-color: #ffffff;">
        <br /> 
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>@yield('nombrepagina') <small></small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @yield('contenido')
                    </div>
                </div>
            </div>
        </div>   
    </body>
     @include('layouts.js')
     @section('includesScripts')
     @show
</html>