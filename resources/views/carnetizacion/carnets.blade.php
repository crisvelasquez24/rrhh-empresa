@extends('layouts.app')
@section('titleForm','Administrar Carnet')
@section('content')
<br/>
    <style type="text/css">
    img{
        padding-left: 20px;
    }
</style>
<script type="text/javascript">
    function changeTamanio(caja) {
        var elementStyle = caja.style;
        elementStyle.position = "relative";
        elementStyle.top = elementStyle.left = "720px";
    }
</script>
<div class="container text-center" style="border: 1px solid #a1a1a1;padding: 15px;width: 70%;">
     <img src="/img/carnet.png"
    width="500px;" height="650px;" alt="barcode" />
    <div style="position:absolute; top:180px; left:340px; visibility:visible z-index:1">

 <div class="item form-group">
    {!!Html::decode(
        Form::label(
            'p_nombre',
            'Nombres: ',
            ['class' => 'control-label col-md-12 col-sm-12 col-xs-12']
        )
    )!!}
 <br/>
</div>
<div class="item form-group">
    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
        {{$datosHoja[0]->p_nombre}}
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
        {{$datosHoja[0]->p_apellido}}
    </div>
</div>
    </div>
    <div style="position:absolute; top:520px; left:321px; visibility:visible z-index:1">
        <?php $barra = $datosHoja[0]->id_user .'0000000000'. $datosHoja[0]->doc ?>
        <img
            src="data:image/png;base64,{{DNS1D::getBarcodePNG("$barra", 'S25')}}"
            width="400px;"
            height="120px;"
            alt="barcode" />
    </div>

<div style="position:absolute; top:300px; left:420px; visibility:visible z-index:1">
    <img
        width="200px;"
        height="180px;"
        src="/fotos/{{ $file_path }}"
    >
</div>
<div align='center' id="btn_print">
    <button type="button" onclick="changeTamanio(this);window.print()" class="btn btn-sm btn-success"><i class="fa fa-print"></i>&nbsp;Imprimir</button>
    <button type="button"  onclick="window.parent.location.href = '/'" class="btn btn-sm btn-warning">Atras</button>
</div>
</div>
@endsection
