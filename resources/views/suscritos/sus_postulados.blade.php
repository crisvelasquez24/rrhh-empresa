@extends('layouts.form')
@section('titulo','Suscritos | Administrar')
@section('nombrepagina','<h2>Sus postulaciones</h2>')
@section('contenido')
<br/>
<table class="table table-striped table-hover table-bordered" id="order_suspostulados">
    <thead>
        <tr>
            <th>Vacante</th>
            <th>Fecha postulación</th>
        </tr>
    </thead>
    <tbody>
        @foreach($postulados as $data)
        <tr>
            <td>{{$data->nombre}}</td>
            <td>{{$data->fecha_postulacion}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop