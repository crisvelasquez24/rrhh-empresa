@extends('layouts.form')
@section('titulo','Suscritos | Administrar')
@section('nombrepagina','<h2>Sus convocatorias</h2>')
@section('contenido')
<br/>
<table class="table table-striped table-hover table-bordered" id="order_susconvocatorias">
    <thead>
        <tr>
            <th>Vacante</th>
            <th>Área de Interés</th>
            <th>Fecha y Hora</th>
            <th>Lugar</th>
            <th>Convocado</th>
        </tr>
    </thead>
    <tbody>
        @foreach($convocatorias as $data)
        <tr>
            <td>{{$data->vacante}}</td>
            <td>{{$data->a_interes}}</td>
            <td>{{$data->fecha}} {{$data->hora}}</td>
            <td>{{$data->lugar}}</td>
            <td>{{$data->created_at}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop