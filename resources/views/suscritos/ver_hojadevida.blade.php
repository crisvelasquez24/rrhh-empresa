@extends('layouts.app')
@section('titleForm','Ver hoja de Vida')
@section('content')

<h2 align="center"><strong>Información Personal</strong></h2>
<br/>
<div class="item form-group">
    <div align="center" class="col-md-12 col-sm-12 col-xs-12 form-group">
        {!!Html::image('fotos/' . $nombreArchivo,null,['class'=>'img-thumbnail profile_img', 'style'=>'width:150px'])!!}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('p_nombre', 'Primer Nombre: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('s_nombre', 'Segundo Nombre: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('s_nombre', 'Primer Apellido: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('s_nombre', 'Segundo Apellido: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->p_nombre}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->s_nombre}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->p_apellido}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->s_apellido}}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('tipo_doc', 'Tipo de Indentificación: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('doc', 'Identificación: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('estado_civil', 'Estado Civil: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('genero', 'Genero: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->tipo_doc}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->doc}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->estado_civil}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->genero}}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('email', 'Email: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('id_area_interes', 'Área de Interés: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('tel', 'Teléfono: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('movil', 'Móvil: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->email}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->nom_area_interes}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->tel}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->movil}}
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 form-group">
    <br/>
    <h2><strong>Datos de Nacimiento</strong></h2>
</div>
<br/>

<div class="item form-group">
    {!!Html::decode(Form::label('fecha_nac', 'Fecha: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('id_paises', 'Pais: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('id_departamentos', 'Departamento: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('id_ciudades', 'Ciudad: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->fecha_nac}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->nom_pais}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->nom_departamento}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->nom_ciudad}}
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 form-group">
    <br/>
    <h2><strong>Residencia Actual</strong></h2>
</div>
<br/>

<div class="item form-group">
    {!!Html::decode(Form::label('dir', 'Dirección: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('paises_hab', 'Pais: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('departamentos_hab', 'Departamento: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('ciudades_hab', 'Ciudad: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->dir}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->departamento_actual}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->ciudad_actual}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->nom_area_interes}}
    </div>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 form-group">
    <br/>
    <h2><strong>Informacion Laboral</strong></h2>
</div>
<br/>

<div class="item form-group">
    {!!Html::decode(Form::label('profecion', 'Profesión u oficio: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('perfil_laboral', 'Perfil Laboral: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('anos_ex', 'Años de experiencia:  ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('Aspiración ', 'Aspiración salarial : ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->profesion}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->perfil_laboral}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->anos_experiencia}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
        {{$hojadevida->aspiracion_salarial}}Millones
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12 form-group">
<div class="item form-group">
    {!!Html::decode(Form::label('profecion', '¿Trabaja actualmente?:', ['class' => 'control-label col-md-4 col-sm-4 col-xs-4']))!!}
    {!!Html::decode(Form::label('perfil_laboral', 'Posibilidad de trasladarse (a otra ciudad o país):  ', ['class' => 'control-label col-md-4 col-sm-4 col-xs-4']))!!}
    {!!Html::decode(Form::label('anos_ex', 'Posibilidad de viajar:   ', ['class' => 'control-label col-md-4 col-sm- col-xs-4']))!!}
</div>
<div class="item form-group">
    <div class="col-md-4 col-sm-4 col-xs-4 form-group">
        {{$hojadevida->trabaja_actualmente}}
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4 form-group">
        {{$hojadevida->posibilidad_translado}}
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4 form-group">
        {{$hojadevida->posibilidad_viajar}}
    </div>
</div>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 form-group">
    <br/>
    <h2><strong>Experiencia Laboral</strong></h2>
</div>
<br/>
@foreach($experiencias_laborales as $data)
    <div class="item form-group">
        {!!Html::decode(Form::label('profecion', 'Nombre Empresa:  ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('perfil_laboral', 'Teléfono de la empresa: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('anos_ex', 'Sector de la Empresa:', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('Aspiración ', 'Cargo:', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    </div>
    <div class="item form-group">
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->empresa }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->telefono_empresa }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->sector_empresa }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->cargo_empresa }}
        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12 form-group">

    <div class="item form-group">
        {!!Html::decode(Form::label('fecha_ingreso', 'Fecha ingreso:  ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('perfil_laboral', 'Fecha Finalización: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('timepo_ex', 'tiempo Experiencia:', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('pais ', 'Pais:', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    </div>
    <div class="item form-group">
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->fecha_ingreso_empresa }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->fecha_finalizacion_empresa }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->tiempo_experiencia }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->pais }}
        </div>
    </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 form-group">

    <div class="item form-group">
        {!!Html::decode(Form::label('departamento', 'Departamento:  ', ['class' => 'control-label col-md-4 col-sm-4 col-xs-']))!!}
        {!!Html::decode(Form::label('ciudad', 'Ciudad: ', ['class' => 'control-label col-md-4 col-sm-4 col-xs-4']))!!}
        {!!Html::decode(Form::label('funciones', 'Funciones y logros alcanzadosl:', ['class' => 'control-label col-md-4 col-sm-4 col-xs-4']))!!}
    </div>
    <div class="item form-group">
        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
            {{ $data->depar }}
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
            {{ $data->ciudad }}
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
            {{ $data->funciones_empresa }}
        </div>
    </div>
    </div>
@endforeach

<div class="col-md-12 col-sm-12 col-xs-12 form-group">
    <br/>
    <h2><strong>Educación formal</strong></h2>
</div>
<hr></hr>
@foreach($formaciones as $data)
   <hr style="color: #0056b2;" />
    <div class="item form-group">
        {!!Html::decode(Form::label('nivel', 'Nivel de estudios:', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('area', 'Area de Estudios', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('titulo', 'Titulo:', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('institucion ', 'Institución:', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    </div>
    <div class="item form-group">
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->nivel_estudios }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->area_estudios }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->titulos_estudios }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->institucion_estudios }}
        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
    <div class="item form-group">
        {!!Html::decode(Form::label('profecion', 'Pais Estudios:', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('perfil_laboral', 'Departamento Estudios', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('anos_ex', 'Ciudad Estudios:', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
        {!!Html::decode(Form::label('Aspiración ', 'Funciones y logros alcanzados:', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    </div>
    <div class="item form-group">
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->pais }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->depar }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->ciudad }}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 form-group">
            {{ $data->fucniones_estudios }}
        </div>
    </div>
    </div>
@endforeach
<div class="col-md-12 col-sm-12 col-xs-12 form-group">
<div class="form-group" align='center' id="btn_print">
    <button type="button" onclick="window.print()" class="btn btn-sm btn-success">Imprimir</button>
    <button type="button"  onclick="window.parent.location.href = '/hoja_de_vida'" class="btn btn-sm btn-warning">Atras</button>
</div>
</div>
@stop
<style type="text/css" media="print">
    @media print {
        #btn_print{display: none}
    }
</style>
