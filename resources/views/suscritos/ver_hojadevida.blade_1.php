@extends('layouts.form')
@section('titulo','Suscritos | Administrar')
@section('nombrepagina','Hoja de vida')
@section('contenido')

@include('theme/dompdf_config.inc.php')

<h2 align="center"><strong>Información Personal</strong></h2>
<br/>
<div class="item form-group">
    <div align="center" class="col-md-12 col-sm-12 col-xs-12 form-group">
        {!!Html::image('fotos/'.$hojadevida->foto,null,['class'=>'img-thumbnail profile_img', 'style'=>'width:150px'])!!}        
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('p_nombre', 'Primer Nombre: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('s_nombre', 'Segundo Nombre: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('s_nombre', 'Primer Apellido: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('s_nombre', 'Segundo Apellido: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->p_nombre}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->s_nombre}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->p_apellido}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">     
        {{$hojadevida->s_apellido}}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('tipo_doc', 'Tipo de Indentificación: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('doc', 'Identificación: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('estado_civil', 'Estado Civil: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('genero', 'Genero: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->tipo_doc}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->doc}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->estado_civil}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">     
        {{$hojadevida->genero}}
    </div>
</div>
<div class="item form-group">
    {!!Html::decode(Form::label('email', 'Email: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('id_area_interes', 'Área de Interés: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('tel', 'Teléfono: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('movil', 'Móvil: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->email}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->nom_area_interes}}
    </div>   
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->tel}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->movil}}
    </div>   
</div>

<div class="col-md-12 col-sm-12 col-xs-12 form-group">
    <br/>
    <h2><strong>Datos de Nacimiento</strong></h2>
</div>
<br/>

<div class="item form-group">
    {!!Html::decode(Form::label('fecha_nac', 'Fecha: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('id_paises', 'Pais: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('id_departamentos', 'Departamento: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('id_ciudades', 'Ciudad: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->fecha_nac}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->nom_pais}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->nom_departamento}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">     
        {{$hojadevida->nom_ciudad}}
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 form-group">
    <br/>
    <h2><strong>Residencia Actual</strong></h2>
</div>
<br/>

<div class="item form-group">
    {!!Html::decode(Form::label('dir', 'Dirección: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('paises_hab', 'Pais: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('departamentos_hab', 'Departamento: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
    {!!Html::decode(Form::label('ciudades_hab', 'Ciudad: ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-3']))!!}
</div>
<div class="item form-group">
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida->dir}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida_hab->nom_pais_hab}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">        
        {{$hojadevida_hab->nom_departamento_hab}}
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 form-group">     
        {{$hojadevida_hab->nom_ciudad_hab}}
    </div>
</div>



<div class="form-group" align='center' id="btn_print">
    <button type="button" onclick="window.print()" class="btn btn-sm btn-success">Imprimir</button>
    <button type="button" onclick="window.print()" class="btn btn-sm btn-danger"><i class="fa fa-file-pdf-o"></i> Pdf</button>
    <button type="button"  onclick="window.parent.location.href = '/hoja_de_vida'" class="btn btn-sm btn-warning">Atras</button>    
</div>
@stop
<style type="text/css" media="print">
    @media print {
        #btn_print{display: none}
    } 
</style>
