@extends('layouts.app')
@section('titleForm','Administrar Administradora')
@section('titleTab')
    <div class="title_right">
        <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <a class="form-control" placeholder="Search for...">Crear Administradora</a>
                <span class="input-group-btn">
                    {!!Html::decode(
                        link_to_route(
                        'administradora.create',
                        '<button class="btn btn-sm btn-warning"><i class="fa fa-plus-square"></i> <span style="color:#FFFFFF";>Agregar</span></button>',
                        [],
                        ['class'=>'']
                    ))!!}
                </span>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- page content -->
    <div class="center_col" role="main">
      <div class="">

        <div class="clearfix"></div>

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_content">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                      <table id="datatable-keytable" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th>Nombre</th>
                            <th>Acción</th>
                          </tr>
                        </thead>

                        <tbody>
                            @foreach($administradoras as $data)
                              <tr>
                                <td>{{$data->nombre}}</td>
                                <td>
                                    {!!Html::decode(link_to_route('administradora.edit',
                                    '<button class="btn-primary" title="Editar"><i class="fa fa-edit"></i></button>', [$data->id], ['class'=>'iframe']))!!}
                                    {{-- <a onclick="del({{$data->id}}, 'admindel/{{$data->nombre}}/', 'Administradora: {{$data->nombre}}');"><button class="btn-danger" title="Eliminar"><i class="fa fa-trash-o"></i></button></a> --}}
                                </td>
                              </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('includesCss')
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/msgbox/jquery.msgbox.css')}}" rel="stylesheet">
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/msgbox/jquery.msgbox.js')}}"></script>
    <script src="{{asset('build/js/datatables.js')}}"></script>
    <script src="{{asset('build/js/funtionsConfirm.js')}}"></script>
@endsection