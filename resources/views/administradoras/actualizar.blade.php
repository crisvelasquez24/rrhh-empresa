@extends('layouts.app')
@section('title','Actualizar Administradoras')
@section('content')
{!! Form::model($administradora,['route' => ['administradora.update',$administradora->id], 
        'id'=>'frm','name'=>'frm',
    'method'=>'PUT', 'class'=>'form-horizontal form-label-left','novalidate']) !!}
        @include('administradoras.forms.form_administradoras')
        <div class="form-group" align='center'>
           {!!Form::submit('Actualizar',['class'=>'btn btn-sm btn-primary'])!!}
            <button type="button"  onclick="window.parent.location.href = '/administradora'" class="btn btn-sm btn-warning">Cancelar</button>
        </div>
    {!!Form::close()!!}
@endsection
@section('includesScripts')
    <script src="{{asset('vendors/validator/validator.js')}}"></script>
@endsection